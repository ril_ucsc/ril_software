#!/usr/bin/python  

import sys
import numpy as np

fin = sys.argv[1] # Saves the first argument (input file name) after invoking the program.
fout = fin.replace('.dat','.txt') # New output file name. Has the same name as the input file but replaces the extension ".dat" with ".txt"
tn=0
sn=0
with open(fin,'r') as r, open(fout,'w') as w: # Opens the input file to read, and opens the output file to write.
    for kk,line in enumerate(r): # kk is the counter, line in the line information in the input file.
        l = line.split()
        tn+=1
    	nrse=float(l[12]); # 
    	nmod=float(l[13]);
    	nsub=float(l[14]);
        theta=1./136.*nrse*2.*np.pi;
        xd=0. ;
        yd=(divmod(nsub,4)[1] - 1.5)* 4.1 ;
        zd=(divmod(nsub,4)[0] - 4.0)*5.4 + (nmod-1.5)* 48.6 ;
        xmea=372 * np.cos(theta)-yd * np.sin(theta)+xd * np.cos(theta);
        ymea=372 * np.sin(theta) + yd * np.cos(theta) + xd * np.sin(theta);
        zmea=zd;
        if pow(pow(xmea-float(l[8]), 2) + pow(ymea-float(l[9]), 2) + pow(zmea-float(l[10]), 2),0.5)>13.0:
            print(line)

    	nrse=float(l[35]);
    	nmod=float(l[36]);
    	nsub=float(l[37]);
        theta=1./136.*nrse*2.*np.pi;
        xd=0. ;
        yd=(divmod(nsub,4)[1] - 1.5)* 4.1 ;
        zd=(divmod(nsub,4)[0] - 4.0)*5.4 + (nmod-1.5)* 48.6 ;
        xmea2=372 * np.cos(theta)-yd * np.sin(theta)+xd * np.cos(theta);
        ymea2=372 * np.sin(theta) + yd * np.cos(theta) + xd * np.sin(theta);
        zmea2=zd;
        if pow(pow(xmea2-float(l[31]), 2) + pow(ymea2-float(l[32]), 2) + pow(zmea2-float(l[33]), 2),0.5)>13.0:
            print(line)

        sn+=1
        w.write(str("%.2f"%xmea)+' '+str("%.2f"%ymea)+' '+str("%.2f"%zmea)+' '+str("%.2f"%xmea2)+' '+str("%.2f"%ymea2)+' '+str("%.2f"%zmea2)+'\n')
print 'Total number of LOR is {0}. Number of LOR that is within FOV is {1}.'.format(tn,sn)
