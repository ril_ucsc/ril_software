import os

cwd = os.getcwd()  # Get the current working directory (cwd)
files = os.listdir(cwd)  # Get all the files in that directory
file_list = []
for file in files:
    if file.endswith('Coincidences.dat'):
        file_list.append(file)

for file in file_list:
    command1 = 'python reorggate.py ' + file
    os.system(command1)