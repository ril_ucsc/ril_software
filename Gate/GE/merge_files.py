import os

cwd = os.getcwd()  # Get the current working directory (cwd)
files = os.listdir(cwd)  # Get all the files in that directory
file_list = []
for file in files:
    if file.endswith('Coincidences.txt'):
        file_list.append(file)

command1 = 'cat '
for file in file_list:
    command1 += file + ' '
 
command1 += '> coincidences_merged.txt'
print(command1)   
os.system(command1)
