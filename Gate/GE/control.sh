#!/bin/bash

counter="1"
fname="ascii_outputCoincidences.txt"

while [ $counter -le "10" ]
do
	Gate PET_Sys.mac
	python reorggate.py ascii_outputCoincidences.dat
	mv ascii_outputCoincidences.txt "$fname$counter"
	mv "$fname$counter" data/
	((counter++))
done
