#!/bin/bash
echo "Input coincidence data: $1"
echo "Regularization: $2"
echo "Normalization: $3"
echo "Attenuation: $4"
echo "Scanner: $5"

if [ $2 == "non"  ]
then
    REG=""
else
	REG="_REG"
fi

if [ $3 == "non"  ]
then
    NORM=""
else
	NORM="_NORM"
fi

if [ $4 == "non"  ]
then
    ATN=""
else
	ATN="_ATN"
fi

CURRENT_DATE=$(date +"%m-%d-%y")
DIR_OUT="${5}_${CURRENT_DATE}${REG}${NORM}${ATN}"
mkdir "${DIR_OUT}"
cp configRecon.txt ${DIR_OUT}

RECON_COMMAND="./imagerecon_v1.7 ${1} ${DIR_OUT}/${DIR_OUT}_ ${2} ${3} ${4}"

echo "Running:"
echo $RECON_COMMAND
eval $RECON_COMMAND

cp ObjectiveFuncValue.txt ${DIR_OUT}

echo "Finished..."

