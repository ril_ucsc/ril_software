import os

runs = [1,2,3] # Defines the number of runs for the simulation.
acquisition_time = 0.1 # Defines the acquisition time for the simulation.

for count,value in enumerate(runs):
    file_name = 'main.mac'
    fh1 = open(file_name,'r')
    lines = fh1.readlines()
    fh1.close()
    
    lines[68] = '/gate/output/ascii/setFileName ' + str(acquisition_time) + 's_run' + str(value) + '_' '\n'
    lines[92] = '/gate/application/setTimeSlice ' + str(acquisition_time) + ' s' + '\n'
    lines[94] = '/gate/application/setTimeStop ' + str(acquisition_time) + ' s' + '\n'

    new_file_name = 'main' + str(value) + '.mac'
    fh2 = open(new_file_name,'w')
    fh2.writelines(lines)
    fh2.close()
    
    command = 'Gate ' + new_file_name
    os.system(command)
