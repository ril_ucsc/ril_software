To compile the code, simply type "make" in a terminal. Compiler: g++ with version at least 4.7.

(Nana: by running Make in terminal, we created executable file ONLY
then by running this command line: 
C:\Users\riluser\Desktop\ImageReconst\code>main.exe ShrinkBig.dat non non non
we created lst file in the same folder)

The command for executing the code is as follows:
Linux:
    ./main InputFile ReorgOption BinOption NormOption
	
Windows 10:
	 main.exe InputFile ReorgOption BinOption NormOption

The meaning of the options is as follows:
    ReorgOption: use "reorg" or "non". Use "reorg" when defining detector panels using GATE "scanner" 
	as system type. Use "non" when defining detector panels using GATE "cylindricalPET" as system type.
	
The coincidence data file named "InputFile.lst" will be output. 


main.exe ShrinkBig.dat reorg non non