#configuration file for generate_lst_from_GATE.py
#make sure to put spaces before and after '=' sign
CTSthresh = 25.0e-9 #5e-9 #20e-9 #20ns coincidence cutoff
Elimlo = 480.3 #491 #429 #kev #470
Elimhi = 541.7 #532 #592 #kev #552
Ethresh = 10 #20 #100 #kev, the minimum detection limit (for finding compton)
#Energy resolution (%)
Eblur = 3 #8% energy blurring FWHM at 511keV (put 0 or comment line if no energy blur should be used)
#Time resolution (s)
Tblur = 18.0e-9
DOIres = 1  #DOI resolution in mm.
#number of repeated modules?
numcolumnInRsector = 5 #In x or y direction
numrowInRsector = 30    #In z direction
#anode or cathode
numcolumnInModule = 8
#cathode or anode
numrowInModule = 39
MaximumNumberClusters = 2  #OK, as random events will cause more than two clusters
MaximumNumberEventsInACluster = 2  #Corresponding to at most two interactions
BinEvents = 1 #0: don't bin events in a crystal. 1: bin events, which is more realistic.
CheckSecDiff = 0 #0: don't check. 1: check, and please fill the following two parameters. Suggest 0 for panel simulation, 1 for conventional PET simulation.
MinSecDiff = 1 #minimum difference of rsector for coincidence generation
TotNumSec = 2 #total number of rsectors.
Debug = 0  #1: print debug information.0: don't print.
