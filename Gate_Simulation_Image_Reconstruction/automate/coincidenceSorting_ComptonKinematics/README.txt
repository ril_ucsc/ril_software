To compile the code, simply type "make" in a terminal. Compiler: g++ with version at least 4.7.

The command for executing the code is as follows:
    ./main InputFile ReorgOption BinOption NormOption
The meaning of the options is as follows:
    ReorgOption: use "reorg" or "non". Use "reorg" when defining detector panels using GATE "scanner" as system type. Use "non" when defining detector panels using GATE "cylindricalPET" as system type.
The coincidence data file named "InputFile.lst" will be output. 