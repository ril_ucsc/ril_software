import numpy as np
from skimage import io
from skimage.transform import resize
from skimage import img_as_bool
import matplotlib.pyplot as plt
from scipy.ndimage import zoom

#variables

nz_input=79.0
ny_input=119.0
nx_input=95.0
nz_output=150.0
ny_output=200.0
nx_output=200.0
input_file = "ged_95x119x79.bin"
output_file= "ged_200x200x150.bin"

#load 3d data
data=np.fromfile(input_file,dtype=np.float32, sep='')
print(data)
#reshape data into the right array dimensions
data=data.reshape(int(nz_input),int(ny_input),int(nx_input))

zoom_x = nx_output/nx_input
zoom_y = ny_output/ny_input
zoom_z = nz_output/nz_input

resized = zoom(data, (zoom_z, zoom_y, zoom_x), order=0)
#resized = zoom(data, zoom=(2.0, 2.0, 2.0), order=0)
print(resized.shape)

np.array(resized,np.float32).tofile(output_file)