function [Parameters, channelIDs] = importKev(Parameters, channelIDs)

% [Parameters, channelIDs] = importKev(Parameters, channelIDs)
% 
% The function imports the ADC to keV calibration parameters from a *.keV
% file. This is done from a *.keV in the current directory, if it exists.
% If none or multiple *.keV files exist in the current directory,
% then a dialog box is opened to allow the user to select the *.keV file to
% import.
%
% importTec must have been called on Parameters before invoking this
% function.
% 
% The input Parameters must be a cell array with structure
% Parameters{node}{board}{rena}{channel} = [1 x 13 vector], originally
% all initialized to -1. In the returned Parameters, the *.keV parameters
% are placed into element positions 6 to 9 of the [1 x 13 vector]
% associated with each channel. Element 6 is the mapping slope, 7 the
% mapping intercept, 8 the parameter status (1 = good), and 9 the polarity.
%
% Channels for which there is incomplete kev parameters are removed from
% channelID. In Parameters, the [1 x 13] vector is set back to -1 for those
% channels, while element number 5 in the remaining channels are updated to
% their new index in channelID.

fprintf('\n');
display('Starting ADC to keV conversion parameter import...');

% Find .keV filename and open it
filename=dir('*.kev');

if(length(filename)~=1)
    display('Current scope has either too many or no .kev files');
    
    % Prompt user to select file
    [filename, pathname] = uigetfile('*.kev', 'Select a ADC to keV conversion file');
    
    if isequal(filename,0)
        disp('No .kev file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    
    % Check if selected file is valid
    fileinfo=dir([pathname filename]);
    if fileinfo.bytes==0
        error('Empty .kev file');
    end
    
    % Open file
    KEVfile=fopen([pathname filename],'r');    
else
    if(filename.bytes==0)
        error('Empty .kev file');
    end
    disp(['Using ', filename.name])
    
    % Open file
    KEVfile=fopen(filename.name,'r');
end
tic

%Skip first two header lines of file
line=fgetl(KEVfile);
line=fgetl(KEVfile);

% This is the first data line
line = fgetl(KEVfile);
addKEV = 0;
missingKEV = 0;
missingTEC = 0;

while ischar(line)
    % Check for invalid or missing parameters for channel
    if(~isempty(strfind(line, 'inf')) || ~isempty(strfind(line, 'nan')))
        missingKEV = missingKEV + 1;
        line = fgetl(KEVfile);
        continue;
    end
    
    % Check for invalid or missing parameters for channel
    kev = textscan(line,'%d %d %d %d %d %f %f');
    if(kev{5}~=0 && kev{5}~=1)
        missingKEV = missingKEV + 1;
        line = fgetl(KEVfile);
        continue;
    end
    
    % If the file uses a -1 node then make it 1.
    if(kev{1}==-1)
        kev{1}=1;
    else
        kev{1}=kev{1}+1;
    end
    
    % Adjust channelID info to work with MATLAB style vector notation (vectors start with 1 not 0)
    kev{2}=kev{2}+1;
    kev{3}=kev{3}+1;
    kev{4}=kev{4}+1;
    
    % Invalid or missing tec parameters means the kev parameters should not be saved either
    % Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
    if(Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(5) == -1)
        missingTEC = missingTEC + 1;
        line = fgetl(KEVfile);
        continue;   
    end
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(6:7)=[kev{6} kev{7}]; % Add parameters to parameter map
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(8)=1;                 % kev parameter status (1: parameters are good)
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(9)=kev{5};            % Polarity
    
    addKEV = addKEV+1;
    line = fgetl(KEVfile);
end
fclose(KEVfile);

% Loop through all channels and tag those that have tec parameters but no
% kev parameters, then remove the channels from the channelIDs list.
% First tag channels to be removing
for i=1:length(channelIDs)
    if (i == 62)
        bob = 3;
    end
    id=channelIDs(i,:);
    % Check keV parameter status
    if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(8) == -1)
        Parameters{id(1)}{id(2)}{id(3)}{id(4)}(:) = -1;
        channelIDs(i,1) = -1;
    end
end

% Then actually remove the channels
index=find(channelIDs(:,1) == -1);
channelIDs(index,:)=[];

% % Count the number of cathode channels
% numCat = 0;

% Loop through all still valid channels and update the channel index
% numbers (parameter 5) so that A matrix construction works correctly
if(~isempty(index))
    for i = 1:length(channelIDs)
        id = channelIDs(i,:);
        Parameters{id(1)}{id(2)}{id(3)}{id(4)}(5) = i;
        
%         % Count the number of cathode channels
%         if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(9)==0)
%             numCat=numCat+1;
%         end
    end
end

if(addKEV==0)
    warning(['Dropped all parameters for ' num2str(missingKEV + missingTEC) ' channels because of missing or invalid TEC parameters or missing or invalid energy conversion parameters.']);
    error('No energy conversion parameters imported.');
end

display(['Added conversion parameters for ' num2str(addKEV) ' channels.']);
warning(['Dropped parameters for ' num2str(missingKEV+missingTEC) ' channels because of missing or invalid TEC parameters, or missing or invalid energy conversion parameters.']);

display('Finished importing ADC to keV conversion parameters...');
fprintf('\n');

toc
