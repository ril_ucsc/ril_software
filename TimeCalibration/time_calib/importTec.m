function [Parameters, channelIDs] = importTec(Parameters, channelIDs)

% [Parameters, channelIDs] = importTec(Parameters, channelIDs)
% 
% The function imports the UV timing circle calibration parameters from a
% *.tec file. This is done from a *.tec in the current directory, if it
% exists. If none or multiple *.tec files exist in the current directory,
% then a dialog box is opened to allow the user to select the *.tec file to
% import.
%
% The input Parameters must be a cell array with structure
% Parameters{node}{board}{rena}{channel} = [1 x 13 vector] of -1s.
% In the returned Parameters, the *.tec parameters are placed into element
% positions 1 to 4 of the [1 x 13 vector] associated with each channel.
% Element position 5 indicates the channel index #, which can be used to
% index into the channelIDs matrix.
%
% Each row of channelIDs has the format [node, board, rena, channel] for
% each channel imported.

fprintf('\n');
display('Starting timing ellipse correction parameter import...');

% Find .tec filename and open it
filename=dir('*.tec');

%==========================================================================
% Show dialog box if there is more or less than 1 .tec file in the current
% directory
if(length(filename)~=1) 
    display('Current scope has either too many or no .tec files');
    
    % Prompt user to select file
    [filename, pathname] = uigetfile('*.tec', 'Select a timing ellipse correction parameters file');
    
    if isequal(filename,0)
        disp('No *.tec file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    
    % Check if selected file is valid
    fileinfo=dir([pathname filename]);
    if (fileinfo.bytes == 0)
        error('Empty .tec file');
    end
    
    % Open file
    TECfile=fopen([pathname filename],'r');    
else
    if(filename.bytes == 0)
        error('Empty .tec file');
    end
    disp(['Using ', filename.name])
    
    % Open file
    TECfile=fopen(filename.name,'r');
end

tic

%==========================================================================
% Loop through each line of the file and add params of each channel to the
% correct cell in the Parameters cell array
params(1:13)=-1;
i=1;
line=fgetl(TECfile); %first line is 'channel{' (skip)
while ischar(line)
    for j=1:8
        line=fgetl(TECfile); %first line is 'node=#'
        if(line~=-1)         %Throw error if there is an invalid line
            switch j         %Do one of the following depending on which line it is
                case 1       %Node line
                    nd=str2double(line(strfind(line,'=')+1:end));
                    % Deal with -1 nodes, which correspond to data files
                    % acquired directly from a RENA board without
                    % intervening dausy chain node.
                    if(nd == -1)
                        nd = 1;
                    else
                        nd = nd+1;
                    end
                case 2      %Board line
                    brd = str2double(line(strfind(line,'=')+1:end))+1;
                case 3      %RENA line
                    rna = str2double(line(strfind(line,'=')+1:end))+1;
                case 4      %Channel line
                    cnl = str2double(line(strfind(line,'=')+1:end))+1;
                otherwise   %The other lines are actual parameters
                    params(j-4) = str2double(line(strfind(line,'=')+1:end));
            end                   
        else
            error(['.tec file format error at line ' num2str((i-1)*10+j)]);
        end        
    end
    params(5)=i; %tec parameter status (>=1: parameters are good)
    % tec_param_status also indexes into channelIDs
    channelIDs = [channelIDs; [nd brd rna cnl]];
    Parameters{nd}{brd}{rna}{cnl}=params; %Write parameters to array
    line=fgetl(TECfile);            %line is '}'
    line=fgetl(TECfile);            %line is 'channel{'
    i=i+1;
end

% Close file
fclose(TECfile);

if(isempty(channelIDs))
    error('No timing ellipse correction parameters imported');
end

fprintf(['Finished importing timing ellipse correction parameters for\n' num2str(length(channelIDs)) ' channels...\n']);
fprintf('\n');

toc

