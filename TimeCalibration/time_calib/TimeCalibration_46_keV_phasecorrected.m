% This script plots and performs time calibration using processing method
% according to Paul, where data is split up according to the node they're
% from.
close all;
pause(0.5);
clear; clc;

%Change loadNew to '1' if you want to read all data from file
%keep it at '0' if you want to load from .mat file
loadNew = 0; %1; %flags for running full simulation 
loadNew2 = 1;
exportTcc = 1; %when =1, export paramters to *.tcc file 
useCalib = 1;

numItMax = 30;   % Maximum number of optimization iterations
dropThresh = 1000;  % Minimum number of events to drop during optimization
                    % to warrant another iteration
numStds = 2;     % Number of standard devs to keep (outlier limit)
numBins = 20;       % Number of bins to use to find gaussian fit to time
                    % differences
RowThreshold = 7;   % minimum number of occurances channel must have in A
                    % matrix to be considered in optimization
                        
if (loadNew2 == 1)
    blockSize = 10000;
    cushionLines = 100;

   fileName = '/Users/Shiva/Desktop/time_calib/20150410-68Ge-500V-st80V-UV490khz-0p8Vpp_kev_c2a.eventp';
    %fileName = '20140509_Ge68_500V_st80V_UV490kHz_1p6Vpp_node1-2.eventp';
    %fileName = '/Users/Shiva/Desktop/time_calib/Olength_kev_gated_c2a_100s_delay.eventp';
    %fileName='/Users/Shiva/Desktop/time_calib/Olength_kev_gated_c2a.eventp';
  %  fileName = '20140612_PSF_Ge68_500V_st80V_UV490kHz_2Vpp_1p9shapingtime.eventp';
    %fileName = '20140612_PSF_Ge68_500V_st80V_UV490kHz_2Vpp_1p9shapingtimeUVdata.eventp';
    % fileName = 'a.eventp';
    uvFreq = 490e3;     % Frequency of U and V sinusoids (Hz)
    ctsWin = 50;
    nodeList = [1 2];   % List of nodes present in data

    % Below parameters are only for the construction of calbration data cell
    % array, and does not reflect the hardware present in the data.
    numNodes = 5;       % Nodes are IDed 1, 2, 3, 4
    numBoards = 48;
    numRENAs = 2;
    numChannels = 36;

    if (loadNew == 1)
        % Initialize data structure to store necessary parameters
        % Parameter list:
        % 1.  centerU          %UV ellipse correction parameters
        % 2.  centerV
        % 3.  psi
        % 4.  radius
        % 5.  tec_param_status (also channel index#)
        % 6.  slope            %ADC to keV conversion parameters
        % 7.  intercept
        % 8.  kev_param_status
        % 9.  polarity
        % 10. a0               %Depth correction parameters
        % 11. a1
        % 12. a2
        % 13. dcc_param_status
        params(1:13)=-1; %Initialize parameter array
        for i=1:numChannels
            chan{i}=params; %Create array of channels
        end
        for i=1:numRENAs
            rena{i}=chan;   %Create array of renas
        end
        for i=1:numBoards
            board{i}=rena;  %Create array of boards
        end
        for i=1:numNodes
            Parameters{i}=board;  %Create array of nodes
        end
        clear params chan rena board;

        % Initialize array to map channel list index to channel ID info to make
        % export easier. I.e. channelIDs is a list of channel ID's where each row
        % corresponds to a channel being considered for optimization
        % Will be [node, board, rena, channel;]
        channelIDs=[];

        %======================================================================
        % Import calibration parameters and photoelectric interaction data to
        % construct A
        %======================================================================
        % UV ellipse correction parameters from .tec file
        [Parameters, channelIDs] = importTec(Parameters, channelIDs);

        % Import depth correction parameters from .dcc file
        Parameters = importDcc(Parameters);

        % Import ADC to keV conversion parameters from .kev file
        [Parameters, channelIDs] = importKev(Parameters, channelIDs);

        %======================================================================
        % Import and process event data
        %======================================================================
        dataByNode = cell(1, length(nodeList));
        dataB4CC = cell(1, length(nodeList));
        for i = 1:length(nodeList)
            dataByNode{i}.ca = [];
            dataByNode{i}.an = [];
            dataB4CC{i}.ca = [];
            dataB4CC{i}.an = [];
        end

        numLines = 0;
        numCoins = 0;
        numDeletes = 0;

        % Import data
        disp([' ']);
        disp(['Reading ' fileName])
        disp([' ']);

        eventpFile = fopen(fileName, 'r');

        eofFlag = 0; %end of file flag
        while(1)
            i = 1;
            %==================================================================
            % Load in measurement tripples
            while(i <= blockSize)
                line = fgetl(eventpFile);

                % Check for invalid input
                if(~ischar(line))
                    eofFlag = 1;
                    break;
                end

                % Node, board, rena, channel, polarity, PHA, U, V, CTS
                inputChunk(i,:)=textscan(line,'%f %f %f %f %f %f %f %f %f');
                i = i + 1;

                % Update user
                numLines = numLines + 1;
                if(mod(numLines, 10000) == 0)
                    % ASCII 8 is the delete character, repeated as many times
                    % as necessary to erase previous printout.
                    fprintf([repmat(8, 1, numDeletes) 'Imported ' num2str(numLines) ' lines...']);
                    numDeletes = length(['Imported ' num2str(numLines) ' lines...']);
                end
            end

            %==================================================================
            % Add imported chunk to temporary data structure
            data = cell2mat(inputChunk);
            inputChunk = {};

            for i = 1:length(nodeList)
                % Extract data from one node
                temp = data(data(:, 1) == nodeList(i), :);

                % Make sure time is ascending
                temp = sortrows(temp ,9);

                % Split by anode and cathode
                dataB4CC{i}.ca = [dataB4CC{i}.ca; temp(temp(:, 5) == 0, :)];
                dataB4CC{i}.an = [dataB4CC{i}.an; temp(temp(:, 5) == 1, :)];
            end

            ind1 = 1;
            ind2 = 1;
            ind1Log = [];
            ind2Log = [];

            %==================================================================
            % Iterate over temporary data structure to look for cathode-cathode
            % coincidence
            % Determine correct end point for iteration
            if (eofFlag == 0)
				% minimum number of cathodes (for both nodes)
                numInB4CC = min([size(dataB4CC{1}.ca, 1) size(dataB4CC{2}.ca, 1)]);
                stopHere1 = numInB4CC-cushionLines;
                stopHere2 = stopHere1;
            else
                stopHere1 = size(dataB4CC{1}.ca, 1);
                stopHere2 = size(dataB4CC{2}.ca, 1);
            end

            % Look for coincidence between cathodes within the ctsWin window
            while ((ind1 < stopHere1) && (ind2 < stopHere2))
                dt = dataB4CC{1}.ca(ind1, 9) - dataB4CC{2}.ca(ind2, 9);

				% each counter is different by 20ns, so if dt is different by 
				% 50 it means that they are different by 1us
                if (abs(dt) <= ctsWin)
                    ind1Log = [ind1Log ind1];
                    ind2Log = [ind2Log ind2];

                    ind1 = ind1 + 1;
                    ind2 = ind2 + 1;
                elseif (dt < 0) % if node 2 before node 1
                    ind1 = ind1 + 1;
                else
                    ind2 = ind2 + 1;
                end
            end
            numCoins = numCoins + length(ind1Log);

            %==================================================================
            % Save paired data into permanent data structure
            dataByNode{1}.ca = [dataByNode{1}.ca; dataB4CC{1}.ca(ind1Log, :)];
            dataByNode{1}.an = [dataByNode{1}.an; dataB4CC{1}.an(ind1Log, :)];
            dataByNode{2}.ca = [dataByNode{2}.ca; dataB4CC{2}.ca(ind2Log, :)];
            dataByNode{2}.an = [dataByNode{2}.an; dataB4CC{2}.an(ind2Log, :)];

            % Delete unpaired data
            dataB4CC{1}.ca(1:ind1-1, :) = [];
            dataB4CC{1}.an(1:ind1-1, :) = [];
            dataB4CC{2}.ca(1:ind2-1, :) = [];
            dataB4CC{2}.an(1:ind2-1, :) = [];

            if (eofFlag == 1)
                break;
            end
        end

        % Close file
        fclose(eventpFile);

        fprintf(['\n']);
        disp([num2str(numCoins/(numLines/4)*100) '% of data were coincidences.']);

        save('20150410-68Ge-500V-st80V-UV490khz-0p8Vpp.mat', 'Parameters', 'channelIDs', 'dataByNode', 'nodeList', 'numLines', 'numCoins');
        disp('Saved to file.');
    end

    %%=========================================================================
    % Post processing
    %==========================================================================
    disp(['Importing data into workspace...']);
    % load('paul1_735MB_dontDelete1.mat');
    %load('scatterList2DataByNodes_data.mat');
    load('20150410-68Ge-500V-st80V-UV490khz-0p8Vpp.mat');

    %==========================================================================
    % Convert energy and time measurements
    disp(['Retrieving calibration parameters...']);
    numChans = size(channelIDs, 1);
    inData = zeros(1, numChans);

    % Loop over nodes in data
    for i = 1:length(nodeList)
        % Loop over data in each node
        for j = 1:size(dataByNode{i}.ca, 1)
            % Cathodes
            nodeIDC = dataByNode{i}.ca(j, 1) + 1;
            brdIDC = dataByNode{i}.ca(j, 2) + 1;
            renaIDC = dataByNode{i}.ca(j, 3) + 1;
            chanIDC = dataByNode{i}.ca(j, 4) + 1;

            % Anodes
            nodeIDA = dataByNode{i}.an(j, 1) + 1;
            brdIDA = dataByNode{i}.an(j, 2) + 1;
            renaIDA = dataByNode{i}.an(j, 3) + 1;
            chanIDA = dataByNode{i}.an(j, 4) + 1;

            % This could happen if the line was tagged for lack of calibration
            % parameter
            if (nodeIDC == 0)
                continue;
            end
            if (nodeIDA == 0)
                continue;
            end

            % Check if calibration parameters exist for channel
            if (Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(1) == -1)
                dataByNode{i}.ca(j, 1) = -1;
                dataByNode{i}.an(j, 1) = -1;
                if (i == 1)
                    dataByNode{i+1}.ca(j, 1) = -1;
                    dataByNode{i+1}.an(j, 1) = -1;
                else
                    dataByNode{i-1}.ca(j, 1) = -1;
                    dataByNode{i-1}.an(j, 1) = -1;
                end
                continue;
            else
                inData(Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(5)) = 1;
                
                if (useCalib == 1)
                    uCenter = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(1);
                    vCenter = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(2);
                    int = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(6);
                    slope = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(7);
                    
                    %ADDED SHIVA
                    psi_val = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(3);
                    radius_tec = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(4);

                    %dataByNode{i}.ca(j, 6) = dataByNode{i}.ca(j, 6)*int + slope;
                    dataByNode{i}.ca(j, 7) = dataByNode{i}.ca(j, 7) - uCenter;
                    dataByNode{i}.ca(j, 8) = dataByNode{i}.ca(j, 8) - vCenter;
                    %ADDED SHIVA
                    %-----Fix V based on psi_val (ellipse correction)
                    dataByNode{i}.ca(j, 8) = (dataByNode{i}.ca(j, 8)-dataByNode{i}.ca(j, 7)*sin(psi_val))/cos(psi_val);
                    
                end
            end

            % Check if calibration parameters exist for channel
            if (Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(1) == -1)
                dataByNode{i}.ca(j, 1) = -1;
                dataByNode{i}.an(j, 1) = -1;
                if (i == 1)
                    dataByNode{i+1}.ca(j, 1) = -1;
                    dataByNode{i+1}.an(j, 1) = -1;
                else
                    dataByNode{i-1}.ca(j, 1) = -1;
                    dataByNode{i-1}.an(j, 1) = -1;
                end
                continue;
            else
                inData(Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(5)) = 1;
                
                if (useCalib == 1)
                    uCenter = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(1);
                    vCenter = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(2);
                    int = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(6);
                    slope = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(7);

                    %ADDED SHIVA
                    psi_val = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(3);
                    radius_tec = Parameters{nodeIDA}{brdIDA}{renaIDA}{chanIDA}(4);
                    
                    %dataByNode{i}.an(j, 6) = dataByNode{i}.an(j, 6)*int + slope;
                    dataByNode{i}.an(j, 7) = dataByNode{i}.an(j, 7) - uCenter;
                    dataByNode{i}.an(j, 8) = dataByNode{i}.an(j, 8) - vCenter;
                    %ADDED SHIVA
                    %-----Fix V based on psi_val (ellipse correction)
                    dataByNode{i}.an(j, 8) = (dataByNode{i}.an(j, 8)-dataByNode{i}.an(j, 7)*sin(psi_val))/cos(psi_val);
                    
                end
            end
        end
    end

    % Remove lines tagged with -1
    for i = 1:length(nodeList)
        index = find(dataByNode{i}.ca(:, 1) == -1);
        dataByNode{i}.ca(index, :) = [];
        dataByNode{i}.an(index, :) = [];
    end

    % Delete channels in channelIDs not in data
    index = find(inData == 0);
    for i = 1:length(index)
        nodeID = channelIDs(index(i), 1);
        brdID = channelIDs(index(i), 2);
        renaID = channelIDs(index(i), 3);
        chanID = channelIDs(index(i), 4);
        Parameters{nodeID}{brdID}{renaID}{chanID}(1:end) = -1;
    end
    channelIDs(index, :) = [];

    % Update Parameters indices with new indices
    for i = 1:size(channelIDs, 1)
        nodeID = channelIDs(i, 1);
        brdID = channelIDs(i, 2);
        renaID = channelIDs(i, 3);
        chanID = channelIDs(i, 4);
        Parameters{nodeID}{brdID}{renaID}{chanID}(5) = i;
    end

    %% Time differences
    tStampC1 = dataByNode{1}.ca(:, 7) + 1i*dataByNode{1}.ca(:, 8);
    tStampC2 = dataByNode{2}.ca(:, 7) + 1i*dataByNode{2}.ca(:, 8);
    dtcc = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
    tStampA1 = dataByNode{1}.an(:, 7) + 1i*dataByNode{1}.an(:, 8);
    dtca1 = angle(tStampA1./tStampC1)/2/pi/uvFreq*1e9;

    tStampA2 = dataByNode{2}.an(:, 7) + 1i*dataByNode{2}.an(:, 8);
    dtca2 = angle(tStampA2./tStampC2)/2/pi/uvFreq*1e9;
    
    figure;
    [h c] = hist(dtcc, -1000:2:1000);
    plot(c, h, 'r.');
    xlabel(['cc']);
    drawnow;

    figure;
    [h1 c1] = hist(dtca1,-1000:2:1000 );
    plot(c1, h1, 'r.');
    xlabel(['ca1']);
    drawnow;

    figure;
    [h2 c2] = hist(dtca2, -1000:2:1000);
    plot(c2, h2, 'r.');
    xlabel(['ca2']);
    drawnow;

    %%========= Added to get in anode numbers for different Rn/Bd/Ch
    even_ids = load('even_bd_ids.txt');
    odd_ids = load('odd_bd_ids.txt');
    %make matrices to hold anode#
    even_anodes = zeros(2,28); %rows are rena, columns are channels
    odd_anodes = zeros(2,28);
    for i=1:length(even_ids)
        %need to do +1 for row because Rena=0 or 1
        even_anodes(even_ids(i,1)+1,even_ids(i,2)) = even_ids(i,3);
    end
    for i=1:length(odd_ids)
        %need to do +1 for row because Rena=0 or 1
        odd_anodes(odd_ids(i,1)+1,odd_ids(i,2)) = odd_ids(i,3);
    end
    
    %%========================================================================= 
    % Make A
    num_coeff=3+39;
    numCoins = size(dataByNode{1}.ca, 1);
    row = 1;
    ANZnum = 1;
    progress = 10;
    for i = 1:numCoins
        a1 = dataByNode{1}.an(i, :);
        c1 = dataByNode{1}.ca(i, :);
        a2 = dataByNode{2}.an(i, :);
        c2 = dataByNode{2}.ca(i, :);
        a1blkpos = (Parameters{a1(1)+1}{a1(2)+1}{a1(3)+1}{a1(4)+1}(5) - 1)*num_coeff + 1;
        c1blkpos = (Parameters{c1(1)+1}{c1(2)+1}{c1(3)+1}{c1(4)+1}(5) - 1)*num_coeff + 1;
        a2blkpos = (Parameters{a2(1)+1}{a2(2)+1}{a2(3)+1}{a2(4)+1}(5) - 1)*num_coeff + 1;
        c2blkpos = (Parameters{c2(1)+1}{c2(2)+1}{c2(3)+1}{c2(4)+1}(5) - 1)*num_coeff + 1;
        
        % Node 1
        a1U = a1(7);
%         if (a1U == 0)
%             a1U = randn(1)*1e-12;
%         end
        a1V = a1(8);
%         if (a1V == 0)
%             a1V = randn(1)*1e-12;
%         end
        c1U = c1(7);
%         if (c1U == 0)
%             c1U = randn(1)*1e-12;
%         end
        c1V = c1(8);
%         if (c1V == 0)
%             c1V = randn(1)*1e-12;
%         end
        c2a1 = c1(6)/a1(6);

        % Node 2
        a2U = a2(7);
%         if (a2U == 0)
%             a2U = randn(1)*1e-12;
%         end
        a2V = a2(8);
%         if (a2V == 0)
%             a2V = randn(1)*1e-12;
%         end
        c2U = c2(7);
%         if (c2U == 0)
%             c2U = randn(1)*1e-12;
%         end
        c2V = c2(8);
%         if (c2V == 0)
%             c2V = randn(1)*1e-12;
%         end
        c2a2 = c2(6)/a2(6);

%         % Define elements of the first row in the notation of the sparse
%         % function: R defines rows, C the columns, and S the actual values.
%         Y(row, 1) = dtca1(i);
%         R(ANZnum:ANZnum+(num_coeff*2-1))= row;
%         C(ANZnum:ANZnum+(num_coeff-1))= [a1blkpos:a1blkpos+(num_coeff-1)];
%         C(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = [c1blkpos:c1blkpos+(num_coeff-1)]; 
%          anode_select = zeros(1,39);
%         S(ANZnum:ANZnum+(num_coeff-1))= [anode_select a1U/1e2 a1U^2/1e5 a1V/1e2 a1U*a1V/1e5 c2a1 c2a1^2 1];
%         S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[anode_select c1U/1e2 c1U^2/1e5 c1V/1e2 c1U*c1V/1e5 1 1 1];
%         %S(ANZnum:ANZnum+(num_coeff-1))= [1];
%         %S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[1];
%         ANZnum = ANZnum + (num_coeff*2);
%         row = row + 1;
% 
%         Y(row, 1) = dtca2(i);
%         R(ANZnum:ANZnum+(num_coeff*2-1))   = row;
%         C(ANZnum:ANZnum+(num_coeff-1))= [a2blkpos:a2blkpos+(num_coeff-1)];
%         C(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = [c2blkpos:c2blkpos+(num_coeff-1)];
%         %S(ANZnum:ANZnum+(num_coeff-1))    = [1];
%         anode_select = zeros(1,39);
%         S(ANZnum:ANZnum+(num_coeff-1))= [anode_select a2U/1e2 a2U^2/1e5 a2V/1e2 a2U*a2V/1e5 c2a2 c2a2^2 1];
%         S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[anode_select c2U/1e2 c2U^2/1e5 c2V/1e2 c2U*c2V/1e5 1 1 1];
%         %S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[1];
%         ANZnum = ANZnum + (num_coeff*2);
%         row = row + 1;

        Y(row, 1) = dtcc(i);
        R(ANZnum:ANZnum+(num_coeff*2-1))    = row;
        C(ANZnum:ANZnum+(num_coeff-1)) = [c1blkpos:c1blkpos+(num_coeff-1)]; 
        C(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = [c2blkpos:c2blkpos+(num_coeff-1)];
        %S(ANZnum:ANZnum+(num_coeff-1))= [c1U/1e2 c1U^2/1e5 c1V/1e2 c1V^2/1e5 1 1 1];
        %S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[c2U/1e2 c2U^2/1e5 c2V/1e2 c2V^2/1e5 1 1 1];
        %find anode# for anode1
        anode_select = zeros(1,39); % = all zeros except for the anode of this interaction
        if mod(a1(2),2) == 1
           anode_number = odd_anodes(a1(3)+1,a1(4));
           
        else
            anode_number = even_anodes(a1(3)+1,a1(4)); 
        end
        anode_select(anode_number) = 1;
        %S(ANZnum:ANZnum+(num_coeff-1))= [anode_select c1U/1e2 c1U^2/1e5 c1V/1e2 c1U*c1V/1e5 c2a1 c2a1^2 1];
        S(ANZnum:ANZnum+(num_coeff-1))= [anode_select c2a1 c2a1^2 1];
        %find anode# for anode2
        anode_select = zeros(1,39);
        if mod(a2(2),2) == 1
           anode_number = odd_anodes(a2(3)+1,a2(4)); %fins which anode# it is for anode1
           
        else
            anode_number = even_anodes(a2(3)+1,a2(4)); 
        end
        anode_select(anode_number) = 1;
        %S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[anode_select c2U/1e2 c2U^2/1e5 c2V/1e2 c2U*c2V/1e5 c2a2 c2a2^2 1];
        S(ANZnum+num_coeff:ANZnum+(num_coeff*2-1)) = -1.*[anode_select c2a2 c2a2^2 1];
        ANZnum = ANZnum + (num_coeff*2);
        row = row + 1;

        % Update user
        if (i/numCoins*100 >= progress)
            disp([num2str(progress) '% done...']);
            progress = progress + 10;
        end
    end

    A = sparse(R, C, S);
    %save('Dec2014_full_before_findWindow.mat')
    %clear all, then load paul2.mat to see which parameters were saved
else
    load('paul2.mat');
end


StoreA = A;
StoreY = Y;

%% ========================================================================
% Solve for X
%==========================================================================
Y = StoreY;
A = StoreA;
Ystart = Y;
Astart = A;

binC = linspace(-1020, 1020, 1000);

window = 2; %stdevs.

current_cal = Y;

for i = 1:numItMax
    
    
    
    sel = ones(size(Y));
    
    figure(1)
    clf;
    
    % Time gate on dta1c1
    
%     subplot(311)
%     [na xa] = hist(current_cal(1:3:end), -1000:2:1000);
%     plot(xa,na)
%     curve = fit(xa',na','Gauss1');
%     hold on
%     plot(xa,curve(xa),'r')
%     xlim([-1000 1000])
%     hold on
%     loLim = curve.b1 - window*curve.c1/sqrt(2);
%     hiLim = curve.b1 + window*curve.c1/sqrt(2);
%     title ([loLim hiLim])
%     
%     %[loLim hiLim] = findWindow(current_cal(1:3:end), numStds, numBins);
%     dropRows = find((current_cal(1:3:end)<loLim) | (current_cal(1:3:end)>hiLim));
%     dropRows = (dropRows*3)-2;
%     dropRows = sort([dropRows' dropRows'+1 dropRows'+2]);
%     sel(dropRows) = 0;
%     
%   
%     
%     % Time gate on dta2c2
%     subplot(312)
%     [na xa] = hist(current_cal(2:3:end), -1000:2:1000);
%     plot(xa,na)
%     curve = fit(xa',na','Gauss1');
%     hold on
%     plot(xa,curve(xa),'r')
%     xlim([-1000 1000])
%     hold on
%     loLim = curve.b1 - window*curve.c1/sqrt(2);
%     hiLim = curve.b1 + window*curve.c1/sqrt(2);
%     title ([loLim hiLim])
%     
%     %[loLim hiLim] = findWindow(current_cal(2:3:end), numStds, numBins);
%     dropRows = find((current_cal(2:3:end)<loLim) | (current_cal(2:3:end)>hiLim));
%     dropRows = (dropRows*3)-1;
%     dropRows = sort([dropRows'-1 dropRows' dropRows'+1]);
%     sel(dropRows) = 0;

    % Time gate on dtc1c2
    subplot(313)
    %[na xa] = hist(current_cal(3:3:end), -1000:2:1000);
    [na xa] = hist(current_cal, -1000:2:1000);
    plot(xa,na)
    curve = fit(xa',na','Gauss1');
    hold on
    plot(xa,curve(xa),'r')
    xlim([-1000 1000])
    hold on
    loLim = max(-700,curve.b1 - window*curve.c1/sqrt(2.));
    hiLim = min(700, curve.b1 + window*curve.c1/sqrt(2.));
    title ([loLim hiLim])
    
  
%     dropRows = find((current_cal(3:3:end)<loLim) | (current_cal(3:3:end)>hiLim));
%     dropRows = (dropRows*3);
%     dropRows = sort([dropRows'-2 dropRows'-1 dropRows']);
%     sel(dropRows) = 0;
    
    dropRows = find((current_cal<loLim) | (current_cal>hiLim));
    dropRows = sort(dropRows');
    sel(dropRows) = 0;
    
    figure(2)
   
    
    sel = sel == 1;
    

    X = A(sel, 1:end-1)\Y(sel);%shiva:calculating X after dropping some of A and Y values
    
    current_cal = Y - A(:, 1:end-1)*X;%shiva:with original Y and A and new guess for X calculate the error
    
    % Add a zero for parameter a6 (DC) for the reference channel
    X(end+1) = 0;

    % Plot iterim results
  %  temp = Ystart - Astart*sum(Xall, 2);
    temp = Ystart - Astart*X;
    temp = temp(temp >= -1020);
    temp = temp(temp <= 1020);
    temp = temp(3:3:end);
    [binH, binC] = hist(temp, binC);
    plot(binC, binH);
    hold on;
    xlabel(['Time difference (ns)']);
    ylabel(['Count']);
    title(['Iteration ' num2str(i)]); 
    drawnow;
    
    display(['Finished iteration ' num2str(i) ', Size of sel ' num2str(sum(sel))]);
    
    pause(.01)
end

%shiva: added this line
% The optimal calibration coefficients
Xopt=X;

% %% ========================================================================
% % Export paramters to *.tcc file
% %==========================================================================
% %DEBUG
% if (exportTcc == 1)
%     %eventPfilePath = '20140509_Ge68_500V_st80V_UV490kHz_1p6Vpp_node1-2.eventp';
%     eventPfilePath = ' /Users/Shiva/Desktop/time_calib/20150410-68Ge-500V-st80V-UV490khz-0p8Vpp_kev_c2a.eventp';
%     display('Exporting parameters to .tcc file...');
%     outf=fopen([eventPfilePath(1:strfind(eventPfilePath,'.eventp')-1) '.tcc'],'w');
%     fprintf(outf, ['node board rena channel a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12 a13 a14 a15 a16 a17 a18 a19 a20 a21 a22 a23 a24 a25 a26 a27 a28 a29 a30 a31 a32 a33 a34 a35 a36 a37 a38 a39 a40 a41 a42 a43 a44 a45\n']);
%     Xind=1;
% 
%     for i=1:length(channelIDs)
%         if(Parameters{channelIDs(i,1)}{channelIDs(i,2)}{channelIDs(i,3)}{channelIDs(i,4)}(9)==1)
%             coeffs = zeros(1,46);
%             %coeffs(end-num_coeff+1:end) = Xopt(Xind:Xind+(num_coeff-1));
%             print=[channelIDs(i,:)-1 coeffs];
%             fprintf(outf,'%4.2d %5.2d %4.2d %7.2d %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e',print);
%             fprintf(outf, '\n');
%             Xind=Xind+num_coeff;
%         else
%             % Since MATLAB chooses the (C/A) parameter to be the constant for
%             % cathodes during optimization, when writing to file, we shift it
%             % to the right column and put zeros as appropriate.
%             coeffs = zeros(1,46);
%             %coeffs(end-num_coeff+1:end) = Xopt(Xind:Xind+(num_coeff-1));
%             coeffs(1:end) = Xopt(Xind:Xind+(num_coeff-1));
%             print=[channelIDs(i,:)-1 coeffs];
%             fprintf(outf,'%4.2d %5.2d %4.2d %7.2d %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e',print);
%             %Cathodes have 2 fewer parameters
%             fprintf(outf, '\n');
%             Xind=Xind+num_coeff;
%         end
%     end
%     fclose(outf);display('Finished exporting parameters to .tcc file...');
% end
