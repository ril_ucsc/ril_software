function Parameters = importDcc(Parameters)

% Parameters = importDcc(Parameters)
% 
% The function imports the dcc anode signal deficit calibration parameters
% from a *.dcc file. This is done from a *.dcc in the current directory, if
% it exists. If none or multiple *.dcc files exist in the current
% directory, then a dialog box is opened to allow the user to select the
% *.dcc file to import.
%
% importTec must have been called on Parameters before invoking this
% function.
%
% The input Parameters must be a cell array with structure
% Parameters{node}{board}{rena}{channel} = [1 x 13 vector], originally
% all initialized to -1. In the returned Parameters, the *.dcc parameters
% are placed into element positions 10 to 12 of the [1 x 13 vector]
% associated with each channel. Element position 13 is a status flag, with
% 1 indicating valid parameters.

fprintf('\n');
display('Starting depth correction parameter import...');

% Find .dcc filename and open it
filename = dir('*.dcc');

if(length(filename) ~= 1)
    display('Current scope has either too many or no .dcc files');
    
    % Prompt user to select file
    [filename, pathname] = uigetfile('*.dcc', 'Select a depth correction coefficient file');
    
    if isequal(filename, 0)
        disp('No .dcc file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    
    % Check if selected file is valid
    fileinfo = dir([pathname filename]);
    if (fileinfo.bytes==0)
        error('Empty .dcc file');
    end
    
    % Open file
    DCCfile = fopen([pathname filename],'r');
else
    if (filename.bytes == 0)
        error('Empty .dcc file');
    end
    disp(['Using ', filename.name])
    
    % Open file
    DCCfile=fopen(filename.name,'r');
end

tic

addDCC = 0;          % Number of channels whos dcc parameters were added
missingTEC = 0;      % Number of channels found in .dcc file that have missing
                     % UV ellipse correction parameters
line=fgetl(DCCfile); %Get first line

while ischar(line)
    dcc=textscan(line,'%d %d %d %d %f %f %f');
    
    % If the file uses a -1 node then make it 1.
    if(dcc{1}==-1)
        dcc{1}=1;
    else
        dcc{1}=dcc{1}+1;
    end
    
    % Adjust channelID info to work with MATLAB style vector notation (vectors start with 1 not 0)
    dcc{2}=dcc{2}+1;
    dcc{3}=dcc{3}+1;
    dcc{4}=dcc{4}+1;
    
    % Invalid or missing tec parameters means the dcc parameters should not be saved either
    % Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
    if(Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(5) == -1)
        missingTEC = missingTEC+1;
        line=fgetl(DCCfile);
        continue;   
    end
    
    % Add parameters to parameter map
    % dcc parameter status (1: parameters are good)
    Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(10:12)=[dcc{5} dcc{6} dcc{7}];
    Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(13)=1;
    addDCC = addDCC+1;
    line = fgetl(DCCfile);
end
fclose(DCCfile);
display(['Added correction parameters for ' num2str(addDCC) ' channels.']);
if(missingTEC~=0)
    warning(['Dropped parameters for ' num2str(missingTEC) ' channels because of missing or invalid TEC parameters.']);
end

display('Finished importing depth correction parameters...');
fprintf('\n');

toc
