uvFreq = 490e3;
numNodes = 2;       % Nodes are IDed 1, 2, 3, 4
numBoards = 4;
numCathodes=8;
n=numNodes*numBoards*numCathodes;
binh_all=[];
max_iter=20;
mean_dt=zeros(2,4,2,28);

%correct u/v centers
%Loop over nodes in data
 for i = 1:2 %node 1 and 2
    % Loop over data in each node
    for j = 1:size(dataByNode{i}.ca, 1)
    	nodeIDC = dataByNode{i}.ca(j, 1) + 1;
     	brdIDC = dataByNode{i}.ca(j, 2) + 1;
        renaIDC = dataByNode{i}.ca(j, 3) + 1;
        chanIDC = dataByNode{i}.ca(j, 4) + 1;
        uCenter = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(1);
        vCenter = Parameters{nodeIDC}{brdIDC}{renaIDC}{chanIDC}(2);
       dataByNode{i}.ca(j, 7) = dataByNode{i}.ca(j, 7) - uCenter;
       dataByNode{i}.ca(j, 8) = dataByNode{i}.ca(j, 8) - vCenter; 
    end 
end
Nopp=[2,1];

%get uncalibrated dtcc
uncalib = [];
calib = [];
%go over all data and find dt
for j=1:length(dataByNode{1}.ca)
    tStampC1 = dataByNode{1}.ca(j, 7) + 1i*dataByNode{1}.ca(j, 8);
    tStampC2 = dataByNode{2}.ca(j, 7) + 1i*dataByNode{2}.ca(j, 8);
    dtcc = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
    uncalib = [uncalib; dtcc];
end   
dtccnew = uncalib;

for num_iter=1:max_iter

   for N=[1,2]
       for B=[0,1,16,17]
           N1B=dataByNode{1,N}.ca(dataByNode{1,N}.ca(:,2)==B,:); %board = B
           N2_N1B=dataByNode{1,Nopp(N)}.ca(dataByNode{1,N}.ca(:,2)==B,:);
           uncalib_N1B = dtccnew(dataByNode{1,N}.ca(:,2)==B);
           
           N1BR0=N1B(N1B(:,3)==0,:); %rena0
           N2_N1BR0=N2_N1B(N1B(:,3)==0,:);
           uncalib_N1BR0 = uncalib_N1B(N1B(:,3)==0);
           N1BR1=N1B(N1B(:,3)==1,:); %rena1
           N2_N1BR1=N2_N1B(N1B(:,3)==1,:);
           uncalib_N1BR1 = uncalib_N1B(N1B(:,3)==1);
           
            if mod(B,2)==0
               %figure
            renas = [0,1,1,1,0,0,0,1];
               channels = [28,26,27,28,27,26,25,25];
                for cs=1:length(renas)
                    if renas(cs) == 0 %if this channl's rena is R0
                        N1BRx = N1BR0;
                        N2_N1BRx = N2_N1BR0;
                        uncalib_N1BRx = uncalib_N1BR0;
                    else
                        N1BRx = N1BR1;
                        N2_N1BRx = N2_N1BR1;
                        uncalib_N1BRx = uncalib_N1BR1;
                    end
                    C = N1BRx(N1BRx(:,4)==channels(cs),:);
                    N2_C = N2_N1BRx(N1BRx(:,4)==channels(cs),:);
                    uncalib_C = uncalib_N1BRx(N1BRx(:,4)==channels(cs));
                    %tStampC1 = C(:, 7) + 1i*C(:, 8);
                    %tStampC2 = N2_C(:, 7) + 1i*N2_C(:, 8);
                    %dtcc = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
                    [binh binx]=hist(uncalib_C,-1000:2:1000);
                    subplot(2,4,cs)
                    plot(binx, binh, '.')
                    meanC = mean(uncalib_C);
                    title(['N' num2str(N) 'B' num2str(B) ' C' num2str(cs) ', avg= ' num2str(round(meanC))])
                    if B == 16
                        Bval = 3;
                    elseif B == 17
                         Bval = 4;
                    elseif B ==0
                        Bval = 1;
                    else
                        Bval = 2;
                    end
                    mean_dt(N,Bval,renas(cs)+1,channels(cs)) = meanC;
                    binh_all = [binh_all; binh];
                end
           else %for odd boards
               %figure
                renas = [1,0,0,0,1,1,1,0];
               channels = [7,5,6,7,10,9,8,4];
                for cs=1:length(renas)
                    if renas(cs) == 0 %if this channl's rena is R0
                        N1BRx = N1BR0;
                        N2_N1BRx = N2_N1BR0;
                        uncalib_N1BRx = uncalib_N1BR0;
                    else
                        N1BRx = N1BR1;
                        N2_N1BRx = N2_N1BR1;
                        uncalib_N1BRx = uncalib_N1BR1;
                    end
                    C = N1BRx(N1BRx(:,4)==channels(cs),:);
                    N2_C = N2_N1BRx(N1BRx(:,4)==channels(cs),:);
                    %tStampC1 = C(:, 7) + 1i*C(:, 8);
                    %tStampC2 = N2_C(:, 7) + 1i*N2_C(:, 8);
                    %dtcc = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
                    uncalib_C = uncalib_N1BRx(N1BRx(:,4)==channels(cs));
                    
                    [binh binx]=hist(uncalib_C,-1000:2:1000);
                     subplot(2,4,cs)
                     plot(binx, binh, '.')
                    meanC = mean(uncalib_C);
                    title(['N' num2str(N) 'B' num2str(B) ' C' num2str(cs) ', avg= ' num2str(round(meanC))])
                    if B == 16
                        Bval = 3;
                    elseif B == 17
                         Bval = 4;
                    elseif B ==0
                        Bval = 1;
                    else
                        Bval = 2;
                    end
                    mean_dt(N,Bval,renas(cs)+1,channels(cs)) = meanC;
                    binh_all = [binh_all; binh];
                end
            end
        end
   end

    %go over all data and find dt
    for j=1:length(dataByNode{1}.ca)

        if dataByNode{1}.ca(j, 2) == 16
                    Bval = 3;
                elseif dataByNode{1}.ca(j, 2) == 17
                     Bval = 4;
                elseif dataByNode{1}.ca(j, 2) ==0
                    Bval = 1;
                else
                    Bval = 2;
        end
        Cmean1 = mean_dt(dataByNode{1}.ca(j, 1), Bval,dataByNode{1}.ca(j, 3)+1,dataByNode{1}.ca(j, 4));
        if dataByNode{2}.ca(j, 2) == 16
                    Bval = 3;
                elseif dataByNode{2}.ca(j, 2) == 17
                     Bval = 4;
                elseif dataByNode{2}.ca(j, 2) ==0
                    Bval = 1;
                else
                    Bval = 2;
        end
        Cmean2 = mean_dt(dataByNode{2}.ca(j, 1), Bval,dataByNode{2}.ca(j, 3)+1,dataByNode{2}.ca(j, 4));
        dtccnew(j) = dtccnew(j) -(Cmean1 + Cmean2);
    end   
    [bin_unh bin_unx]=hist(uncalib,-1000:2:1000);
    figure;
    plot(bin_unx, bin_unh,'.')
    title(['iteration ' num2str(num_iter)])
    [bin_calibh bin_calibx]=hist(dtccnew,-1000:2:1000);
    hold on;
    plot(bin_calibx, bin_calibh,'k.')

    %remove some lines
    lo_lim = 0.95*min(dtccnew);
    hi_lim = 0.95*max(dtccnew);
    dataByNode{1,1}.ca = dataByNode{1,1}.ca(dtccnew<=hi_lim & dtccnew>=lo_lim,:); 
    %dataByNode{1,1}.ca = dataByNode{1,1}.ca(dtccnew<=hi_lim,:);
    dataByNode{1,2}.ca = dataByNode{1,2}.ca(dtccnew>=lo_lim & dtccnew<=hi_lim,:);
    %dataByNode{1,2}.ca = dataByNode{1,2}.ca(dtccnew<=hi_lim,:);
    
    dtccnew = dtccnew(dtccnew>=lo_lim & dtccnew<=hi_lim);
    %dtccnew = dtccnew(dtccnew<=hi_lim);
    
end

%        %cathode 1
%        C1=N1BR0(N1BR0(:,4)==28,:);
%        N2_C1=N2_N1BR0(N1BR0(:,4)==28,:);
%        tStampC1 = C1(:, 7) + 1i*C1(:, 8);
%        tStampC2 = N2_C1(:, 7) + 1i*N2_C1(:, 8);
%        dtcc1 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh1 binx]=hist(dtcc1,-1000:2:1000);
%        figure
%        plot(binx, binh1, '.')
%        meanC1 = mean(dtcc1);
%        title(['N1B' num2str(B) ' C1   mean= ' num2str(meanC1)])
%        mean_v=[mean_v meanC1]
%        %cathode 2
%        C2=N1BR1(N1BR1(:,4)==26,:);
%        N2_C2=N2_N1BR1(N1BR1(:,4)==26,:);
%        tStampC1 = C2(:, 7) + 1i*C2(:, 8);
%        tStampC2 = N2_C2(:, 7) + 1i*N2_C2(:, 8);
%        dtcc2 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh2 binx]=hist(dtcc2,-1000:2:1000);
%        figure
%        plot(binx, binh2, '.')
%        meanC2 = mean(dtcc2);
%        title(['N1B' num2str(B) ' C2   mean= ' num2str(meanC2)])
%        mean_v=[mean_v meanC2]
%        %cathode 3
%        C3=N1BR1(N1BR1(:,4)==27,:);
%        N2_C3=N2_N1BR1(N1BR1(:,4)==27,:);
%        tStampC1 = C3(:, 7) + 1i*C3(:, 8);
%        tStampC2 = N2_C3(:, 7) + 1i*N2_C3(:, 8);
%        dtcc3 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh3 binx]=hist(dtcc3,-1000:2:1000);
%        figure
%        plot(binx, binh3, '.')
%        meanC3 = mean(dtcc3);
%        title(['N1B' num2str(B) ' C3   mean= ' num2str(meanC3)])
%        mean_v=[mean_v meanC3]
%        %cathode4
%        C4=N1BR1(N1BR1(:,4)==28,:);
%        N2_C4=N2_N1BR1(N1BR1(:,4)==28,:);
%        tStampC1 = C4(:, 7) + 1i*C4(:, 8);
%        tStampC2 = N2_C4(:, 7) + 1i*N2_C4(:, 8);
%        dtcc4 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh4 binx]=hist(dtcc4,-1000:2:1000);
%        figure
%        plot(binx, binh4, '.')
%        meanC4 = mean(dtcc4);
%        title(['N1B' num2str(B) ' C4   mean= ' num2str(meanC4)])
%        mean_v=[mean_v meanC4]
%        
%        %cathode 5
%        C5=N1BR0(N1BR0(:,4)==27,:);
%        N2_C5=N2_N1BR0(N1BR0(:,4)==27,:);
%        tStampC1 = C5(:, 7) + 1i*C5(:, 8);
%        tStampC2 = N2_C5(:, 7) + 1i*N2_C5(:, 8);
%        dtcc5 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh5 binx]=hist(dtcc5,-1000:2:1000);
%        figure
%        plot(binx, binh5, '.')
%        meanC5 = mean(dtcc5);
%        title(['N1B' num2str(B) ' C5   mean= ' num2str(meanC5)])
%        mean_v=[mean_v meanC5]
%        %cathode 6
%        C6=N1BR0(N1BR0(:,4)==26,:);
%        N2_C6=N2_N1BR0(N1BR0(:,4)==26,:);
%        tStampC1 = C6(:, 7) + 1i*C6(:, 8);
%        tStampC2 = N2_C6(:, 7) + 1i*N2_C6(:, 8);
%        dtcc6 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh6 binx]=hist(dtcc6,-1000:2:1000);
%        figure
%        plot(binx, binh6, '.')
%        meanC6 = mean(dtcc6);
%        title(['N1B' num2str(B) ' C6   mean= ' num2str(meanC6)])
%        mean_v=[mean_v meanC6]
%        %cathode 7
%        C7=N1BR0(N1BR0(:,4)==25,:);
%        N2_C7=N2_N1BR0(N1BR0(:,4)==25,:);
%        tStampC1 = C7(:, 7) + 1i*C7(:, 8);
%        tStampC2 = N2_C7(:, 7) + 1i*N2_C7(:, 8);
%        dtcc7 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh7 binx]=hist(dtcc7,-1000:2:1000);
%        figure
%        plot(binx, binh7, '.')
%        meanC7 = mean(dtcc7);
%        title(['N1B' num2str(B) ' C7   mean= ' num2str(meanC7)])
%        mean_v=[mean_v meanC7]
%        %cathode8
%        C8=N1BR1(N1BR1(:,4)==25,:);
%        N2_C8=N2_N1BR1(N1BR1(:,4)==25,:);
%        tStampC1 = C8(:, 7) + 1i*C8(:, 8);
%        tStampC2 = N2_C8(:, 7) + 1i*N2_C8(:, 8);
%        dtcc8 = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
%        [binh8 binx]=hist(dtcc8,-1000:2:1000);
%        figure
%        plot(binx, binh8, '.')
%        meanC8 = mean(dtcc8);
%        title(['N1B' num2str(B) ' C8   mean= ' num2str(meanC8)])
%        mean_v=[mean_v meanC8]






%%
uvFreq = 490e3; 
N1=1;
B1=17;
R1=1;
C1=7;
NB1=dataByNode{1,N1}.ca(dataByNode{1,N1}.ca(:,2)==B1,:);
NB1_a=dataByNode{1,N1}.an(dataByNode{1,N1}.ca(:,2)==B1,:);
NBR1=NB1(NB1(:,3)==R1,:);
NBR1_a=NB1_a(NB1(:,3)==R1,:);
NBRC1=NBR1(NBR1(:,4)==C1,:);
NBRC1_a=NBR1_a(NBR1(:,4)==C1,:);
%figure
%hist(NBRC1(:,6)./NBRC1_a(:,6),0:0.05:2)

N2=2;
NB2=dataByNode{1,N2}.ca(dataByNode{1,N1}.ca(:,2)==B1,:);
NB2_a=dataByNode{1,N2}.an(dataByNode{1,N1}.ca(:,2)==B1,:);
NBR2=NB2(NB1(:,3)==R1,:);
NBR2_a=NB2_a(NB1(:,3)==R1,:);
NBRC2=NBR2(NBR1(:,4)==C1,:);
NBRC2_a=NBR2_a(NBR1(:,4)==C1,:);
%figure
%hist(NBRC2(:,6)./NBRC2_a(:,6),0:0.05:2)

%time difference
tCN1 = NBRC1(:, 7) + 1i*NBRC1(:, 8);
tCN2 = NBRC2(:, 7) + 1i*NBRC2(:, 8);
dtCCN1N2 = angle(tCN1./tCN2)/2/pi/uvFreq*1e9;
%time difference whole CC
tStampC1 = dataByNode{1}.ca(:, 7) + 1i*dataByNode{1}.ca(:, 8);
tStampC2 = dataByNode{2}.ca(:, 7) + 1i*dataByNode{2}.ca(:, 8);
dtcc = angle(tStampC1./tStampC2)/2/pi/uvFreq*1e9;
figure;
[h c] = hist(dtcc, -1000:2:1000);
plot(c, h, 'r.');
hold on
%figure;
[h c] = hist(dtCCN1N2, -1000:2:1000);
plot(c, h , 'b.');
xlabel(['cc']);

%dt vc C2A
figure
subplot(211)
C2AN1 = NBRC1(:,6)./NBRC1_a(:,6);
C2AN2 = NBRC2(:,6)./NBRC2_a(:,6);
plot(C2AN1,dtCCN1N2,'b.')
xlim([0,1.2])
title('Uncalibrated dt of one cathode in N1 vs C2A ratio')
hold on
subplot(212)
plot(C2AN1.*C2AN1,dtCCN1N2,'b.')
title('Uncalibrated dt of one cathode in N1 vs C2A^2')
xlim([0,1.44])
%subplot(212)
%plot(C2AN2,dtCCN1N2,'r.')
%title('Uncalibrated dt of all the coincidences in N2 with one cathode in N1 vs C2A ratio')

%dt vs cathode & anode energy
figure
subplot(211)
plot(NBRC1(:,6),dtCCN1N2,'b.')
title('uncalibrated dt of one cathode vs one cathode energy')
subplot(212)
plot(NBRC1_a(:,6),dtCCN1N2,'r.')
title('uncalibrated dt of one cathode vs anode energy from anodes with the same cathode')

%dt versus UV for one cathode in N1 and everything incoincident N2
figure
subplot(221)
U_NBRC1 = NBRC1(:,7);
plot(NBRC1(:,7),dtCCN1N2,'b.')
xlabel('U')
subplot(222)
plot(NBRC1(:,8),dtCCN1N2,'b.')
xlabel('V')
subplot(223)
plot(NBRC1(:,7).*NBRC1(:,7),dtCCN1N2,'b.')
xlabel('U^2')
subplot(224)
plot(NBRC1(:,7).*NBRC1(:,8),dtCCN1N2,'b.')
xlabel('UV')

%finding two cathodetime window versus UV
B2=0;
R2=0;
C2=28;
N2B=NBRC2(NBRC2(:,2)==B2,:);
NBRC1_N2B=NBRC1(NBRC2(:,2)==B2,:);
N2B2R=N2B(N2B(:,3)==R2,:);
NBRC1_N2BR=NBRC1_N2B(N2B(:,3)==R2,:);
N2B2RC=N2B2R(N2B2R(:,4)==28,:);
NBRC1_N2BRC=NBRC1_N2BR(N2B2R(:,4)==28,:);
tCN1_N1C = NBRC1_N2BRC(:, 7) + 1i*NBRC1_N2BRC(:, 8);
tCN2_N2C = N2B2RC(:, 7) + 1i*N2B2RC(:, 8);
dtCCN1N2_cc = angle(tCN1_N1C./tCN2_N2C)/2/pi/uvFreq*1e9;
figure;
[h c] = hist(dtCCN1N2_cc, -1000:2:1000);
plot(c, h , 'b.');
xlabel(['cc']);
%plot dt_two specific cathode forN1N2 versus C1 U
figure;
subplot(221)
plot(NBRC1_N2BRC(:,7),dtCCN1N2_cc,'.')
xlabel('U')
subplot(222)
plot(NBRC1_N2BRC(:,8),dtCCN1N2_cc,'.')
xlabel('V')
subplot(223)
plot(NBRC1_N2BRC(:,7).*NBRC1_N2BRC(:,7),dtCCN1N2_cc,'b.')
xlabel('U^2')
subplot(224)
plot(NBRC1_N2BRC(:,7).*NBRC1_N2BRC(:,8),dtCCN1N2_cc,'b.')
xlabel('UV')
figure
plot(NBRC1_N2BRC(:,7),NBRC1_N2BRC(:,8),'.')



figure
plot3(NBRC1(:,7),NBRC1(:,8),dtCCN1N2,'b.')

figure
plot(NBRC1(:,7),NBRC1(:,8),'.')
xlabel(['U'], 'fontsize',28);
ylabel(['V'],'fontsize',28);
set(gca,'lineWidth',3)
set(gca,'fontsize', 28)

figure
plot(NBRC1(:,7),dtCCN1N2,'.')
xlabel(['U'], 'fontsize',28);
ylabel(['dt (ns)'],'fontsize',28);
set(gca,'lineWidth',3)
set(gca,'fontsize', 28)


figure
plot(NBRC1(:,7),NBRC1(:,8),'r.')
%%selecting one cathode with specific anode in N1 with all in N2
NBRC1_a_temp=NBRC1_a(NBRC1_a(:,3)==0,:);
NBRC1_temp=NBRC1(NBRC1_a(:,3)==0,:);
NBRC2_a_temp = NBRC2_a(NBRC1_a(:,3)==0,:);
NBRC2_temp=NBRC2(NBRC1_a(:,3)==0,:);

NBRC1_a_anode=NBRC1_a_temp(NBRC1_a_temp(:,4)==14,:);
NBRC1_Cindx_anode=NBRC1_temp(NBRC1_a_temp(:,4)==14,:);
NBRC2_a_anode=NBRC2_a_temp(NBRC1_a_temp(:,4)==14,:);
%NBRC2_Cindx_anode=NBRC2_temp(NBRC1_a_temp(:,4)==14,:);

%find anode on N2
NBRC2_a_anode_N2_temp=NBRC2_a_anode(NBRC2_a_anode(:,3)==0,:);
NBRC1_Cindx_anode_N2_temp=NBRC1_Cindx_anode(NBRC2_a_anode(:,3)==0,:);
NBRC2_a_anode_N2_anode=NBRC2_a_anode_N2_temp(NBRC2_a_anode_N2_temp(:,4)==11,:);
NBRC1_Cindx_anode_N2_anode=NBRC1_Cindx_anode_N2_temp(NBRC2_a_anode_N2_temp(:,4)==11,:);
figure;
plot(NBRC1_Cindx_anode(:,7),NBRC1_Cindx_anode(:,8),'b.')
hold on
plot(NBRC1_Cindx_anode_N2_anode(:,7),NBRC1_Cindx_anode_N2_anode(:,8),'r.')
title('UV for one an/ca pair in N1 and all in N2')
%%
UN1=dataByNode{1,1}.ca(:,7);
VN1=dataByNode{1,1}.ca(:,8);
figure
plot3(UN1,VN1, dtcc,'.')

plot(UN1, dtcc,'.')
%%
figure
tempsel = sel(3:3:end);
plot(dataByNode{1,1}.ca(tempsel,7),dataByNode{1,1}.ca(tempsel,8),'.')
set(gca,'lineWidth',3)
set(gca,'fontsize', 28)
figure;
[binH, binC] = hist(temp, binC);
plot(binC, binH);
set(gca,'lineWidth',3)
set(gca,'fontsize', 28)
figure
tempsel2 = sel(1:3:end);
plot(dataByNode{1,1}.an(tempsel2,7),dataByNode{1,1}.an(tempsel2,8),'.')
figure
tempsel3 = sel(2:3:end);
plot(dataByNode{1,2}.an(tempsel3,7),dataByNode{1,2}.an(tempsel3,8),'.')