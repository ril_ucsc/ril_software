fid = fopen('PED_Data_1502271856_L0_0.dat');
data = fread(fid);
fclose(fid);

start = find(data == 128);

data = data(min(start):max(start)-1);
start = find(data == 128);
check = diff(start);
baad = check ~= 138;
start(baad) =[];

packets = zeros(138,length(start));
for i=1:length(start)
    packets(:,i) = data(start(i):start(i)+137);
end

timestamp = packets(4,:)*(2^31) + packets(5,:)*(2^28) + packets(6,:)*(2^21) + packets(7,:)*(2^14)...
            + packets(8,:)*(2^7) + packets(9,:);
        
timestamp = timestamp/48000000*4;

data_rate = length(timestamp)/(max(timestamp)-min(timestamp))/16

%packets = reshape(data,138,length(data)/138);

%get rid of the pedestal.
%packets = packets(:,20000:end);
chipid = packets(3,:);

%only packets from one chip
%1-8
subset = ((chipid-15)/16 == 5);  
packets = packets(:,subset);

source = packets(2,:);

timestamp = packets(4,:)*(2^31) + packets(5,:)*(2^28) + packets(6,:)*(2^21) + packets(7,:)*(2^14)...
            + packets(8,:)*(2^7) + packets(9,:);
        
timestamp = timestamp/48000000*4;


top = source == 19;
bot = source == 24;

tops = packets(:,top);
bots = packets(:,bot);

ts_top = timestamp(top);
ts_bot = timestamp(bot);

i_top = 1;
i_bot = 1;

pair = [];
delta = [];

while i_top < length(ts_top) && i_bot < length(ts_bot)
    
    temp = ts_top(i_top) - ts_bot(i_bot);
    
    if 35 < temp && temp < 50
        
       pair = [pair; i_top i_bot];
       delta = [delta; ts_top(i_top) - ts_bot(i_bot)];
       
       i_top = i_top+1;
       i_bot = i_bot+1;
    else
        if ts_top(i_top) > ts_bot(i_bot)
            i_bot = i_bot+1;
        else
             i_top = i_top+1;
        end
    end
    
end
%%

%44,56,68,80,92,104,116,128
i=44;
%i = 128-i;

%for i=4:2:132
e0 = tops(i-2,pair(:,1))*(2^6) + tops(i-1,pair(:,1));
u0 = tops(i,pair(:,1))*(2^6) + tops(i+1,pair(:,1));
v0 = tops(i+2,pair(:,1))*(2^6) + tops(i+3,pair(:,1));
ee0 = tops(i+4,pair(:,1))*(2^6) + tops(i+5,pair(:,1));
    
e1 = bots(i-2,pair(:,2))*(2^6) + bots(i-1,pair(:,2));
u1 = bots(i,pair(:,2))*(2^6) + bots(i+1,pair(:,2));
v1 = bots(i+2,pair(:,2))*(2^6) + bots(i+3,pair(:,2));
ee1 = bots(i+4,pair(:,2))*(2^6) + bots(i+5,pair(:,2));

% i=104;
% e1 = tops(i-2,pair(:,1))*(2^6) + tops(i-1,pair(:,1));
% u1 = tops(i,pair(:,1))*(2^6) + tops(i+1,pair(:,1));
% v1 = tops(i+2,pair(:,1))*(2^6) + tops(i+3,pair(:,1));
% ee1 = tops(i+4,pair(:,1))*(2^6) + tops(i+5,pair(:,1));

clf
subplot(121) 
plot(u1,v1,'.')
hold on 
plot(u0,v0,'r.')
subplot(122) 
hist(e0, 1:4095)
hold on 
%hist(e1)
title(i)
%pause
%end



e0 =e0';
u0 = u0' - mean(u0);
v0 = v0' - mean(v0);
e1 =e1';
u1 = u1' - mean(u1);
v1 = v1' - mean(v1);

L0 = mean(sqrt(u0.^2+v0.^2));
L1 = mean(sqrt(u1.^2+v1.^2));

u0 = u0/L0;
v0 = v0/L0;
u1 = u1/L1;
v1 = v1/L1;



delta= (u0+sqrt(-1)*v0)./(u1+sqrt(-1)*(v1));

tdiff= angle(delta)/2/pi/980000*1e9;

phase0 = atan2(u0,v0);
phase1 = atan2(u1,v1);



A = [cos(phase0) sin(phase0) sin(phase0).^2 sin(phase0).*cos(phase0) ones(size(phase0))];
A2 = [cos(phase1) sin(phase1) sin(phase1).^2 sin(phase1).*cos(phase1)];

A=[A -A2];

y = tdiff;


x=A\y;

adj = y-A*x;



[na xa] = hist(adj,100);


% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( xa, na );

% Set up fittype and options.
ft = fittype( 'gauss1' );
opts = fitoptions( ft );
opts.Display = 'Off';
opts.Lower = [-Inf -Inf 0];
%opts.StartPoint = [168 -0.0637190681743075 0.465617328287129];
opts.StartPoint = [168 0 0.465617328287129];
opts.Upper = [Inf Inf Inf];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'na vs. xa', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel( 'xa' );
xlim([-20 20]);
ylabel( 'na' );
grid on

fwhm = fitresult.c1/sqrt(2)*2.3548;
midpt = fitresult.b1;

title(['Mid =' num2str(midpt) ', FWHM = ' num2str(fwhm)])


