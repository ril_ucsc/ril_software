function [loLim, hiLim, randPedestal] = findWindow(data, numStds, numBins);

% [loLim, hiLim] = findWindow(data, numStds, numBins);
% 
% Given data, this function fits a gaussian curve to a histogram of the
% data and returns the values of loLim and hiLim, which are, respectively,
% values on the histogram's x axis which is numStds below and above the
% mean. numBins specifies the number of bins to use for the histogram.

debug =1;

binCenter = linspace(min(data), max(data), numBins);
[binHeight, binCenter] = hist(data, binCenter);

%==========================================================================
% Iteratively find the pedestal of randoms
maxHeight = max(binHeight);
compLog = zeros(1, 101);
for threshInd = 0:100
    compLog(threshInd + 1) = length(find(binHeight >= maxHeight*(1 - threshInd*0.01)));
end

% Estimate the instantaneous gradient by taking a moving window of 7
% samples
for i = 1:95
    tempX = [i:i + 6];
    tempY = compLog(i:i + 6);
    temp = polyfit(tempX, tempY, 1);
    myGrad(i) = temp(1);
end

% Detect presence of pedestal by thresholding on coefficient of
% determination (R^2). Basically we check how well myGrad can be modelled
% by a line. If there is a pedestal, there would be a knick point
% where the number of points above threshold rapidly increases. If we're
% looking at a normal peal without pedestal, then the rise if more
% constant.
coeffs = polyfit([1:90], compLog(1:90), 1);
compLogFit = polyval(coeffs, [1:90]);
compLogMean = mean(compLog(1:90));
COD = 1 - sum((compLog(1:90) - compLogFit).^2)/sum((compLog(1:90) - compLogMean).^2);
%==========================================================================
% Tune the threshold here
% Nominal value is 0.84
if (COD <= 0.94)
	% Weigh the index by square of the gradient
	myGrad = myGrad.^2;
	myGrad = myGrad/sum(myGrad);
	optThreshInd = round(sum(myGrad.*[4:98]));
	randPedestal = maxHeight*(1 - optThreshInd*0.01)*0.8;
	binHeight = binHeight - randPedestal;
else
    optThreshInd = 98;
    randPedestal = 0;
end

% Debug
if (debug > 0)
	figure;
	plot(compLog, 'o');
	hold on; grid on;
	plot([4:98], myGrad/max(myGrad)*max(compLog)/2, 'r');
	plot([optThreshInd optThreshInd], [0 max(compLog)], 'k');
	plot([1:101], polyval(coeffs, [1:101]), 'r', 'LineWidth', 2);
	xlabel(['R^2 error = ' num2str(COD)]);
	
	figure;
	hist(data, binCenter);
	hold on; grid on;
	plot([min(binCenter) max(binCenter)], [randPedestal randPedestal], 'r');
	axis tight;
end

%==========================================================================
% Perform gaussian fit
startPt = binCenter(find(binHeight == max(binHeight)));
startPt = startPt(1);

s = fitoptions('Method', 'NonlinearLeastSquares',...
               'Lower', [0 -700 0],...
               'Upper', [max(binHeight) 700 500],...
               'Startpoint', [max(binHeight)*0.9 startPt 10],...
               'MaxFunEvals', 100000,...
               'MaxIter', 100000);
f = fittype('a*exp(-0.5*((b - x)/c)^2)', 'options', s);
[fittedModel, goodnessOfFit] = fit(binCenter', binHeight', f);

% Debug
if (debug > 0)
    temp = linspace(min(data), max(data), numBins*10);
    figure;
    plot(binCenter, binHeight, 'o');
    hold on; grid on;
    plot(temp, fittedModel.a*exp(-0.5*((fittedModel.b - temp)/fittedModel.c).^2), 'r', 'LineWidth', 2);
end

%==========================================================================
% Assign outputs
loLim = max([-500 fittedModel.b - numStds*fittedModel.c]);
hiLim = min([500 fittedModel.b + numStds*fittedModel.c]);