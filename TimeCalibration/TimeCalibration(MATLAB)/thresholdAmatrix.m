function [A, Y, chanList, colsRemd, numColsRemd] = thresholdAmatrix(A, Y, chanList, thresh, Parameters, numMTinEvent)

colsRemd = []; %List of columns removed (in order of removal)
numColsRemd = []; %List of number of columns removed per iteration of thresholding 
                  %(i.e. in the first iteration, colsRemd(1:numColsRemd(1))
                  %were the columns that were removed, and
                  %colsRemd(2:numColsRemd(2)) were removed in the second
                  %iteration 

while(1)
    Acol2chanIndx = [];
    %Create vector to keep track of which columns go to which channels
    %Here we assume the A matrix columns are ordered by channel as they appear
    %in the chanList (i.e. if the first channel in the list is an anode, then
    %the first 7 columns of A correspond to this channel).
    [numChans, ~] = size(chanList);
    for i = 1:numChans
        id = chanList(i,:);
        if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(9)==0) %Cathode
            Acol2chanIndx = [Acol2chanIndx i*ones(1,5)];
        else
            Acol2chanIndx = [Acol2chanIndx i*ones(1,7)];
        end
    end
    Acol2chanIndx = Acol2chanIndx(1:end-1); %Remove last element because last channel of A is used as reference so it's missing a constant term
    
    row2del = [];
    col2del = [];
    [R, C, S]=find(A);
    [numRows, numCols] = size(A);
    if(numRows == 0 || numCols == 0)
        break;
    end
    [binHeights Acols]=hist(C,1:numCols);
    ind=find(binHeights<=thresh);
    if(numCols < length(Acol2chanIndx)) %This occurs when channels with no events have columns on the right hand side of A
        ind = [ind numCols+2:length(Acol2chanIndx)];
    end
    if(isempty(ind))
        break;
    end
    ids = unique(Acol2chanIndx(ind)); %indices of channels in the chanList to remove from the matrix
    for i = 1:length(ids)
        warning(['Dropping coefficient matrix values associated with node ' num2str(chanList(ids(i),1)-1) ' board ' num2str(chanList(ids(i),2)-1)...
            ' rena ' num2str(chanList(ids(i),3)-1) ' channel ' num2str(chanList(ids(i),4)-1) ' because of insufficient number of valid events.']);
        chanList(ids(i), 1) = -1;
    end
    for i = 1:length(ind)
        temp=find(C==ind(i));   %Find the columns of the elements to be removed and their row numbers as well
        col2del=[col2del C(temp)'];
        row2del=[row2del R(temp)'];
        if(isempty(temp))
            col2del=[col2del ind(i)];
        end
    end
    col2del=unique(col2del);
    row2del=unique(row2del);
    index=find(chanList(:,1)==-1);
    chanList(index,:)=[];
    if(numMTinEvent == 4)   % If we are working with coincidence data, we must remove rows in groups of threes so for each row to delete, determine the other two
        rowPos = mod(row2del,3);
        temprow2del = [];
        for i = 1:length(row2del)
            if(rowPos(i) == 0)
                temprow2del = [temprow2del row2del(i)-2 row2del(i)-1];
            elseif(rowPos(i) == 2)
                temprow2del = [temprow2del row2del(i)-1 row2del(i)+1];
            elseif(rowPos(i) == 1)
                temprow2del = [temprow2del row2del(i)+1 row2del(i)+2];
            end
        end
        row2del = [row2del temprow2del];
        row2del = unique(row2del);
    end
    
    Y(row2del)=[];
    [numR numC]=size(A);
    col2del=col2del(find(col2del<=numC));   %Make sure column and row numbers are valid
    row2del=row2del(find(row2del<=numR));
    colsRemd = [colsRemd col2del];
    numColsRemd = [numColsRemd length(col2del)];
    A(:,col2del)=[];
    A(row2del,:)=[];
end

end