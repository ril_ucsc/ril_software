%% Find the channel indexes of coincid*ence pairs with the k highest hits
k = 10;
numBoards = 5;
genCoinMatrix;
load CoinMatrix;
localCoin = coinMatrix;

chans = [];
numHits = [];
i = 0;
while( i < k)
    [rs, cs] = find(localCoin == max(localCoin(:)));
    if(numel(rs) == 0)
        break;
    end
    for j = 1:numel(rs)
        anNum_nd3 = mtrx_Anodes(rs(j));
        caNum_nd3 = mtrx_Cathodes(rs(j));
        bd_nd3 = floor(rs(j)/(39*8));
        if(bd_nd3 == 0)
            anIndx_nd3 = AnMask_Ev(anNum_nd3);
            caIndx_nd3 = CaMask_Ev(caNum_nd3);
        else
            anIndx_nd3 = AnMask_Od(anNum_nd3);
            caIndx_nd3 = CaMask_Od(caNum_nd3);
        end
        anID = [3 bd_nd3+1 (floor(anIndx_nd3/36)+1)  (anIndx_nd3-(floor(anIndx_nd3/36))*36)+1];
        caID = [3 bd_nd3+1 (floor(caIndx_nd3/36)+1)  (caIndx_nd3-(floor(caIndx_nd3/36))*36)+1];
        
        chanIndxAn2 = Parameters{anID(1)}{anID(2)}{anID(3)}{anID(4)}(5); %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        chanIndxCa2 = Parameters{caID(1)}{caID(2)}{caID(3)}{caID(4)}(5); %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        
        anNum_nd2 = mtrx_Anodes(cs(j));
        caNum_nd2 = mtrx_Cathodes(cs(j));
        bd_nd2 = floor(cs(j)/(39*8));
        if(bd_nd2 == 0)
            anIndx_nd2 = AnMask_Ev(anNum_nd2);
            caIndx_nd2 = CaMask_Ev(caNum_nd2);
        else
            anIndx_nd2 = AnMask_Od(anNum_nd2);
            caIndx_nd2 = CaMask_Od(caNum_nd2);
        end
        anID = [2 bd_nd2+1 (floor(anIndx_nd2/36)+1)  (anIndx_nd2-(floor(anIndx_nd2/36))*36)+1];
        caID = [2 bd_nd2+1 (floor(caIndx_nd2/36)+1)  (caIndx_nd2-(floor(caIndx_nd2/36))*36)+1];
        
        chanIndxAn1 = Parameters{anID(1)}{anID(2)}{anID(3)}{anID(4)}(5); %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        chanIndxCa1 = Parameters{caID(1)}{caID(2)}{caID(3)}{caID(4)}(5); %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        
        chans = [chans; chanIndxAn1 chanIndxCa1 chanIndxAn2 chanIndxCa2];
        numHits = [numHits; localCoin(rs(j), cs(j))];
        i = i+1;
        if(i >= k)
            break;
        end
        localCoin(rs(j), cs(j)) = 0;
    end    
end
