% TimeCalibration_W14.m
clear, clc , close all

%==========================================================================
%% Set up constants
%==========================================================================
% timeLo = 200;       %Coincidence time constraint (low)
% timeHi = 400;       %Coincidence time constraint (high)
eLo = 511*0.9;   % Energy gating constraint in keV(low)
eHi = 511*1.1;   % ""        ""      "'     (high)
phtPk=511;         % Photopeak in keV

C2Ahi=1.2;         % Cathode to Anode energy ratio gating constraint (high)
C2Alo=0;           % " " " "  (low)
uvFreq = 0.49e6;      % Frequency of U and V sinusoids (Hz)
numItMax=20;       % Maximum number of optimization iterations
dropThresh = 30;   % Minimum number of events to drop during optimization to warrant another iteration
numStds = 2.35;    % Number of standard devs to keep (outlier limit)
radTol=0.50;        % UV corrected circle radius tolerance (+/-)
RowThreshold=7;   % minimum number of occurances channel must have in A matrix to be considered in optimization

learn=true;        % Test code using learn and test sets of data (split data in half)

numMTinEvent = 4;  % Number of measurement triples in an event to consider
                   % Either 2 (calibrating time differences for 1 module)
                   %     or 4 (calibrating time differences between 2
                   %           modules)
coinciWin_CTS = 50 %2.35482*13/(1000/50); %stddev[ns]*2.35/(1000/(num CTS bins in 1us))

numNodes = 5;      % Nodes are IDed 1,2,3,4
numBoards = 48;
numRENAs = 2;
numChannels = 36;

numAnParam = 7;
numCaParam = 5;

% Initialize data structure to store channel energy spectra
for i=1:numNodes
    for j=1:numBoards
        for k=1:numRENAs
            for l=1:numChannels
                energies{i}{j}{k}{l}=[];
            end
        end
    end
end

% Initialize data structure to store necessary parameters
params(1:13)=-1; %Initialize parameter array
for i=1:numChannels
    chan{i}=params; %Create array of channels
end
for i=1:numRENAs
    rena{i}=chan;   %Create array of renas
end
for i=1:numBoards
    board{i}=rena;  %Create array of boards
end
for i=1:numNodes
    Parameters{i}=board;  %Create array of nodes
end
% Parameter list:
% centerU     %UV ellipse correction parameters
% centerV
% psi
% radius
% tec_param_status (also channel index#)
% slope       %ADC to keV conversion parameters
% intercept
% kev_param_status
% polarity
% a0          %Depth correction parameters
% a1
% a2
% dcc_param_status

% Initialize array to map channel list index to channel ID info to make export easier.
% I.e. channelIDs is a list of channel ID's where each row corresponds to a
% channel being considered for optimization
channelIDs=[]; %Will be [node, board, rena, channel;]

%==========================================================================
%% Import UV ellipse correction parameters from .tec file
%==========================================================================
fprintf('\n');
display('Starting timing ellipse correction parameter import...');
%Find .tec filename and open it
filename=dir('*.tec');
if(length(filename)~=1) %If there is more or less than 1 .tec file in the current directory
    display('Current scope has either too many or no .tec files');
    %prompt user to select file
    [filename, pathname] = uigetfile('*.tec', 'Select a timing ellipse correction parameters file');
    if isequal(filename,0)
        disp('No .tec file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    fileinfo=dir([pathname filename]);
    if fileinfo.bytes==0
        error('Empty .tec file');
    end
    TECfile=fopen([pathname filename],'r');    
else
    if(filename.bytes==0)
        error('Empty .tec file');
    end
    disp(['Using ', filename.name])
    TECfile=fopen(filename.name,'r');
end
tic

% Loop through each line of the file and add params of each channel to the
% correct cell in the Parameters cell array
i=1;
line=fgetl(TECfile); %first line is 'channel{' (skip)
while ischar(line)
    for j=1:8
        line=fgetl(TECfile); %first line is 'node=#'
        if(line~=-1)         %Throw error if there is an invalid line
            switch j         %Do one of the following depending on which line it is
                case 1       %Node line
                    nd=str2double(line(strfind(line,'=')+1:end));
                    if(nd==-1)  %Deal with -1 nodes
                        nd=1;
                    else
                        nd=nd+1;
                    end
                case 2      %Board line
                    brd=str2double(line(strfind(line,'=')+1:end))+1;
                case 3      %RENA line
                    rna=str2double(line(strfind(line,'=')+1:end))+1;
                case 4      %Channel line
                    cnl=str2double(line(strfind(line,'=')+1:end))+1;
                otherwise   %The other lines are actual parameters
                    params(j-4)=str2double(line(strfind(line,'=')+1:end));
            end                   
        else
            error(['.tec file format error at line ' num2str((i-1)*10+j)]);
        end        
    end
    params(5)=i; %tec parameter status (>=1: parameters are good)
    % tec_param_status also indexes into channelIDs
    channelIDs = [channelIDs; [nd brd rna cnl]];
    Parameters{nd}{brd}{rna}{cnl}=params; %Write parameters to array
    line=fgetl(TECfile);            %line is '}'
    line=fgetl(TECfile);            %line is 'channel{'
    i=i+1;
end
fclose(TECfile);
if(isempty(channelIDs))
    error('No timing ellipse correction parameters imported');
end
fprintf(['Finished importing timing ellipse correction parameters for\n' num2str(length(channelIDs)) ' channels...\n']);
toc
fprintf('\n');

%==========================================================================
%% Import depth correction parameters from .dcc file
%==========================================================================
display('Starting depth correction parameter import...');
filename=dir('*.dcc');
if(length(filename)~=1)
    display('Current scope has either too many or no .dcc files');
    [filename, pathname] = uigetfile('*.dcc', 'Select a depth correction coefficient file');
    if isequal(filename,0)
        disp('No .dcc file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    fileinfo=dir([pathname filename]);
    if fileinfo.bytes==0
        error('Empty .dcc file');
    end
    DCCfile=fopen([pathname filename],'r');
    else
    if(filename.bytes==0)
        error('Empty .dcc file');
    end
    disp(['Using ', filename.name])
    DCCfile=fopen(filename.name,'r');
end

tic
i=1;
addDCC=0;       % Number of channels whos dcc parameters were added
missingTEC=0;   % Number of channels found in .dcc file that have missing
                % UV ellipse correction parameters
line=fgetl(DCCfile); %Get first line
while ischar(line)
    dcc=textscan(line,'%d %d %d %d %f %f %f');
    if(dcc{1}==-1) %If the file uses a -1 node then make it 1.
        dcc{1}=1;
    else
        dcc{1}=dcc{1}+1;
    end
    dcc{2}=dcc{2}+1;    %Adjust channelID info to work with MATLAB style vector notation (vectors start with 1 not 0)
    dcc{3}=dcc{3}+1;
    dcc{4}=dcc{4}+1;
    %Invalid or missing tec parameters means the dcc parameters should not be saved either
    if(Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(5)==-1) %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        missingTEC=missingTEC+1;
        line=fgetl(DCCfile);
        i=i+1;
        continue;   
    end
    
    Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(10:12)=[dcc{5} dcc{6} dcc{7}]; %Add parameters to parameter map
    Parameters{dcc{1}}{dcc{2}}{dcc{3}}{dcc{4}}(13)=1; %dcc parameter status (1: parameters are good)
    addDCC=addDCC+1;
    line=fgetl(DCCfile);
    i=i+1;
    
    
end
fclose(DCCfile);
display(['Added correction parameters for ' num2str(addDCC) ' channels.']);
if(missingTEC~=0)
    warning(['Dropped parameters for ' num2str(missingTEC) ' channels because of missing or invalid TEC parameters.']);
end
display('Finished importing depth correction parameters...');
toc
fprintf('\n');

%==========================================================================
%% Import ADC to keV conversion parameters from .kev file
%==========================================================================
display('Starting ADC to keV conversion parameter import...');
filename=dir('*.kev');
if(length(filename)~=1)
    display('Current scope has either too many or no .kev files');
    [filename, pathname] = uigetfile('*.kev', 'Select a ADC to keV conversion file');
    if isequal(filename,0)
        disp('No .kev file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    fileinfo=dir([pathname filename]);
    if fileinfo.bytes==0
        error('Empty .kev file');
    end
    KEVfile=fopen([pathname filename],'r');    
else
    if(filename.bytes==0)
        error('Empty .kev file');
    end
    disp(['Using ', filename.name])
    KEVfile=fopen(filename.name,'r');
end
tic
i=1;

line=fgetl(KEVfile); %Skip first three lines of file
line=fgetl(KEVfile);
line=fgetl(KEVfile);
line=fgetl(KEVfile);
addKEV=0;
missingKEV=0;
missingTEC=0;
while ischar(line)
    if(~isempty(strfind(line, 'inf')) || ~isempty(strfind(line, 'nan')))
        missingKEV = missingKEV+1;
        line=fgetl(KEVfile);
        i=i+1;
        continue; %Invalid or missing parameters for channel
    end
    kev=textscan(line,'%d %d %d %d %d %f %f');
    if(kev{5}~=0 && kev{5}~=1)
        missingKEV = missingKEV+1;
        line=fgetl(KEVfile);
        i=i+1;
        continue; %Invalid or missing parameters for channel
    end
    if(kev{1}==-1) %If the file uses a -1 node then make it 1.
        kev{1}=1;
    else
        kev{1}=kev{1}+1;
    end
    kev{2}=kev{2}+1;    %Adjust channelID info to work with MATLAB style vector notation (vectors start with 1 not 0)
    kev{3}=kev{3}+1;
    kev{4}=kev{4}+1;
    %Invalid or missing tec parameters means the kev parameters should not be saved either
    if(Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(5)==-1) %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        missingTEC=missingTEC+1;
        line=fgetl(KEVfile);
        i=i+1;
        continue;   
    end
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(6:7)=[kev{6} kev{7}]; %Add parameters to parameter map
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(8)=1; %kev parameter status (1: parameters are good)
    Parameters{kev{1}}{kev{2}}{kev{3}}{kev{4}}(9)=kev{5}; %polarity
    addKEV=addKEV+1;
    line=fgetl(KEVfile);
    i=i+1;
end
fclose(KEVfile);

%Loop through all channels that have tec parameters and clear the
%parameters if there are no kev parameters. Then remove the channels from
%the channelIDs list
for i=1:length(channelIDs)
    id=channelIDs(i,:);
    if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(8)==-1)
        Parameters{id(1)}{id(2)}{id(3)}{id(4)}(:)=-1;
        channelIDs(i,1)=-1;
    end
end

numCat=0;

index=find(channelIDs(:,1)==-1);
channelIDs(index,:)=[];
%Loop through all still valid channels and update the channel index
%numbers(parameter 5) so that A matrix construction works correctly
if(~isempty(index))
    for i=1:length(channelIDs)
        id=channelIDs(i,:);
        Parameters{id(1)}{id(2)}{id(3)}{id(4)}(5)=i;
        if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(9)==0)  %Count the number of cathode channels
            numCat=numCat+1;
        end
    end
end

if(addKEV==0)
    warning(['Dropped all parameters for ' num2str(missingKEV+missingTEC) ' channels because of missing or invalid TEC parameters or missing or invalid energy conversion parameters.']);
    error('No energy conversion parameters imported.');
end
display(['Added conversion parameters for ' num2str(addKEV) ' channels.']);
warning(['Dropped parameters for ' num2str(missingKEV+missingTEC) ' channels because of missing or invalid TEC parameters, or missing or invalid energy conversion parameters.']);
display('Finished importing ADC to keV conversion parameters...');
toc
fprintf('\n');

%=========================================================================
%% Import/Process coincident events from .eventp file and populate A matrix
%=========================================================================
display('Starting eventp data import and A matrix construction...');
%Find .eventp filename and open it
filename=dir('*.eventp');
if(length(filename)~=1)
    display('Current scope has either too many or no .eventp files');
    [filename, pathname] = uigetfile('*.eventp', 'Select a Photoelectric event data file');
    if isequal(filename,0)
        disp('No .eventp file selected');
        return;
    else
        disp(['User selected ', fullfile(pathname, filename)])
    end
    fileinfo=dir([pathname filename]);
    if fileinfo.bytes==0
        error('Empty .eventp file');
    end
    BTCfile=fopen([pathname filename],'r');    
else
    if(filename.bytes==0)
        error('Empty .eventp file');
    end
    disp(['Using ', filename.name])
    BTCfile=fopen(filename.name,'r');
end


tic
eventp=cell(numMTinEvent,9); %Initialize cell array for data buffer
% either 2 or 4 rows (1 for each measurement in event)
% columns:
% node, board, rena, channel, polarity, PHA, U, V, Coarse Time Stamp
R=[];
C=[];
S=[];
dts0=[];
dts1=[];
iter=0;
row=0;
ANZnum=1; %Index of A(sparse) element if using sparse matrix population methods
[numChanIndxs, ~] = size(channelIDs);
foundChanIndx = zeros(1,numChanIndxs); % Vector to keep track of which channels were added to A matrix
numTimesFound = 0;
CTSs = [];
genCoinMatrix;
while(1)
    % DEBUG: Stop import after some number of lines imported
%     if(iter == numMTinEvent*18000)
%         break;
%     end
    % Import numMTinEvent lines from the .eventp file
    % numMTinEvent is number of coincident measurements in an event
    exit=0; %While loop exit flag
    stop=0;
    for i=1:numMTinEvent
        line=fgetl(BTCfile);
        if(~ischar(line))
            exit=1; %Exit flag is set
            break; %Break out of for loop
        end
        eventp(i,:)=textscan(line,'%d %d %d %d %d %d %d %d %f');
        
        %Modification of eventp channel ID info to work with MATLAB array notation
        if(eventp{i,1}==-1)
            eventp{i,1}=1;
        else
            eventp{i,1}=(eventp{i,1})+1;
        end
        eventp{i,2}=(eventp{i,2})+1;
        eventp{i,3}=(eventp{i,3})+1;
        eventp{i,4}=(eventp{i,4})+1;            
    end
    if(exit) %Exit the loop (Stop Import)
        break;
    end
    if(stop)
        continue;
    end
    
    iter=iter+1; %increment loop iteration counter
    if(mod(iter,1000)==0)
        display(['Imported ' num2str(numMTinEvent*iter) ' lines...']);
    end
    % DEBUG: catch only CTS and continue
%     CTSs = [CTSs eventp{1,9} eventp{3,9}];
%     continue;
    
    % DEBUG: Catch only channels:
    chans = [ 57    44   166   156];
    currChans = zeros(1,numMTinEvent);
    for i = 1:numMTinEvent
        currChans(i) = Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(5);
    end
    [numCoin, ~] = size(chans);
    stop = 1;
    for i = 1:numCoin
        intsct = intersect(currChans, chans(i,:));
        if(length(intsct) == length(chans(i,:)))
            stop = 0;
            break;
        end
    end
    if(stop)
        continue;
    end
    numTimesFound = numTimesFound +1
    
    % Check if any of the conversion/correction parameters for the channel are missing and skip the group if they are.
    % Also check polarity consistency
    stop=0;
    for i=1:numMTinEvent
        if(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(9) ~= eventp{i,5})
            stop=1;
            break;
        elseif(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(5)==-1) %missing tec parameters
            stop=1;
            break;
        elseif(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(8)==-1)%missing kev parameters
            stop=1;
            break
        elseif(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(9)==1 && Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(13)==-1)%missing dcc parameters if anode
            stop=1;
            break;
        end
    end
    if(stop)
        continue;
    end
    
   % Convert ADC energy values to keV and identfiy anodes, cathodes, and
   % check if events are possibly coincident if considering coincident
   % event data
   CTS=eventp{1,9};
   stop=0;
   a1ind = -1;
   a2ind = -1;
   c1ind = -1;
   c2ind = -1;
   for i=1:numMTinEvent
       slope=Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(6);
       intercept=Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(7);
       eventp{i,6}=double(eventp{i,6})*double(slope)+double(intercept);
       
       % Check that for time calibration between detectors, the CTS
       % difference between alleged coincident events are within the
       % coincidence window
       if(numMTinEvent == 4)
           CTSdiff = CTS - eventp{i,9};
           if(abs(CTSdiff) >= coinciWin_CTS)
               stop = 1;
               break;
           end
       end
       
       if(eventp{i,5}==0)  %Cathode
           if(i <= 2)
               c1ind = i;
           else
               c2ind = i;
           end
       else
           if(i <= 2)
               a1ind = i;
           else
               a2ind = i;
           end
       end
   end
   if(stop)
       continue;
   end
   % Check the number of anodes and cathodes and that the two pairs of
   % events come from different detectors
    if(numMTinEvent == 2 && (c1ind == -1 || a1ind == -1))
        continue;
    elseif(numMTinEvent == 4 && (c1ind == -1 || a1ind == -1 || c2ind == -1 || a2ind == -1))
        continue;
    elseif(numMTinEvent == 4 && (eventp{a1ind, 1} == eventp{a2ind}))
        continue;
    end

    
    % Anode energy correction
    a0=Parameters{eventp{a1ind,1}}{eventp{a1ind,2}}{eventp{a1ind,3}}{eventp{a1ind,4}}(10);
    a1=Parameters{eventp{a1ind,1}}{eventp{a1ind,2}}{eventp{a1ind,3}}{eventp{a1ind,4}}(11);
    a2=Parameters{eventp{a1ind,1}}{eventp{a1ind,2}}{eventp{a1ind,3}}{eventp{a1ind,4}}(12);
    C2A_1 = double(eventp{c1ind,6})/double(eventp{a1ind,6});
    cFactor=phtPk/(a2*(C2A_1)^2+a1*(C2A_1)+a0);
    eventp{a1ind,6}=eventp{a1ind,6}*cFactor;
    % store energies
    energies{eventp{a1ind,1}}{eventp{a1ind,2}}{eventp{a1ind,3}}{eventp{a1ind,4}}=[energies{eventp{a1ind,1}}{eventp{a1ind,2}}{eventp{a1ind,3}}{eventp{a1ind,4}} eventp{a1ind,6}];
    energies{eventp{c1ind,1}}{eventp{c1ind,2}}{eventp{c1ind,3}}{eventp{c1ind,4}}=[energies{eventp{c1ind,1}}{eventp{c1ind,2}}{eventp{c1ind,3}}{eventp{c1ind,4}} eventp{c1ind,6}];
    if(numMTinEvent == 4)
        a0=Parameters{eventp{a2ind,1}}{eventp{a2ind,2}}{eventp{a2ind,3}}{eventp{a2ind,4}}(10);
        a1=Parameters{eventp{a2ind,1}}{eventp{a2ind,2}}{eventp{a2ind,3}}{eventp{a2ind,4}}(11);
        a2=Parameters{eventp{a2ind,1}}{eventp{a2ind,2}}{eventp{a2ind,3}}{eventp{a2ind,4}}(12);
        C2A_2 = double(eventp{c2ind,6})/double(eventp{a2ind,6});
        cFactor=phtPk/(a2*(C2A_2)^2+a1*(C2A_2)+a0);
        eventp{a2ind,6}=eventp{a2ind,6}*cFactor;
        % store energies
        energies{eventp{a2ind,1}}{eventp{a2ind,2}}{eventp{a2ind,3}}{eventp{a2ind,4}}=[energies{eventp{a2ind,1}}{eventp{a2ind,2}}{eventp{a2ind,3}}{eventp{a2ind,4}} eventp{a2ind,6}];
        energies{eventp{c2ind,1}}{eventp{c2ind,2}}{eventp{c2ind,3}}{eventp{c2ind,4}}=[energies{eventp{c2ind,1}}{eventp{c2ind,2}}{eventp{c2ind,3}}{eventp{c2ind,4}} eventp{c2ind,6}];    
    end
    
%     % Energy gating
    if(((C2A_1 < C2Alo) || (C2A_1 > C2Ahi)) || (eventp{a1ind,6}<eLo || eventp{a1ind,6}>eHi))
        continue;
    end
    if(numMTinEvent == 4)
        if(((C2A_2 < C2Alo) || (C2A_2 > C2Ahi)) || (eventp{a2ind,6}<eLo || eventp{a2ind,6}>eHi))
            continue;
        end
    end
    
    % Correct U and V data and find channel block in A matrix for each
    % channel
    stop = 0;
    chanID = -1*ones(numMTinEvent, 1);
    for i = 1:numMTinEvent
        chanID(i)=Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(5); %Parameters{node#}{board#}{RENA#}{channel#}(parameter#)
        switch(i)
            case a1ind
                a1blknum=chanID(i);
            case c1ind
                c1blknum=chanID(i);
            case a2ind
                a2blknum=chanID(i);
            case c2ind
                c2blknum=chanID(i);
        end
        eventp{i,7}=double(eventp{i,7})-double(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(1)); %U=U-Ucenter
        eventp{i,8}=double(eventp{i,8})-double(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(2)); %V=V-Vcenter
        %V=(V-U*sin(Psi))/cos(Psi)    
        eventp{i,8}=(eventp{i,8}-eventp{i,7}*sin(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(3)))./cos(Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(3)); 
        
        tempRad=sqrt(eventp{i,7}^2+eventp{i,8}^2);
        meanRad=Parameters{eventp{i,1}}{eventp{i,2}}{eventp{i,3}}{eventp{i,4}}(4);
        
        if(~(tempRad>= meanRad*(1-radTol) && tempRad<=meanRad*(1+radTol)))      %radius tolerance check
            stop=1;
            break;
        end
    end
    if(stop)
        continue;
    end
    
    % Update coincidence matrix
    %======================================================================
    if(numMTinEvent == 4)
        nd1 = eventp{a1ind, 1}-1;
        bd1 = eventp{a1ind, 2}-1;
        an_rn1 = eventp{a1ind, 3}-1;
        an1 = eventp{a1ind, 4}-1;
        
        nd2 = eventp{a2ind, 1} - 1;
        bd2 = eventp{a2ind, 2} - 1;
        an_rn2 = eventp{a2ind, 3} - 1;
        an2 = eventp{a2ind, 4} - 1;
        
        ca_rn1 = eventp{c1ind, 3} - 1;
        ca_rn2 = eventp{c2ind, 3} - 1;
        ca1 = eventp{c1ind, 4} - 1;
        ca2 = eventp{c2ind, 4} - 1;
        if(mod(bd1, 2)==0)
            an_mask1 = AnMask_Ev;
            ca_mask1 = CaMask_Ev;
        else
            an_mask1 = AnMask_Od;
            ca_mask1 = CaMask_Od;
        end
        if(mod(bd2,2)==0)
            an_mask2 = AnMask_Ev;
            ca_mask2 = CaMask_Ev;
        else
            an_mask2 = AnMask_Od;
            ca_mask2 = CaMask_Od;
        end
        
        phyCa1 = find(ca_mask1 == ((ca_rn1)*numChannels + ca1));
        phyCa2 = find(ca_mask2 == ((ca_rn2)*numChannels + ca2));
        phyAn1 = find(an_mask1 == ((an_rn1)*numChannels + an1));
        phyAn2 = find(an_mask2 == ((an_rn2)*numChannels + an2));
        
        if(isempty(phyCa1) || isempty(phyCa2) || isempty(phyAn1) || isempty(phyAn2))
            warning('Unknown channel mapping');
            continue;
        end       
        
        if(nd1 < nd2)
            colIndx = (bd1)*39*8 + (phyCa1-1)*39 + phyAn1;
            rowIndx = (bd2)*39*8 + (phyCa2-1)*39 + phyAn2;
        else
            colIndx = (bd2)*39*8 + (phyCa2-1)*39 + phyAn2;
            rowIndx = (bd1)*39*8 + (phyCa1-1)*39 + phyAn1;
        end
        
        coinMatrix(rowIndx, colIndx) = coinMatrix(rowIndx, colIndx)+1;
        
        
    end
    
    % Populate row corresponding to a'*c = tc1 - ta1
    %======================================================================
    row = row+1; % Row number in A matrix to populate
    dta1c1 = 1e9*angle((eventp{a1ind,7}+1i*eventp{a1ind,8})/(eventp{c1ind,7}+1i*eventp{c1ind,8}))*(1/uvFreq)/(2*pi); %1e9*angle((Ua1+jVa1)/(Uc1+jVc1))*(1/2pi*uvFreq)
    
    % Debug
    if(eventp{a1ind,3} == 1 && eventp{a1ind,3} == eventp{c1ind,3}) %Same (RENA 0)
        dts0 = [dts0 dta1c1];
    end
    if(eventp{a1ind,3} == 2 && eventp{a1ind,3} == eventp{c1ind,3}) %Same (RENA 1)
        dts1 = [dts1 dta1c1];
    end
    
    % Update vector to keep track of channels added to A matrix
    foundChanIndx(chanID) = 1;
    
    Y(row,1) = dta1c1;
    
    a1blkpos = (a1blknum-1)*7+1; %A matrix column index to start parameters for particular channel
    c1blkpos = (c1blknum-1)*7+1;
    
    % Parameter calculation
    a1U = eventp{a1ind,7};
    a1V = eventp{a1ind,8};
    c1U = eventp{c1ind,7};
    c1V = eventp{c1ind,8};
    c2a1 = double(eventp{c1ind,6})/double(eventp{a1ind,6});
    
    % Populate A matrix by creating Row (R) Column(C) and Value (S) vectors
    % (much faster)

    % Define elements of the first row
    R(ANZnum:ANZnum+13)=row;
    C(ANZnum:ANZnum+6)=[a1blkpos:a1blkpos+6];
    S(ANZnum:ANZnum+6)=[a1U a1U^2 a1V a1V^2 c2a1 c2a1^2 1];
    C(ANZnum+7:ANZnum+13)=[c1blkpos:c1blkpos+6];
%     S(ANZnum+7:ANZnum+13)=[c1U c1U^2 c1V c1V^2 c2a1 c2a1^2 1];
    S(ANZnum+7:ANZnum+13) = -1.*[c1U c1U^2 c1V c1V^2 0 0 1];
    
    ANZnum=ANZnum+14;

    
    % If necessary populate rows corresponding to a'c = tc2 - ta2 and a'c =
    % tc2 - tc1
    %======================================================================
    if(numMTinEvent == 4)
        % a'c = tc2 - ta2
        row = row+1;
        dta2c2 = 1e9*angle((eventp{a2ind,7}+1i*eventp{a2ind,8})/(eventp{c2ind,7}+1i*eventp{c2ind,8}))*(1/uvFreq)/(2*pi); %1e9*angle((Ua2+jVa2)/(Uc2+jVc2))*(1/2pi*uvFreq)
        Y(row,1) = dta2c2;
        
        a2blkpos = (a2blknum-1)*7+1; %A matrix column index to start parameters for particular channel
        c2blkpos = (c2blknum-1)*7+1;
        
        % Parameter calculation
        a2U = eventp{a2ind,7};
        a2V = eventp{a2ind,8};
        c2U = eventp{c2ind,7};
        c2V = eventp{c2ind,8};
        c2a2 = double(eventp{c2ind,6})/double(eventp{a2ind,6});
        
        % Define elements of the second row
        R(ANZnum:ANZnum+13)=row;
        C(ANZnum:ANZnum+6)=[a2blkpos:a2blkpos+6];
        S(ANZnum:ANZnum+6)=[a2U a2U^2 a2V a2V^2 c2a2 c2a2^2 1];
        C(ANZnum+7:ANZnum+13)=[c2blkpos:c2blkpos+6];
        %S(ANZnum+7:ANZnum+13)=[c1U c1U^2 c1V c1V^2 c2a1 c2a1^2 1];
        S(ANZnum+7:ANZnum+13) = -1.*[c2U c2U^2 c2V c2V^2 0 0 1];
        ANZnum=ANZnum+14;
        
        % a'c = tc2 - tc1
        row = row+1;
        dtc1c2 = 1e9*angle((eventp{c1ind,7}+1i*eventp{c1ind,8})/(eventp{c2ind,7}+1i*eventp{c2ind,8}))*(1/uvFreq)/(2*pi); %1e9*angle((Uc1+jVc1)/(Uc2+jVc2))*(1/2pi*uvFreq)
        Y(row,1) = dta1c1;
        
        % Define elements of the third row
        R(ANZnum:ANZnum+13)=row;
        C(ANZnum:ANZnum+6)=[c1blkpos:c1blkpos+6];
        S(ANZnum:ANZnum+6)=[c1U c1U^2 c1V c1V^2 0 0 1];
        C(ANZnum+7:ANZnum+13)=[c2blkpos:c2blkpos+6];
        %S(ANZnum+7:ANZnum+13)=[c1U c1U^2 c1V c1V^2 c2a1 c2a1^2 1];
        S(ANZnum+7:ANZnum+13) = -1.*[c2U c2U^2 c2V c2V^2 0 0 1];
        ANZnum=ANZnum+14;        
    end
end
fclose(BTCfile);
if(isempty(R) || isempty(S) || isempty(C)) %If the while loop never saved any valid data
    error(sprintf(['A matrix construction vectors are empty!\nData with matching time stamps in ' filename.name ' are from channels not found in the correction/conversion files or have values outside the limits.']));
end

A = sparse(R,C,S);



%==========================================================================
%% Find and remove columns of A that have too few elements
%==========================================================================
% First remove columns that correspond to C/A ratio for cathode channels
Col2del = [];
for i = 1:length(channelIDs)
    id = channelIDs(i,:);
    elementstoRemove = [];
    if(Parameters{id(1)}{id(2)}{id(3)}{id(4)}(9)==0) %Cathode
        chanColBlockStart = (i-1)*7 +1;
        Col2del = [Col2del chanColBlockStart+4 chanColBlockStart+5];
    end
end

[numR, numC] = size(A);
Col2del = unique(Col2del);
Col2del(find(Col2del > numC)) = []; %Remove columns to delete that are out of the A matrix bounds


A(:,Col2del)= [];
A(:,end) = [];%drop last column because of reference channel lacking a DC parameter

% Next, remove channels that have too few events associated with them and
% the corresponding events
[A, Y, channelIDs, ~, ~] = thresholdAmatrix(A,Y,channelIDs, RowThreshold, Parameters, numMTinEvent);

fprintf('Finished building A matrix.\n');
%toc
fprintf('\n');

learn = false;

if (learn && numMTinEvent == 2)
    Awhole=A;
    Ywhole=Y;
    A=A(1:2:end,:);
    Y=Y(1:2:end);
    Atest=Awhole(2:2:end,:);
    Ytest=Ywhole(2:2:end);
    disp('Learning set A size')
    size(A)
    disp('Test set A size')
    size(Atest)
elseif(learn && numMTinEvent == 4)
    Awhole = A;
    Ywhole = Y;
    total = length(Ywhole);
    rows1 = 1:3:total;
    rows2 = 2:3:total;
    rows3 = 3:3:total;
    A = Awhole(sort([rows1(1:2:end) rows2(1:2:end) rows3(1:2:end)]), :);
    Y = Ywhole(sort([rows1(1:2:end) rows2(1:2:end) rows3(1:2:end)]));
    Atest = Awhole(sort([rows1(2:2:end) rows2(2:2:end) rows3(2:2:end)]), :);
    Ytest = Ywhole(sort([rows1(2:2:end) rows2(2:2:end) rows3(2:2:end)]));
    disp('Learning set A size')
    size(A)
    disp('Test set A size')
    size(Atest)    
end


%==========================================================================
%% Calculate time calibration parameters and filter out random events
%==========================================================================
display('Starting outlier windowing and calibration parameter optimization.');
tic
Ystart=Y;
Astart=A;
for i=1:numItMax
    if(numMTinEvent == 2)
        numDrop=0; %Number of events dropped per iteration
        [loLim hiLim]=findWindow(Y(1:1:end), numStds, 50); %Window on dta1c1 values
        dropRows=find((Y(1:1:end)<loLim) | (Y(1:1:end)>hiLim)); %Find rows of events outside window
        Y(sort([dropRows]))=[];      %Delete rows of A and Y that are outside of window on dta1c1 values
        A(sort([dropRows]),:)=[];    
        numDrop=numDrop+length(dropRows);
    else
        numDrop=0; %Number of events dropped per iteration
        [loLim hiLim]=findWindow(Y(1:3:end), numStds, 50); %Window on dta1c1 values
        dropRows=find((Y(1:3:end)<loLim) | (Y(1:3:end)>hiLim)); %Find rows of events outside window
        dropRows=(dropRows*3)-2;                           %dropRows is a vector of out of window indices from the set of every third element of Y not relative to every element of Y so we convert
        Y(sort([dropRows dropRows+1 dropRows+2]))=[];      %Delete rows of A and Y that are outside of window on dta1c1 values
        A(sort([dropRows dropRows+1 dropRows+2]),:)=[];    %dta2c2 and dtc1c2 values and corresponding A rows are also removed
        numDrop=numDrop+length(dropRows);
        
        [loLim hiLim]=findWindow(Y(2:3:end), numStds, 50); %Window on dta2c2 values
        dropRows=find((Y(2:3:end)<loLim) | (Y(2:3:end)>hiLim)); %Find rows of events outside window
        dropRows=(dropRows*3)-1;                           %dropRows is a vector of out of window indices from the set of every third element of Y not relative to every element of Y so we convert
        Y(sort([dropRows dropRows-1 dropRows+1]))=[];      %Delete rows of A and Y that are outside of window on dta1c1 values
        A(sort([dropRows dropRows-1 dropRows+1]),:)=[];    %dta1c1 and dtc1c2 values and corresponding A rows are also removed
        numDrop=numDrop+length(dropRows);
        
        [loLim hiLim]=findWindow(Y(3:3:end), numStds, 50); %Window on dtc1c2 values
        dropRows=find((Y(3:3:end)<loLim) | (Y(3:3:end)>hiLim)); %Find rows of events outside window
        dropRows=(dropRows*3);                             %dropRows is a vector of out of window indices from the set of every third element of Y not relative to every element of Y so we convert
        Y(sort([dropRows dropRows-1 dropRows-2]))=[];      %Delete rows of A and Y that are outside of window on dta1c1 values
        A(sort([dropRows dropRows-1 dropRows-2]),:)=[];    %dta1c1 and dta2c2 values and corresponding A rows are also removed
        numDrop=numDrop+length(dropRows);       
    end
    
    % Threshold matrix to keep it from becoming rank deficient
    [A, Y, channelIDs, colsRemd, numColsRemd] = thresholdAmatrix(A,Y,channelIDs, RowThreshold, Parameters, numMTinEvent);
    
    if(~isempty(colsRemd)) % Have to remove elements of Xall that correspond to channels that have been removed
        for j = 1:length(numColsRemd)
            if(i ~= 1)
                Xall(colsRemd(1:numColsRemd(j)), :) = [];
            end
            Astart(:, colsRemd(1:numColsRemd(j))) = [];
            if(learn)
                Atest(:, colsRemd(1:numColsRemd(j))) = [];
            end
            colsRemd(1:numColsRemd(j)) = [];
        end
    end

    
    X=A\Y;
    Y=Y-A*X;
    X(end+1)=0; %Add a zero for parameter a6 (DC) for the reference channel
    Xall(:,i)=X;
    
    display(['Finished iteration ' num2str(i)]);
    
    if(numDrop<dropThresh && i>1)
        break
    end    
end
Xopt=sum(Xall,2); %The optimal calibration coefficients are the sum of the coefficients found in each iteration.

fprintf('Finished outlier windowing and calibration parameter optimization.\n');
toc
fprintf('\n');

%%  Generate Plots

if(~learn && numMTinEvent == 2)
    figure,hist(Ystart,50);
    title('Distribution of time differences before optimization');
    xlabel('Time difference [ns]'),ylabel('Count')
    
    [binHeight, binCenter]=hist(Y,75);
    f2=fit(binCenter', binHeight', 'gauss1');
    coef2=coeffvalues(f2);
    fwhm2=coef2(3)*2.35428
    conf=confint(f2);
    intv=(conf(:,3)).*2.35428;
    fwhm2-intv(1)
    figure,plot(f2,binCenter, binHeight);
    legend off
    xlabel('Time difference [ns]'),ylabel('Count')
    title(['Distribution of time differences after outlier windowing',10,'and calibration parameter optimization']);
    
    Yend=Astart*Xopt(1:end-1);
    Ydiff=Ystart-Yend;
    [binHeight, binCenter]=hist(Ydiff,75);
    f3=fit(binCenter', binHeight', 'gauss1');
    coef3=coeffvalues(f3);
    fwhmend=coef3(3)*2.35428
    conf=confint(f3);
    intv=(conf(:,3)).*2.35428;
    fwhmend-intv(1)
    figure,plot(f3,binCenter, binHeight);
    legend off
    title('\DeltaT-Ac after applying optimal parameters');
    xlabel('Time difference [ns]'),ylabel('Count')
elseif(numMTinEvent == 2)
    figure,hist(Ystart,50);
    title('Time Offsets of Learning Set');
    xlabel('Time difference [ns]'),ylabel('Count')
%     xlim([-500 500]),ylim([0 300]);
    %export_fig DTlearnset.pdf -pdf -transparent
    
    [binHeight, binCenter]=hist(Y,50);
    f2=fit(binCenter', binHeight', 'gauss1');
    coef2=coeffvalues(f2);
    fwhm2=coef2(3)*2.35428
    conf=confint(f2);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhm2-intv(1)
    figure,plot(f2,binCenter, binHeight);
    legend off
    xlabel('Time difference [ns]'),ylabel('Count')
    title(['Time Offsets of Learning Set After ' num2str(i) ' Iterations']);
%     xlim([-500 500]),ylim([0 300]);
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhm2,4) ' � ' num2str(conInt,4) ' ns'],'LineStyle', 'none');
   % export_fig DTlearnsetALS.pdf -pdf -transparent

    figure,hist(Ytest,50);
    title('Time Offsets of Test Set');
    xlabel('Time difference [ns]'),ylabel('Count')
%     xlim([-500 500]),ylim([0 300]);
    %export_fig DTtestset.pdf -pdf -transparent
    
    Yend=Astart*Xopt(1:end-1);
    Ydiff=Ystart-Yend;
    [binHeight, binCenter]=hist(Ydiff,750);
    f3=fit(binCenter', binHeight', 'gauss1');
    coef3=coeffvalues(f3);
    fwhm3=coef3(3)*2.35428
    conf=confint(f3);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhm3-intv(1)
    figure,plot(f3,binCenter, binHeight);
    legend off
    title('\DeltaT-Ac After Applying Parameters to Learning Set');
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhm3,4) ' �' num2str(conInt,4) ' ns'],'LineStyle', 'none');
    xlabel('Time difference [ns]'),ylabel('Count')
%     xlim([-500 500]),ylim([0 300]);
   % export_fig DTlearnsetAfter.pdf -pdf -transparent

    Yend=Atest*Xopt(1:end-1);
    Ydiff=Ytest-Yend;
    [binHeight, binCenter]=hist(Ydiff,750);
    f4=fit(binCenter', binHeight', 'gauss1');
    coef4=coeffvalues(f4);
    fwhmend=coef4(3)*2.35428
    conf=confint(f4);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhmend-intv(1)
    figure,plot(f3,binCenter, binHeight);
    legend off
    title('\DeltaT-Ac After Applying Paramters to Test Set');
    xlabel('Time difference [ns]'),ylabel('Count')
%     xlim([-500 500]),ylim([0 300]);
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhmend,4) ' � ' num2str(conInt,4) ' ns'],'LineStyle', 'none');
    %export_fig DTtestsetAfter.pdf -pdf -transparent
elseif(learn && numMTinEvent == 4)
    figure,hist(Ystart(3:3:end),50);
    title('Time Offsets between Cathodes of Learning Set');
    xlabel('Time difference [ns]'),ylabel('Count')
    
    [binHeight, binCenter]=hist(Y(3:3:end),50);
    f2=fit(binCenter', binHeight', 'gauss1');
    coef2=coeffvalues(f2);
    fwhm2=coef2(3)*2.35428
    conf=confint(f2);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhm2-intv(1)
    figure,plot(f2,binCenter, binHeight);
    legend off
    xlabel('Time difference [ns]'),ylabel('Count')
    title(['Time Offsets between Cathodes of Learning Set After ' num2str(i) ' Iterations']);
%     xlim([-500 500]),ylim([0 300]);
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhm2,4) ' � ' num2str(conInt,4) ' ns'],'LineStyle', 'none');
    
    figure,hist(Ytest(3:3:end),50);
    title('Time Offsets between Cathodes of Test Set');
    xlabel('Time difference [ns]'),ylabel('Count')
    
    Yend=Astart*Xopt(1:end-1);
    Ydiff=Ystart-Yend;
    [binHeight, binCenter]=hist(Ydiff(3:3:end),150);
    f3=fit(binCenter', binHeight', 'gauss1');
    coef3=coeffvalues(f3);
    fwhm3=coef3(3)*2.35428
    conf=confint(f3);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhm3-intv(1)
    figure,plot(f3,binCenter, binHeight);
    legend off
    title('\DeltaT-Ac After Applying Parameters to Learning Set');
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhm3,4) ' �' num2str(conInt,4) ' ns'],'LineStyle', 'none');
    xlabel('Time difference [ns]'),ylabel('Count')
    
    Yend=Atest*Xopt(1:end-1);
    Ydiff=Ytest-Yend;
    [binHeight, binCenter]=hist(Ydiff(3:3:end),150);
    f4=fit(binCenter', binHeight', 'gauss1');
    coef4=coeffvalues(f4);
    fwhmend=coef4(3)*2.35428
    conf=confint(f4);
    intv=(conf(:,3)).*2.35428;
    conInt=fwhmend-intv(1)
    figure,plot(f4,binCenter, binHeight);
    legend off
    title('\DeltaT-Ac After Applying Paramters to Test Set');
    xlabel('Time difference [ns]'),ylabel('Count')
%     xlim([-500 500]),ylim([0 300]);
    annotation('textbox',[0.6 0.6 0.1 0.1],'String', ['FWHM: ' num2str(fwhmend,4) ' � ' num2str(conInt,4) ' ns'],'LineStyle', 'none');   
    
end

%==========================================================================
%% Export time calibration parameters to .tcc file
%==========================================================================
display('Exporting parameters to .tcc file...');
tic
outf=fopen([filename.name(1:strfind(filename.name,'.eventp')-1) '.tcc'],'w');
fprintf(outf, ['node board rena channel      a0              a1              a2             a3             a4              a5              a6\n']);
Xind=1; 
Acol2chanID = [];
for i=1:length(channelIDs)
    if(channelIDs(i,1)==-1) %Skip channels that were deleted after kev parameter import
        continue;
    end
    if(Parameters{channelIDs(i,1)}{channelIDs(i,2)}{channelIDs(i,3)}{channelIDs(i,4)}(9)==1) 
        print=[channelIDs(i,:)-1 Xopt(Xind:Xind+6)'];
        fprintf(outf,'%4.2d %5.2d %4.2d %7.2d %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e',print);
        fprintf(outf, '\n');
        Xind=Xind+7;
        Acol2chanID = [Acol2chanID i*ones(1,7)]; %Save the mapping between the column of A and the channel ID
    else
        print=[channelIDs(i,:)-1 Xopt(Xind:Xind+3)' 0 0 Xopt(Xind+4)'];
        fprintf(outf,'%4.2d %5.2d %4.2d %7.2d %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e %14.6e',print);     %Cathodes have 2 fewer parameters
        fprintf(outf, '\n');
        Xind=Xind+5;    
        Acol2chanID = [Acol2chanID i*ones(1,5)];
    end
    
end
fclose(outf);
display('Finished exporting parameters to .tcc file...');
toc







