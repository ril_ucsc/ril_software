x = linspace(0, 800, 100);
for i = 1:length(channelIDs)
    id = channelIDs(i,:);
    hist(energies{id(1)}{id(2)}{id(3)}{id(4)}, x);
    xlim([ 0 800 ])
    pause(0.5)
end