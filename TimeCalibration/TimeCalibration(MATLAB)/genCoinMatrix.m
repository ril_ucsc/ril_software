numBoards = 2;
numCathodes = 8;
numAnodes = 39;
numTrigPairs = numAnodes*numCathodes*numBoards;

coinMatrix = zeros((numBoards-1)*(numAnodes*numCathodes) + (numCathodes-1)*numAnodes + numAnodes);

AnMask_Ev = [60, 24, 59, 23, 58, 22, 57, 21, 56, 20, 53, 19, 55, 18, 52, 15, 54, 14, 17, 13, 16, 12, 49, 11, 51, 10, 48, 9, 50, 8, 47, 7, 46, 6, 45, 5, 44, 4, 43];
CaMask_Ev = [28, 62, 63, 64, 27, 26, 25, 61];
AnMask_Od = [47, 8, 48, 9, 49, 10, 50, 11, 51, 12, 54, 13, 52, 14, 55,15,53,16,20,17,21,18,58,19,56,22,59,23,57,24,60,25,61,26,62,27,63,28,64];
CaMask_Od = [43,5,6,7,46,45,44,4];

mtrx_Anodes = repmat(1:numAnodes, [1, numBoards*numCathodes]);
mtrx_Cathodes = [];
for i = 1:numCathodes
    mtrx_Cathodes = [mtrx_Cathodes i*ones(1,numAnodes)];
end
mtrx_Cathodes = repmat(mtrx_Cathodes, [1, numBoards]);


% To determine the 4 channels corresponding to an element of coinMatrix:
% For row indexes, the node is 2
% For column indexes, the node is 1
% Indexes > than 39*8=312 correspond to board 1
% Indexes <= 312 correspond to board 0
% 
% mtrx_Anodes(index) = anode number
% AnMask_Ev(anode number) or AnMask_Od(anode number) provides the anode channel index. (Use AnMask_Ev for even board)
% mtrx_Cathodes(index) = cathode number
% CaMask_Ev(cathode number) or CaMask_Od(cathode number) provides the cathode channel index
% Rena number is determined by mod(channel index, 36) and channel number is channel index - rena number*36

