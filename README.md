This repository contains all the software used within the RIL group of University of California, Santa Cruz.
Read and Write access will be given exclusively to members of the group and their account using their university's email.

The README file will be updated as processes and strategies will be established.
