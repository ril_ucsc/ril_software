/********************************************
 * Function: Blur the energy, time and binning XYZ 1*1*1.
 * *****************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include "TString.h"
#include "TFile.h"
#include "TChain.h"
#include "TH1.h"
#include "TH1I.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH1C.h"
#include "TTree.h"
#include "TMath.h"
#include "TRandom.h"
#include "TCanvas.h"

using namespace std;

const Double_t timeResolution = 3.397E-9; // 8 ns FWHM
const Float_t energyResolution = 0.00433; // %2 FWHM at 511 keV
const Double_t CoinThresd = 20E-9; //[s] RENA-3 is 50MHz
const Float_t lowEnergyCut = 0.05; // noise level [MeV]

inline Bool_t IsBinned(Double_t t1, Double_t t2);
inline Float_t Binning1mm(Float_t pos); // 1 mm resolution

Int_t BlurBinHits(TString file){
	gRandom->SetSeed(0);

	TFile *dataFile = new TFile("./data/" + file + ".root");
	TTree *hitTree = (TTree*)dataFile->Get("Hits");

	Double_t time = 0.;
	Float_t edep = 0.;
	Float_t posX = 0.;
	Float_t posY = 0.;
	Float_t posZ = 0.;

	hitTree->SetBranchAddress("time", &time);
	hitTree->SetBranchAddress("edep", &edep);
	hitTree->SetBranchAddress("posX", &posX);
	hitTree->SetBranchAddress("posY", &posY);
	hitTree->SetBranchAddress("posZ", &posZ);

	Int_t nHit = hitTree->GetEntries();
	cout<<"Total hits = "<<nHit<<endl;

	TFile *blurFile = new TFile("./data/" + file + "_blur.root", "RECREATE");
	TTree *blurTree = new TTree("blurTree", "blurred tree");

	Double_t blurTime = 0;
	Float_t blurEdep = 0;
	Float_t binPosX = 0;
	Float_t binPosY = 0;
	Float_t binPosZ = 0;

	blurTree->Branch("blurTime", &blurTime, "blurTime/D");
	blurTree->Branch("blurEdep", &blurEdep, "blurEdep/F");
	blurTree->Branch("binPosX", &binPosX, "binPosX/F");
	blurTree->Branch("binPosY", &binPosY, "binPosY/F");
	blurTree->Branch("binPosZ", &binPosZ, "binPosZ/F");

	vector <Double_t> timeVec; Double_t sumTime;
	vector <Float_t> edepVec; Float_t sumEdep;

	Float_t lastBinX, currentBinX; 
	Float_t lastBinY, currentBinY; 
	Float_t lastBinZ, currentBinZ; 
	Double_t lastTime, currentTime; 
	Bool_t isNewYZ = true;

	Int_t iHit = 0;
	while(iHit < nHit){
		hitTree->GetEntry(iHit);

		/*** Step0. Check Rayleigh scattering ***/
		if(edep == 0){iHit++; continue;}

		/*** Step1: Check hits from the same pixel ***/
		currentBinX = Binning1mm(posX); 
		currentBinY = Binning1mm(posY); 
		currentBinZ = Binning1mm(posZ); 
		currentTime  = time;

		if(isNewYZ){ // situation1: a new hit
			lastBinX = currentBinX; lastBinY = currentBinY; lastBinZ = currentBinZ; 
			lastTime = currentTime;
			isNewYZ = false;
		}else{ // situation2: not a new hit
			if( (currentBinX == lastBinX) && (currentBinY == lastBinY) && (currentBinZ == lastBinZ) && IsBinned(currentTime, lastTime) ){//hit from the same pixel
				timeVec.push_back(time);
				edepVec.push_back(edep);
				iHit++; 
				if(iHit%1000000 == 0){cout<<iHit<<endl;}
			}else{ // //situation3: hit from a new pixel. Process data
				sumTime = 0; sumEdep = 0; 
				for(unsigned int j=0; j<timeVec.size(); j++){
					sumTime += timeVec[j] * edepVec[j];
					sumEdep += edepVec[j];
				}
				sumTime /= sumEdep; 

				binPosX = lastBinX;
				binPosY = lastBinY;
				binPosZ = lastBinZ;
				blurEdep = sumEdep + gRandom->Gaus(0, energyResolution);
				blurTime = sumTime + gRandom->Gaus(0, timeResolution);

				if(blurEdep>lowEnergyCut){
					blurTree->Fill();
				}
				timeVec.clear(); edepVec.clear(); 
				isNewYZ = true;
			}
		} 
	}

	blurFile->Write();
	return 0;
} 

inline Bool_t IsBinned(Double_t t1, Double_t t2){
	if( TMath::Abs(t1-t2) < CoinThresd ){
		return true;
	}else{
		return false;
	}  
}
inline Float_t Binning1mm(Float_t pos){
	if(pos>0){
		return Int_t(pos) + 0.5;
	}else{
		return Int_t(pos) - 0.5;
	}
}
