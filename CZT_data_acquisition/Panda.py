# Importing necessary packages
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import UnivariateSpline

# Import config.py with all configurations for the plotting

import config


def make_norm_dist(x, mean, sd):
    return 1.0 / (sd * np.sqrt(2 * np.pi)) * np.exp(-(x - mean) ** 2 / (2 * sd ** 2))


def peak(z, c):
    return np.exp(-np.power(z - c, 2) / 16.0)


def lin_interp(x, y, i, half):
    return x[i] + (x[i + 1] - x[i]) * ((half - y[i]) / (y[i + 1] - y[i]))


def half_max_x(x, y):
    half = max(y) / 2.0
    signs = np.sign(np.add(y, -half))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    return [lin_interp(x, y, zero_crossings_i[0], half),
            lin_interp(x, y, zero_crossings_i[1], half)]


# For style
plt.style.use('fivethirtyeight')

# Reading the Data file: Notice the delimiter <SPACE>
data = pd.read_csv('New Data/test_fwhm2.dat', delimiter=' ')

# Loading Data to lists {Format of the data file is: node board rena channel polarity(1 for anode & 0 for cathode) adc_e adc_u adc_v counter}

Node = data['node']
Board = data['board']
Rena = data['rena']
Channel = data['channel']
Polarity = data['polarity']
ADC_Energy = data['adc_e']
ADC_U = data['adc_u']
ADC_V = data['adc_v']

Counter = data['counter']

# End of loading data
# Test functionality of filtering (iteration syntax can probably be improved)

filtered_adc_e = []

for i in range(0,
               len(Board)):  # we are using len(Board) but len(RENA_no) or len(node_no) would have been the same number
    # print (Board[i])
    if Board[i] == config.BOARD_NO and Rena[i] == config.RENA_NO and Node[i] == config.NODE_NO and Channel[
        i] in config.CHANNEL_NO:
        filtered_adc_e.append(ADC_Energy[i])

print(type(filtered_adc_e))
sorted_adc = filtered_adc_e
sorted_adc.sort()
counts_adc = {}
current_element = sorted_adc[0]
counts_adc[str(current_element)] = sorted_adc.count(current_element)
for i in range(len(sorted_adc)):
    if sorted_adc[i] != current_element:
        current_element = sorted_adc[i]
        counts_adc[str(current_element)] = sorted_adc.count(current_element)

with open('adc_counts.txt', 'w') as f:
    for element in counts_adc:
        f.write("Total counts of ADC value " + str(element) + " is: " + str(counts_adc[element]) + "\n")

adce2 = []
max_count = max(filtered_adc_e)
direction = 1
adc_former = 0
# filtered_adc_e=filtered_adc_e[10:-1]
for adc_e in filtered_adc_e:
    if direction == 1:
        if adc_e > adc_former:
            adce2.append(adc_e)
            adc_former = adc_e
            print(adc_e)
        if adc_e == max_count:
            direction = 0
    if direction == 0:
        if adc_e < adc_former:
            adce2.append(adc_e)
            adc_former = adc_e
print(adce2)

plt.hist(filtered_adc_e, bins=1000, edgecolor='black')

plt.title('ADC_Energy')
plt.xlabel('KeV')
plt.ylabel('Counts')

plt.tight_layout()

plt.show()

# make some fake data
# x= np.linspace(0, 20, 21)
filtered_adc_e_arr = np.array(filtered_adc_e)
# y = peak(filtered_adc_e_arr, 10)
# y = np.amax(filtered_adc_e_arr)

# find the two crossing points
# hmx = half_max_x(filtered_adc_e_arr, y)

# print the answer
# fwhm = hmx[1] - hmx[0]
# print("FWHM:{:.3f}".format(fwhm))

# a convincing plot
# half = max(y)/2.0
# mp.plot(filtered_adc_e_arr, y)
# mp.plot(hmx, [half, half])
# mp.show()
