close all;

% This function imports the eventTrailsDump.txt file and puts it into
% a corresponding cell array.
rawData = load('eventLogDump.txt');

numInterxn = rawData(1);
rawData = rawData(2:end, :);

for i = 1:numInterxn
	numChargeElem = rawData(1);
	rawData = rawData(2:end, :);
    for j = 1:numChargeElem
        lenE = rawData(1);
        rawData = rawData(2:end, :);
        % qEMobile qETrapped xPosE yPosE zPosE t 
        eventTrail{i, j}.E = rawData(1:lenE, :);
        rawData = rawData(lenE + 1:end, :);
        
        lenH = rawData(1);
        rawData = rawData(2:end, :);
        % qHMobile qHTrapped xPosH yPosH zPosH t 
        eventTrail{i, j}.H = rawData(1:lenH, :);
        rawData = rawData(lenH + 1:end, :);
    end
end

% Plot the charge element trajectories in detector
figure;
for i = 1:numInterxn
    for j = 1:numChargeElem
        x = eventTrail{i, j}.E(:, 3);
        y = eventTrail{i, j}.E(:, 4);
        z = eventTrail{i, j}.E(:, 5);
        
        plot3(x, y, z, 'r.-');
        hold on;
        
        x = eventTrail{i, j}.H(:, 3);
        y = eventTrail{i, j}.H(:, 4);
        z = eventTrail{i, j}.H(:, 5);        
        
        plot3(x, y, z, 'b.-');
        hold on;
    end
end
grid on;
xlabel('x');
ylabel('y');
zlabel('z');

colorString = 'bgrcmyk';
for i = 1:numInterxn
    figure;
    for j = 1:numChargeElem
    	plot(eventTrail{i, j}.E(:, 8), eventTrail{i, j}.E(:, 1), [colorString(mod(j - 1, 7) + 1) 'o-']);
    	hold on;
    end
	title(['Intrxn ' num2str(i) ' Mobile electrons as a function of time']);
	xlabel('Time (s)');
	ylabel('Mobile electrons');
	
	figure;
    for j = 1:numChargeElem
    	plot(eventTrail{i, j}.E(:, 8), eventTrail{i, j}.E(:, 2), [colorString(mod(j - 1, 7) + 1) 'o-']);
    	hold on;
    end
	title(['Intrxn ' num2str(i) ' Fixed electrons as a function of time (this is not an accumulated quantity)']);
	xlabel('Time (s)');
	ylabel('Fixed electrons');
	
	figure;
    for j = 1:numChargeElem
	    plot(eventTrail{i, j}.H(:, 8), eventTrail{i, j}.H(:, 1), [colorString(mod(j - 1, 7) + 1) 'o-']);
	    hold on;
	end
	title(['Intrxn ' num2str(i) ' Mobile holes as a function of time']);
	xlabel('Time (s)');
	ylabel('Mobile holes');

	figure;
    for j = 1:numChargeElem
    	plot(eventTrail{i, j}.H(:, 8), eventTrail{i, j}.H(:, 2), [colorString(mod(j - 1, 7) + 1) 'o-']);
    	hold on;
    end
	title(['Intrxn ' num2str(i) ' Fixed holes as a function of time (this is not an accumulated quantity)']);
	xlabel('Time (s)');
	ylabel('Fixed holes');
	
	figure;
	for j = 1:numChargeElem
    	plot(eventTrail{i, j}.E(:, 8), eventTrail{i, j}.E(:, 6), [colorString(mod(j - 1, 7) + 1) 'o-']);
    	hold on;
    end
	xlabel('Time (s)');
	ylabel('Cathode electron phi');
	
	figure;
    for j = 1:numChargeElem
	    plot(eventTrail{i, j}.H(:, 8), eventTrail{i, j}.H(:, 6), [colorString(mod(j - 1, 7) + 1) 'o-']);
	    hold on;
	end
	xlabel('Time (s)');
	ylabel('Cathode hole phi');
	
	figure;
	for j = 1:numChargeElem
    	plot(eventTrail{i, j}.E(:, 8), eventTrail{i, j}.E(:, 7), [colorString(mod(j - 1, 7) + 1) 'o-']);
    	hold on;
    end
	xlabel('Time (s)');
	ylabel('Anode electron phi');
	
	figure;
    for j = 1:numChargeElem
	    plot(eventTrail{i, j}.H(:, 8), eventTrail{i, j}.H(:, 7), [colorString(mod(j - 1, 7) + 1) 'o-']);
	    hold on;
	end
	xlabel('Time (s)');
	ylabel('Anode hole phi');
end

% Import cathode signal
lenCathodeSignal = rawData(1);
rawData = rawData(2:end, :);
cathodeSignal = rawData(1:lenCathodeSignal, 1);
cathodeTime = rawData(1:lenCathodeSignal, 2);
rawData = rawData(lenCathodeSignal + 1:end, :);

% Import anode signal
lenAnodeSignal = rawData(1);
rawData = rawData(2:end, :);
anodeSignal = rawData(1:lenAnodeSignal, 1);
anodeTime = rawData(1:lenAnodeSignal, 2);
rawData = rawData(lenAnodeSignal + 1:end, :);

% Import anode and cathode mobile and fixed signal components
lenMFSignal = rawData(1);
rawData = rawData(2:end, :);
caVsTimeMobile = rawData(:, 1);
caVsTimeFixed = rawData(:, 2);
anVsTimeMobile = rawData(:, 3);
anVsTimeFixed = rawData(:, 4);
t = rawData(:, 5);

figure;
plot(t, caVsTimeMobile, '-o');
hold on;
plot(t, caVsTimeFixed, 'r-o');
plot(cathodeTime, cathodeSignal, 'k-o');
xlabel('Time (s)');
ylabel('caVsTime (eV) Mobile = b Fixed = r, Sum = k');

figure;
plot(t, anVsTimeMobile, '-o');
hold on;
plot(t, anVsTimeFixed, 'r-o');
plot(anodeTime, anodeSignal, 'k-o');
xlabel('Time (s)');
ylabel('anVsTime (eV) Mobile = b Fixed = r, Sum = k');