clear; clc; close all;

interactiveOut

% Resample all sequences at regular sampling rate so we can filter them
sampT = 1e-9;
t = interp1(timeVec, timeVec, [0:sampT:max(timeVec)]);
an = anVsTime{1};
an = interp1(timeVec, an, t);
ca = caVsTime{1};
ca = interp1(timeVec, ca, t);

figure;
plot(timeVec, anVsTime{1});
hold on; grid on;
plot(t, an, 'r.-');
xlabel(['Time (second)']);
ylabel(['Anode signal (A.U.)']);
title(['Blue: original, red: resampled']);
figure;
plot(timeVec, caVsTime{1});
hold on; grid on;
plot(t, ca, 'r.-');
xlabel(['Time (second)']);
ylabel(['Cathode signal (A.U.)']);
title(['Blue: original, red: resampled']);

% Construct preamplifier transfer function (refer to notes in lab book made
% on Jan 7, 2011).
Cin = 470e-12;
% % Cremat CR-110
% Rf = 100e6;
% Cf = 1.4e-12;
% RENA-3 ASIC
Rf = 200e6;    % Config 0 = 200 Mohm, config 1 = 1200 Mohm
Cf = 60e-15;  % Config 0 = 15 fF, config 1 = 60 fF
% Negative sign is ignored
h = tf([Rf 0], [Rf*Cf 1]);
% Convert to a discrete filter, tustin corresponds to bilinear
% transformation
hd = c2d(h, sampT, 'tustin');
% Make sure the continuous and discrete filters are equivalent
figure;
step(hd, 'r.-', [0:sampT:8e-4]);
hold on;
step(h, [0:sampT:8e-4]);
xlabel(['Time (second)']);
ylabel(['Filter step response']);
title(['Blue: continuous, red: discrete']);

% Pad the end of the charge signal sequence with the final value for long
% enough to see the effect of the filter at dc (unpadded signals are too
% short to observe the effects of the preamplifier).
% t = [t t(end)+[0:8e4]*sampT];
% an = [an an(end)*ones(1, length([0:8e4]))];
% ca = [ca ca(end)*ones(1, length([0:8e4]))];

% Filter the charge signals with the preamplifier
[num, den] = tfdata(hd);
num = num{1};
den = den{1};
ampAn = filter(num, den, an);
figure;
plot(t, an/max(abs(an)));
hold on;
plot(t, ampAn/max(abs(ampAn)), 'r.-');
xlabel(['Time (second)']);
ylabel(['Amplitude-normalized anode signal (A.U.)']);
title(['Blue: original, red: matlab preamp output']);

ampCa = filter(num, den, ca);
figure;
plot(t, ca/max(abs(ca)));
hold on;
plot(t, ampCa/max(abs(ampCa)), 'r.-');
xlabel(['Time (second)']);
ylabel(['Amplitude-normalized cathode signal (A.U.)']);
title(['Blue: original, red: matlab preamp output']);

for i = 1:numAnCollectedCharge
	figure;
	plot(timeVecPreamp, anVsTimePreamp{i}, '.-');
	title(['CTSI preamplified anode ' num2str(anCollectorID(i))]);
end

for i = 1:numCaCollectedCharge
	figure;
	plot(timeVecPreamp, caVsTimePreamp{i}, '.-');
	title(['CTSI preamplified cathode ' num2str(anCollectorID(i))]);
end