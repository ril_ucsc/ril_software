% This script plots the scatter plot of events in listOut.txt
clear; clc;
% close all;

rawData = load('listOut.txt')';

index1 = 1;
index2 = 1;
numCols = size(rawData, 2);
data = -1*ones(size(rawData));

% Sift out only 1 anode-1 cathode events
while (index1 < numCols - 1)
    startInd = index1;
    while ((rawData(1, startInd) == rawData(1, index1 + 1)) && (index1 < numCols - 1))
        index1 = index1 + 1;
    end
    
    if (((index1 - startInd) == 1) && (rawData(3, startInd) == 0) && (rawData(3, index1) == 1))
        data(:, index2:index2+1) = rawData(:, startInd:index1);
        index2 = index2 + 2;
    end
    index1 = index1 + 1;
end

ind = find(data(1, :) == -1);
data = data(:, 1:ind - 1);

an = data(5, 1:2:end);
noisyAn = data(7, 1:2:end);
ca = abs(data(5, 2:2:end));
noisyCa = abs(data(7, 2:2:end));

figure;
plot(ca, an, '.')
xlabel(['Cathode']);
ylabel(['Anode']);
xlim([0 0.3]);
ylim([0 0.3]);

figure;
plot(noisyCa, noisyAn, '.')
xlabel(['Noisy cathode']);
ylabel(['noisy anode']);
xlim([0 0.3]);
ylim([0 0.3]);