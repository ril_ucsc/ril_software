% This file generates a gray output file of interactions at specified
% spatial locations
close all;

fileName = 'pseudoGrayOutput.txt';
numEvents = 1000;
xMin = -0.02;
xMax = -0.019;
yMin = 0;
yMax = 0;
zMin = 0.29;
zMax = 0.291;

xPosOffset = 0;
yPosOffset = 5.95;
zPosOffset = -0.25;

xRand = rand(1, numEvents);
yRand = rand(1, numEvents);
zRand = rand(1, numEvents);

xPos = xMin + (xMax - xMin)*xRand + xPosOffset;
yPos = yMin + (yMax - yMin)*yRand + yPosOffset;
zPos = zMin + (zMax - zMin)*zRand + zPosOffset;

% Open file
fid = fopen(fileName, 'wt');
for i = 1:numEvents
    % Column format of gray output:
	% Column 1: eventType (1 = Compton, 3 = photoelectric)
    fprintf(fid, '%0.0f ', 3);
	% Column 2: eventID
    fprintf(fid, '%0.0f ', i);
	% Column 3: direction (color)
    fprintf(fid, '%0.0f ', 0);
	% Column 4: time
	fprintf(fid, '%0.0f ', 0);
	% Column 5: energy
	fprintf(fid, '%8.10e ', 5.109989e-01);
	% Column 6: x
	fprintf(fid, '%8.10e ', xPos(i));
	% Column 7: y
	fprintf(fid, '%8.10e ', yPos(i));
	% Column 8: z
    fprintf(fid, '%8.10e ', zPos(i));
	% Column 9: one;
	fprintf(fid, '%0.0f ', 1);
	% Column 10: detectorID;
    fprintf(fid, '%0.0f\n', 0);
end

% Close file
fclose(fid);

figure; plot3(xPos, yPos, zPos, '.'); grid on