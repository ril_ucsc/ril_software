clear all
clear 
close
tic 
%% READ IN OUTPUT FROM HeatMapVariable 
% ONLY WORKS FOR BOX TYPE GEOMETRY. IF YOU ARE NOT USING BOXES
% PERPENDICULAR TO AXIS THEN SKIP THIS FILE AND GO STRAIGHT TO THE
% VISUALIZATION. YOU WILL BE UNABLE TO PARTITION YOUR GEOMETRY BUT IT WILL
% PLOT YOUR GEOMETRY IN VOXELS.

% YOU CAN DO A ROTATED GEOMETRY THOUGH IF YOU FIRST ROTATE IT TO BE
% PERPENDICULAR TO AN AXIS AND THEN SORT THE ROWS AND THEN ROTATE IT BACK.
% THIS CAN SIMPLY BE DONE THROUGH THE USE OF A ROTATION MATRIX.

load('VariableOutput');
% Define output file name
outputVariablePartitioned = 'VariableOutputPartitioned';
% Preallocate Memory (May have to make bigger to make your code faster)
originsx = zeros(460800*4,5);
originsy = zeros(460800*4,5);
% Define bounds for boxes in the "x" direction and assign them to the X/Y 
% box position coordinates. This will sort points into 2 boxes where each set is
% perpendicular to each other. For example my set up is made of a 4 sided
% box. the originsx will take the boxes on one axis and group them and
% originsy will take the boxes on the other axis and group them. 
% This is done by defining the xBoxX .... matrices as follows:
% defing the x,y,z bounds from the least value to the most value. Do this
% for both boxes found on the x direction. The loop will group these boxes
% together and then everything else (ie the other boxes) will be grouped
% together. 
xBoxX = [-88 -40];
xBoxY = [-80 40];
xBoxZ = [-40 40];

yBoxX = [40 88];
yBoxY = [-40 80];
yBoxZ = [-40 40];
% Finally define how every so often you want to partition your voxels. I
% wanted to group my voxels in the outward direction into every 6 voxels.
% Make sure you use a divisible number. 
partitionNumber = 6;

for i = 1:size(origins,1)
    if origins(i,1) < xBoxX(2) && origins(i,1) > xBoxX(1) ...
            && origins(i,2) < xBoxY(2) && origins(i,2) > xBoxY(1) ...
            && origins(i,3) < xBoxZ(2) && origins(i,3) > xBoxZ(1)
       originsx(i,:)=origins(i,:);
                
    elseif  origins(i,1) < yBoxX(2) && origins(i,1) > yBoxX(1) ...
            && origins(i,2) < yBoxY(2) && origins(i,2) > yBoxY(1) ...
            && origins(i,3) < yBoxZ(2) && origins(i,3) > yBoxZ(1)
        originsx(i,:)=origins(i,:);
    else 
        originsy(i,:) = origins(i,:);
    end
end
originsy( ~any(originsy,2), : ) = [];
originsx( ~any(originsx,2), : ) = [];
% Sort the x boxes based on depths. Loop through them and sum them based on
% every "x" voxels
originsx = sortrows(originsx,[2 3]);

for i = 1:size(originsx,1)/partitionNumber
    val1 = originsx((i*partitionNumber)-(partitionNumber-1):(i*partitionNumber),4);
    val2 = sum(val1)/size(val1,1);
    originsx((i*partitionNumber)-(partitionNumber-1):(i*partitionNumber),4) = val2;
end
% Do the same for the y boxes
originsy = sortrows(originsy,[1 3]);

for i = 1:size(originsy,1)/6
    val1 = originsy((i*partitionNumber)-(partitionNumber-1):(i*partitionNumber),4);
    val2 = sum(val1)/size(val1,1);
    originsy((i*partitionNumber)-(partitionNumber-1):(i*partitionNumber),4) = val2;
end
%Recreate the final origins plot with the repartitioned values
origins = [originsx;originsy];
save(outputVariablePartitioned)



