clear all
clear 
close
% define most "negative" voxel center position for one of the modules
xyz = [40.5,-39.5,-39.5];
% Loop through every voxel position in one module to define a matrix
% that contains the x,y,z position of every voxel center within that module
finalMatrix = zeros(460800,3);
z = 1;
for i=1:48
    for j=1:120
        for k = 1:80
            finalMatrix(z,:)=[xyz(1)+i-1,xyz(2)+j-1,xyz(3)+k-1];
            z = z+1;
        end
    end
end
%
% do the same for a perpindicular module
xyz2 = [-39.5,-40.5,-39.5];
finalMatrix2 = zeros(460800,3);
z = 1;
for i=1:120
    for j=1:48
        for k = 1:80
            finalMatrix2(z,:)=[xyz2(1)+(i-1),xyz2(2)-(j-1),xyz2(3)+(k-1)];
            z = z+1;
        end
    end
end
%invert and shift those two modules to create your last two module matrices
finalMatrix3 = [finalMatrix(:,1)-128,finalMatrix(:,2)-40,finalMatrix(:,3)];
finalMatrix4 = [finalMatrix2(:,1)-40,finalMatrix2(:,2)+128,finalMatrix2(:,3)];
%combine all matrices into one matrix that defines entire detector geometry
origins = [finalMatrix;finalMatrix2;finalMatrix3;finalMatrix4];
%add column of zeros
%}

%{
finalMatrix = finalMatrix';
x_center = 0;
y_center = 0;
z_center = 0;

center = repmat([x_center; y_center; z_center], 1, size(finalMatrix,2));

theta = 2*pi/3;

R = [cos(theta) -sin(theta) 0; ...
     sin(theta)  cos(theta) 0; ...
             0           0  1];
         
s = finalMatrix - center; 
so = R*s;
finalMatrix1 = so + center;
s2 = finalMatrix1 -center;
so2 = R*s2;
finalMatrix2 = so2 + center;

finalMatrix = finalMatrix';
finalMatrix1 = finalMatrix1';
finalMatrix2 = finalMatrix2';

origins = [finalMatrix;finalMatrix1;finalMatrix2];

%plot3(finalMatrixfinal(:,1),finalMatrixfinal(:,2),finalMatrixfinal(:,3),'linestyle', 'none','Marker','*','MarkerSize',1 )
practiceOld = origins;
    practiceNew = zeros(size(practiceOld,1) , size(practiceOld,2));
    % loop through all of your hits' xyz coordinates and change the coordinates
    % such that they are located at the same position of the center of the
    % nearest voxel. (ie; change the coordinates so they end up at the nearest
    % half value (0.5 1.5 2.5 ...)
    for i = 1:size(origins,1)
        practiceNew(i,:) = ceil(practiceOld(i,:));
        for j = 1:3
           if practiceOld(i,j) < practiceNew(i,j)
               practiceNew(i,j) = practiceNew(i,j)-.5;
           elseif practiceOld(i,j) > practiceNew(i,j)
               practiceNew(i,j) = practiceNew(i,j)+.5;
           else
               practiceNew(i,j) = practiceNew(i,j)+.5;
           end
        end
    end
    
    origins = practiceNew;
    origins( ~any(origins,2), : ) = [];

az = 0;
el = 90;
view(az, el);





%}
dlmwrite('detectorVoxelsRotated.txt',origins);
%}


