
-cd to this folder

First you have to create your detector geometry out of XYZ coordinate that define the centre of each 1 mm voxels. An example of this code for a box shape geometry is called creatDetectorVoxels.m. The output of this file is txt file that has three columns of x, y, z of all the voxels. In this example is called “detectorVoxelsRotated.txt”.

HeatMapVariable.m gets the *.hit from Gate simulation and the system voxels coordinators explained above (detectorVoxelsRotated.txt)and gives the “VariableOutput Rotated” which is the number of hits in each voxels.

165 GB two hours 

Then you can rum:
HeatMapVisulization  

get in “VariableOutput Rotated” and cameraInfo (gca properties of your system geometry)and visualize data

Or you can partition first and then run the visualization 

HeatMapPartitioning


HeatMapVisulization 