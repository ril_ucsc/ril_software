%% Visualization
clear 
close 
close all
%Read in output from HeatMapPartitioning code
load('VariableOutputRotated')
load('cameraInfo');
% IF WANT FULL BLOCK ==1 IF WANT HALF BLOCK ==2
DefineValue = 1;
% IF PLOT DOESN'T LOOK GOOD (AXIS OR WHATEVER) GO TO BOTTOM AND SET GCA
% PROPERTIES AS YOU WANT / You will also have to set the "Patch" code to
% your geometry. 
%% I RECOMMEND FOLDING THIS CODE SINCE IT IS JUST MONOTONOUS
% Define Color map values
c = jet(65);
%Preallocation of memory for 32 different Possible module colors
origin0 = zeros(size(origins,1) , size(origins,2));
origin1 = zeros(size(origins,1) , size(origins,2));
origin2 = zeros(size(origins,1) , size(origins,2));
origin3 = zeros(size(origins,1) , size(origins,2));
origin4 = zeros(size(origins,1) , size(origins,2));
origin5 = zeros(size(origins,1) , size(origins,2));
origin6 = zeros(size(origins,1) , size(origins,2));
origin7 = zeros(size(origins,1) , size(origins,2));
origin8 = zeros(size(origins,1) , size(origins,2));
origin9 = zeros(size(origins,1) , size(origins,2));
origin10 = zeros(size(origins,1) , size(origins,2));
origin11 = zeros(size(origins,1) , size(origins,2));
origin12 = zeros(size(origins,1) , size(origins,2));
origin13 = zeros(size(origins,1) , size(origins,2));
origin14 = zeros(size(origins,1) , size(origins,2));
origin15 = zeros(size(origins,1) , size(origins,2));
origin16 = zeros(size(origins,1) , size(origins,2));
origin17 = zeros(size(origins,1) , size(origins,2));
origin18 = zeros(size(origins,1) , size(origins,2));
origin19 = zeros(size(origins,1) , size(origins,2));
origin20 = zeros(size(origins,1) , size(origins,2));
origin21 = zeros(size(origins,1) , size(origins,2));
origin22 = zeros(size(origins,1) , size(origins,2));
origin23 = zeros(size(origins,1) , size(origins,2));
origin24 = zeros(size(origins,1) , size(origins,2));
origin25 = zeros(size(origins,1) , size(origins,2));
origin26 = zeros(size(origins,1) , size(origins,2));
origin27 = zeros(size(origins,1) , size(origins,2));
origin28 = zeros(size(origins,1) , size(origins,2));
origin29 = zeros(size(origins,1) , size(origins,2));
origin30 = zeros(size(origins,1) , size(origins,2));
origin31 = zeros(size(origins,1) , size(origins,2));
origin32 = zeros(size(origins,1) , size(origins,2));
origin33 = zeros(size(origins,1) , size(origins,2));
origin34 = zeros(size(origins,1) , size(origins,2));
origin35 = zeros(size(origins,1) , size(origins,2));
origin36 = zeros(size(origins,1) , size(origins,2));
origin37 = zeros(size(origins,1) , size(origins,2));
origin38 = zeros(size(origins,1) , size(origins,2));
origin39 = zeros(size(origins,1) , size(origins,2));
origin40 = zeros(size(origins,1) , size(origins,2));
origin41 = zeros(size(origins,1) , size(origins,2));
origin42 = zeros(size(origins,1) , size(origins,2));
origin43 = zeros(size(origins,1) , size(origins,2));
origin44 = zeros(size(origins,1) , size(origins,2));
origin45 = zeros(size(origins,1) , size(origins,2));
origin46 = zeros(size(origins,1) , size(origins,2));
origin47 = zeros(size(origins,1) , size(origins,2));
origin48 = zeros(size(origins,1) , size(origins,2));
origin49 = zeros(size(origins,1) , size(origins,2));
origin50 = zeros(size(origins,1) , size(origins,2));
origin51 = zeros(size(origins,1) , size(origins,2));
origin52 = zeros(size(origins,1) , size(origins,2));
origin53 = zeros(size(origins,1) , size(origins,2));
origin54 = zeros(size(origins,1) , size(origins,2));
origin55 = zeros(size(origins,1) , size(origins,2));
origin56 = zeros(size(origins,1) , size(origins,2));
origin57 = zeros(size(origins,1) , size(origins,2));
origin58 = zeros(size(origins,1) , size(origins,2));
origin59 = zeros(size(origins,1) , size(origins,2));
origin60 = zeros(size(origins,1) , size(origins,2));
origin61 = zeros(size(origins,1) , size(origins,2));
origin62 = zeros(size(origins,1) , size(origins,2));
origin63 = zeros(size(origins,1) , size(origins,2));
origin64 = zeros(size(origins,1) , size(origins,2));

% preallocation for later recall
originc0 = [];
originc1 = [];
originc2 = [];
originc3 = [];
originc4 = [];
originc5 = [];
originc6 = [];
originc7 = [];
originc8 = [];
originc9 = [];
originc10 = [];
originc11 = [];
originc12 = [];
originc13 = [];
originc14 = [];
originc15 = [];
originc16 = [];
originc17 = [];
originc18 = [];
originc19 = [];
originc20 = [];
originc21 = [];
originc22 = [];
originc23 = [];
originc24 = [];
originc25 = [];
originc26 = [];
originc27 = [];
originc28 = [];
originc29 = [];
originc30 = [];
originc31 = [];
originc32 = [];
originc33 = [];
originc34 = [];
originc35 = [];
originc36 = [];
originc37 = [];
originc38 = [];
originc39 = [];
originc40 = [];
originc41 = [];
originc42 = [];
originc43 = [];
originc44 = [];
originc45 = [];
originc46 = [];
originc47 = [];
originc48 = [];
originc49 = [];
originc50 = [];
originc51 = [];
originc52 = [];
originc53 = [];
originc54 = [];
originc55 = [];
originc56 = [];
originc57 = [];
originc58 = [];
originc59 = [];
originc60 = [];
originc61 = [];
originc62 = [];
originc63 = [];
originc64 = [];
%%
%Divide up Modules into 33 different sections to later assign a color to
%them based on the amount of hits that occured in that module

%Cut in Half
%
if DefineValue == 2
    for p=1:size(origins,1)
        if origins(p,1) >0 %|| origins(p,2)<0
            origins(p,:) = zeros(1,size(origins,2));
        end
    end
    origins( ~any(origins,2), : ) = [];
end
%}

h = max(origins(:,4));
val = h/64;
%% I AGAIN RECOMMEND FOLDING THIS CODE
for i=1:size(origins(:,1),1)
    if origins(i,4) == 0
        origin0(i,:) = origins(i,:);
        originc0 = c(1,:); 
    elseif origins(i,4)>val*0 && origins(i,4)<=val*1
        origin1(i,:) = origins(i,:);
        originc1 = c(2,:);
    elseif origins(i,4)>val*1 && origins(i,4)<=val*2
        origin2(i,:) = origins(i,:);
        originc2 = c(3,:);
    elseif origins(i,4)>val*2 && origins(i,4)<=val*3
        origin3(i,:) = origins(i,:);
        originc3 = c(4,:);
    elseif origins(i,4)>val*3 && origins(i,4)<=val*4
        origin4(i,:) = origins(i,:);
        originc4 = c(5,:);
    elseif origins(i,4)>val*4 && origins(i,4)<=val*5
        origin5(i,:) = origins(i,:);
        originc5 = c(6,:);
    elseif origins(i,4)>val*5 && origins(i,4)<=val*6
        origin6(i,:) = origins(i,:);
        originc6 = c(7,:);
    elseif origins(i,4)>val*6 && origins(i,4)<=val*7
        origin7(i,:) = origins(i,:);
        originc7 = c(8,:);
    elseif origins(i,4)>val*7 && origins(i,4)<=val*8
        origin8(i,:) = origins(i,:);
        originc8 = c(9,:);
    elseif origins(i,4)>val*8 && origins(i,4)<=val*9
        origin9(i,:) = origins(i,:);
        originc9 = c(10,:);
    elseif origins(i,4)>val*9 && origins(i,4)<=val*10
        origin10(i,:) = origins(i,:);
        originc10 = c(11,:);
    elseif origins(i,4)>val*10 && origins(i,4)<=val*11
        origin11(i,:) = origins(i,:);
        originc11 = c(12,:);
    elseif origins(i,4)>val*11 && origins(i,4)<=val*12
        origin12(i,:) = origins(i,:);
        originc12 = c(13,:);
    elseif origins(i,4)>val*12 && origins(i,4)<=val*13
        origin13(i,:) = origins(i,:);
        originc13 = c(14,:);
    elseif origins(i,4)>val*13 && origins(i,4)<=val*14
        origin14(i,:) = origins(i,:);
        originc14 = c(15,:);
    elseif origins(i,4)>val*14 && origins(i,4)<=val*15
        origin15(i,:) = origins(i,:);
        originc15 = c(16,:);
    elseif origins(i,4)>val*15 && origins(i,4)<=val*16
        origin16(i,:) = origins(i,:);
        originc16 = c(17,:);
    elseif origins(i,4)>val*16 && origins(i,4)<=val*17
        origin17(i,:) = origins(i,:);
        originc17 = c(18,:);
    elseif origins(i,4)>val*17 && origins(i,4)<=val*18
        origin18(i,:) = origins(i,:);
        originc18 = c(19,:);
    elseif origins(i,4)>val*18 && origins(i,4)<=val*19
        origin19(i,:) = origins(i,:);
        originc19 = c(20,:);
    elseif origins(i,4)>val*19 && origins(i,4)<=val*20
        origin20(i,:) = origins(i,:);
        originc20 = c(21,:);
    elseif origins(i,4)>val*20 && origins(i,4)<=val*21
        origin21(i,:) = origins(i,:);
        originc21 = c(22,:);
    elseif origins(i,4)>val*21 && origins(i,4)<=val*22
        origin22(i,:) = origins(i,:);
        originc22 = c(23,:);
    elseif origins(i,4)>val*22 && origins(i,4)<=val*23
        origin23(i,:) = origins(i,:);
        originc23 = c(24,:);
    elseif origins(i,4)>val*23 && origins(i,4)<=val*24
        origin24(i,:) = origins(i,:);
        originc24 = c(25,:);
    elseif origins(i,4)>val*24 && origins(i,4)<=val*25
        origin25(i,:) = origins(i,:);
        originc25 = c(26,:);
    elseif origins(i,4)>val*25 && origins(i,4)<=val*26
        origin26(i,:) = origins(i,:);
        originc26 = c(27,:);
    elseif origins(i,4)>val*26 && origins(i,4)<=val*27
        origin27(i,:) = origins(i,:);
        originc27 = c(28,:);
    elseif origins(i,4)>val*27 && origins(i,4)<=val*28
        origin28(i,:) = origins(i,:);
        originc28= c(29,:);
    elseif origins(i,4)>val*28 && origins(i,4)<=val*29
        origin29(i,:) = origins(i,:);
        originc29 = c(30,:);
    elseif origins(i,4)>val*29 && origins(i,4)<=val*30
        origin30(i,:) = origins(i,:);
        originc30 = c(31,:);
    elseif origins(i,4)>val*30 && origins(i,4)<=val*31
        origin31(i,:) = origins(i,:);
        originc31 = c(32,:);
    elseif origins(i,4)>val*31 && origins(i,4)<=val*32
        origin32(i,:) = origins(i,:);
        originc32 = c(33,:);
    elseif origins(i,4)>val*32 && origins(i,4)<=val*33
        origin33(i,:) = origins(i,:);
        originc33 = c(34,:);
    elseif origins(i,4)>val*33 && origins(i,4)<=val*34
        origin34(i,:) = origins(i,:);
        originc34 = c(35,:);
    elseif origins(i,4)>val*34 && origins(i,4)<=val*35
        origin35(i,:) = origins(i,:);
        originc35 = c(36,:);
    elseif origins(i,4)>val*35 && origins(i,4)<=val*36
        origin36(i,:) = origins(i,:);
        originc36 = c(37,:);
    elseif origins(i,4)>val*36 && origins(i,4)<=val*37
        origin37(i,:) = origins(i,:);
        originc37 = c(38,:);
    elseif origins(i,4)>val*37 && origins(i,4)<=val*38
        origin38(i,:) = origins(i,:);
        originc38 = c(39,:);
    elseif origins(i,4)>val*38 && origins(i,4)<=val*39
        origin39(i,:) = origins(i,:);
        originc39 = c(40,:);
    elseif origins(i,4)>val*39 && origins(i,4)<=val*40
        origin40(i,:) = origins(i,:);
        originc40 = c(41,:);
    elseif origins(i,4)>val*40 && origins(i,4)<=val*41
        origin41(i,:) = origins(i,:);
        originc41 = c(42,:);
    elseif origins(i,4)>val*41 && origins(i,4)<=val*42
        origin42(i,:) = origins(i,:);
        originc42 = c(43,:);
    elseif origins(i,4)>val*42 && origins(i,4)<=val*43
        origin43(i,:) = origins(i,:);
        originc43 = c(44,:);
    elseif origins(i,4)>val*43 && origins(i,4)<=val*44
        origin44(i,:) = origins(i,:);
        originc44 = c(45,:);
    elseif origins(i,4)>val*44 && origins(i,4)<=val*45
        origin45(i,:) = origins(i,:);
        originc45 = c(46,:);
    elseif origins(i,4)>val*45 && origins(i,4)<=val*46
        origin46(i,:) = origins(i,:);
        originc46 = c(47,:);
    elseif origins(i,4)>val*46 && origins(i,4)<=val*47
        origin47(i,:) = origins(i,:);
        originc47 = c(48,:);
    elseif origins(i,4)>val*47 && origins(i,4)<=val*48
        origin48(i,:) = origins(i,:);
        originc48 = c(49,:);
    elseif origins(i,4)>val*48 && origins(i,4)<=val*49
        origin49(i,:) = origins(i,:);
        originc49 = c(50,:);
    elseif origins(i,4)>val*49 && origins(i,4)<=val*50
        origin50(i,:) = origins(i,:);
        originc50 = c(51,:);
    elseif origins(i,4)>val*50 && origins(i,4)<=val*51
        origin51(i,:) = origins(i,:);
        originc51 = c(52,:);
    elseif origins(i,4)>val*51 && origins(i,4)<=val*52
        origin52(i,:) = origins(i,:);
        originc52 = c(53,:);
    elseif origins(i,4)>val*52 && origins(i,4)<=val*53
        origin53(i,:) = origins(i,:);
        originc53 = c(54,:);
    elseif origins(i,4)>val*53 && origins(i,4)<=val*54
        origin54(i,:) = origins(i,:);
        originc54 = c(55,:);
    elseif origins(i,4)>val*54 && origins(i,4)<=val*55
        origin55(i,:) = origins(i,:);
        originc55 = c(56,:);
    elseif origins(i,4)>val*55 && origins(i,4)<=val*56
        origin56(i,:) = origins(i,:);
        originc56 = c(57,:);
    elseif origins(i,4)>val*56 && origins(i,4)<=val*57
        origin57(i,:) = origins(i,:);
        originc57 = c(58,:);
    elseif origins(i,4)>val*57 && origins(i,4)<=val*58
        origin58(i,:) = origins(i,:);
        originc58 = c(59,:);
    elseif origins(i,4)>val*58 && origins(i,4)<=val*59
        origin59(i,:) = origins(i,:);
        originc59 = c(60,:);
    elseif origins(i,4)>val*59 && origins(i,4)<=val*60
        origin60(i,:) = origins(i,:);
        originc60 = c(61,:);
    elseif origins(i,4)>val*60 && origins(i,4)<=val*61
        origin61(i,:) = origins(i,:);
        originc61 = c(62,:);
    elseif origins(i,4)>val*61 && origins(i,4)<=val*62
        origin62(i,:) = origins(i,:);
        originc62 = c(63,:);
    elseif origins(i,4)>val*62 && origins(i,4)<=val*63
        origin63(i,:) = origins(i,:);
        originc63 = c(64,:);
    elseif origins(i,4)>val*63 && origins(i,4)<=val*64
        origin64(i,:) = origins(i,:);
        originc64 = c(65,:);
    end
end
% Delete any rows that ended up being all zeros
origin0( ~any(origin0,2), : ) = [];
origin1( ~any(origin1,2), : ) = [];
origin2( ~any(origin2,2), : ) = [];
origin3( ~any(origin3,2), : ) = [];
origin4( ~any(origin4,2), : ) = [];
origin5( ~any(origin5,2), : ) = [];
origin6( ~any(origin6,2), : ) = [];
origin7( ~any(origin7,2), : ) = [];
origin8( ~any(origin8,2), : ) = [];
origin9( ~any(origin9,2), : ) = [];
origin10( ~any(origin10,2), : ) = [];
origin11( ~any(origin11,2), : ) = [];
origin12( ~any(origin12,2), : ) = [];
origin13( ~any(origin13,2), : ) = [];
origin14( ~any(origin14,2), : ) = [];
origin15( ~any(origin15,2), : ) = [];
origin16( ~any(origin16,2), : ) = [];
origin17( ~any(origin17,2), : ) = [];
origin18( ~any(origin18,2), : ) = [];
origin19( ~any(origin19,2), : ) = [];
origin20( ~any(origin20,2), : ) = [];
origin21( ~any(origin21,2), : ) = [];
origin22( ~any(origin22,2), : ) = [];
origin23( ~any(origin23,2), : ) = [];
origin24( ~any(origin24,2), : ) = [];
origin25( ~any(origin25,2), : ) = [];
origin26( ~any(origin26,2), : ) = [];
origin27( ~any(origin27,2), : ) = [];
origin28( ~any(origin28,2), : ) = [];
origin29( ~any(origin29,2), : ) = [];
origin30( ~any(origin30,2), : ) = [];
origin31( ~any(origin31,2), : ) = [];
origin32( ~any(origin32,2), : ) = [];
origin33( ~any(origin33,2), : ) = [];
origin34( ~any(origin34,2), : ) = [];
origin35( ~any(origin35,2), : ) = [];
origin36( ~any(origin36,2), : ) = [];
origin37( ~any(origin37,2), : ) = [];
origin38( ~any(origin38,2), : ) = [];
origin39( ~any(origin39,2), : ) = [];
origin40( ~any(origin40,2), : ) = [];
origin41( ~any(origin41,2), : ) = [];
origin42( ~any(origin42,2), : ) = [];
origin43( ~any(origin43,2), : ) = [];
origin44( ~any(origin44,2), : ) = [];
origin45( ~any(origin45,2), : ) = [];
origin46( ~any(origin46,2), : ) = [];
origin47( ~any(origin47,2), : ) = [];
origin48( ~any(origin48,2), : ) = [];
origin49( ~any(origin49,2), : ) = [];
origin50( ~any(origin50,2), : ) = [];
origin51( ~any(origin51,2), : ) = [];
origin52( ~any(origin52,2), : ) = [];
origin53( ~any(origin53,2), : ) = [];
origin54( ~any(origin54,2), : ) = [];
origin55( ~any(origin55,2), : ) = [];
origin56( ~any(origin56,2), : ) = [];
origin57( ~any(origin57,2), : ) = [];
origin58( ~any(origin58,2), : ) = [];
origin59( ~any(origin59,2), : ) = [];
origin60( ~any(origin60,2), : ) = [];
origin61( ~any(origin61,2), : ) = [];
origin62( ~any(origin62,2), : ) = [];
origin63( ~any(origin63,2), : ) = [];
origin64( ~any(origin64,2), : ) = [];
%%
%Plot all different modules with their now designated colors according to
%teh JET color map
figure(99)
hold on
%% FOR CODE FOLDING
plot3(origin0(:,1),origin0(:,2),origin0(:,3),'linestyle', 'none','Color',originc0,'Marker','s','MarkerSize',1 )
plot3(origin1(:,1),origin1(:,2),origin1(:,3),'linestyle', 'none','Color',originc1,'Marker','s','MarkerSize',1 )
plot3(origin2(:,1),origin2(:,2),origin2(:,3),'linestyle', 'none','Color',originc2,'Marker','s','MarkerSize',1 )
plot3(origin3(:,1),origin3(:,2),origin3(:,3),'linestyle', 'none','Color',originc3,'Marker','s','MarkerSize',1 )
plot3(origin4(:,1),origin4(:,2),origin4(:,3),'linestyle', 'none','Color',originc4,'Marker','s','MarkerSize',1 )
plot3(origin5(:,1),origin5(:,2),origin5(:,3),'linestyle', 'none','Color',originc5,'Marker','s','MarkerSize',1 )
plot3(origin6(:,1),origin6(:,2),origin6(:,3),'linestyle', 'none','Color',originc6,'Marker','s','MarkerSize',1 )
plot3(origin7(:,1),origin7(:,2),origin7(:,3),'linestyle', 'none','Color',originc7,'Marker','s','MarkerSize',1 )
plot3(origin8(:,1),origin8(:,2),origin8(:,3),'linestyle', 'none','Color',originc8,'Marker','s','MarkerSize',1 )
plot3(origin9(:,1),origin9(:,2),origin9(:,3),'linestyle', 'none','Color',originc9,'Marker','s','MarkerSize',1 )
plot3(origin10(:,1),origin10(:,2),origin10(:,3),'linestyle', 'none','Color',originc10,'Marker','s','MarkerSize',1 )
plot3(origin11(:,1),origin11(:,2),origin11(:,3),'linestyle', 'none','Color',originc11,'Marker','s','MarkerSize',1 )
plot3(origin12(:,1),origin12(:,2),origin12(:,3),'linestyle', 'none','Color',originc12,'Marker','s','MarkerSize',1 )
plot3(origin13(:,1),origin13(:,2),origin13(:,3),'linestyle', 'none','Color',originc13,'Marker','s','MarkerSize',1 )
plot3(origin14(:,1),origin14(:,2),origin14(:,3),'linestyle', 'none','Color',originc14,'Marker','s','MarkerSize',1 )
plot3(origin15(:,1),origin15(:,2),origin15(:,3),'linestyle', 'none','Color',originc15,'Marker','s','MarkerSize',1 )
plot3(origin16(:,1),origin16(:,2),origin16(:,3),'linestyle', 'none','Color',originc16,'Marker','s','MarkerSize',1 )
plot3(origin17(:,1),origin17(:,2),origin17(:,3),'linestyle', 'none','Color',originc17,'Marker','s','MarkerSize',1 )
plot3(origin18(:,1),origin18(:,2),origin18(:,3),'linestyle', 'none','Color',originc18,'Marker','s','MarkerSize',1 )
plot3(origin19(:,1),origin19(:,2),origin19(:,3),'linestyle', 'none','Color',originc19,'Marker','s','MarkerSize',1 )
plot3(origin20(:,1),origin20(:,2),origin20(:,3),'linestyle', 'none','Color',originc20,'Marker','s','MarkerSize',1 )
plot3(origin21(:,1),origin21(:,2),origin21(:,3),'linestyle', 'none','Color',originc21,'Marker','s','MarkerSize',1 )
plot3(origin22(:,1),origin22(:,2),origin22(:,3),'linestyle', 'none','Color',originc22,'Marker','s','MarkerSize',1 )
plot3(origin23(:,1),origin23(:,2),origin23(:,3),'linestyle', 'none','Color',originc23,'Marker','s','MarkerSize',1 )
plot3(origin24(:,1),origin24(:,2),origin24(:,3),'linestyle', 'none','Color',originc24,'Marker','s','MarkerSize',1 )
plot3(origin25(:,1),origin25(:,2),origin25(:,3),'linestyle', 'none','Color',originc25,'Marker','s','MarkerSize',1 )
plot3(origin26(:,1),origin26(:,2),origin26(:,3),'linestyle', 'none','Color',originc26,'Marker','s','MarkerSize',1 )
plot3(origin27(:,1),origin27(:,2),origin27(:,3),'linestyle', 'none','Color',originc27,'Marker','s','MarkerSize',1 )
plot3(origin28(:,1),origin28(:,2),origin28(:,3),'linestyle', 'none','Color',originc28,'Marker','s','MarkerSize',1 )
plot3(origin29(:,1),origin29(:,2),origin29(:,3),'linestyle', 'none','Color',originc29,'Marker','s','MarkerSize',1 )
plot3(origin30(:,1),origin30(:,2),origin30(:,3),'linestyle', 'none','Color',originc30,'Marker','s','MarkerSize',1 )
plot3(origin31(:,1),origin31(:,2),origin31(:,3),'linestyle', 'none','Color',originc31,'Marker','s','MarkerSize',1 )
plot3(origin32(:,1),origin32(:,2),origin32(:,3),'linestyle', 'none','Color',originc32,'Marker','s','MarkerSize',1 )
plot3(origin33(:,1),origin33(:,2),origin33(:,3),'linestyle', 'none','Color',originc33,'Marker','s','MarkerSize',1 )
plot3(origin34(:,1),origin34(:,2),origin34(:,3),'linestyle', 'none','Color',originc34,'Marker','s','MarkerSize',1 )
plot3(origin35(:,1),origin35(:,2),origin35(:,3),'linestyle', 'none','Color',originc35,'Marker','s','MarkerSize',1 )
plot3(origin36(:,1),origin36(:,2),origin36(:,3),'linestyle', 'none','Color',originc36,'Marker','s','MarkerSize',1 )
plot3(origin37(:,1),origin37(:,2),origin37(:,3),'linestyle', 'none','Color',originc37,'Marker','s','MarkerSize',1 )
plot3(origin38(:,1),origin38(:,2),origin38(:,3),'linestyle', 'none','Color',originc38,'Marker','s','MarkerSize',1 )
plot3(origin39(:,1),origin39(:,2),origin39(:,3),'linestyle', 'none','Color',originc39,'Marker','s','MarkerSize',1 )
plot3(origin40(:,1),origin40(:,2),origin40(:,3),'linestyle', 'none','Color',originc40,'Marker','s','MarkerSize',1 )
plot3(origin41(:,1),origin41(:,2),origin41(:,3),'linestyle', 'none','Color',originc41,'Marker','s','MarkerSize',1 )
plot3(origin42(:,1),origin42(:,2),origin42(:,3),'linestyle', 'none','Color',originc42,'Marker','s','MarkerSize',1 )
plot3(origin43(:,1),origin43(:,2),origin43(:,3),'linestyle', 'none','Color',originc43,'Marker','s','MarkerSize',1 )
plot3(origin44(:,1),origin44(:,2),origin44(:,3),'linestyle', 'none','Color',originc44,'Marker','s','MarkerSize',1 )
plot3(origin45(:,1),origin45(:,2),origin45(:,3),'linestyle', 'none','Color',originc45,'Marker','s','MarkerSize',1 )
plot3(origin46(:,1),origin46(:,2),origin46(:,3),'linestyle', 'none','Color',originc46,'Marker','s','MarkerSize',1 )
plot3(origin47(:,1),origin47(:,2),origin47(:,3),'linestyle', 'none','Color',originc47,'Marker','s','MarkerSize',1 )
plot3(origin48(:,1),origin48(:,2),origin48(:,3),'linestyle', 'none','Color',originc48,'Marker','s','MarkerSize',1 )
plot3(origin49(:,1),origin49(:,2),origin49(:,3),'linestyle', 'none','Color',originc49,'Marker','s','MarkerSize',1 )
plot3(origin50(:,1),origin50(:,2),origin50(:,3),'linestyle', 'none','Color',originc50,'Marker','s','MarkerSize',1 )
plot3(origin51(:,1),origin51(:,2),origin51(:,3),'linestyle', 'none','Color',originc51,'Marker','s','MarkerSize',1 )
plot3(origin52(:,1),origin52(:,2),origin52(:,3),'linestyle', 'none','Color',originc52,'Marker','s','MarkerSize',1 )
plot3(origin53(:,1),origin53(:,2),origin53(:,3),'linestyle', 'none','Color',originc53,'Marker','s','MarkerSize',1 )
plot3(origin54(:,1),origin54(:,2),origin54(:,3),'linestyle', 'none','Color',originc54,'Marker','s','MarkerSize',1 )
plot3(origin55(:,1),origin55(:,2),origin55(:,3),'linestyle', 'none','Color',originc55,'Marker','s','MarkerSize',1 )
plot3(origin56(:,1),origin56(:,2),origin56(:,3),'linestyle', 'none','Color',originc56,'Marker','s','MarkerSize',1 )
plot3(origin57(:,1),origin57(:,2),origin57(:,3),'linestyle', 'none','Color',originc57,'Marker','s','MarkerSize',1 )
plot3(origin58(:,1),origin58(:,2),origin58(:,3),'linestyle', 'none','Color',originc58,'Marker','s','MarkerSize',1 )
plot3(origin59(:,1),origin59(:,2),origin59(:,3),'linestyle', 'none','Color',originc59,'Marker','s','MarkerSize',1 )
plot3(origin60(:,1),origin60(:,2),origin60(:,3),'linestyle', 'none','Color',originc60,'Marker','s','MarkerSize',1 )
plot3(origin61(:,1),origin61(:,2),origin61(:,3),'linestyle', 'none','Color',originc61,'Marker','s','MarkerSize',1 )
plot3(origin62(:,1),origin62(:,2),origin62(:,3),'linestyle', 'none','Color',originc62,'Marker','s','MarkerSize',1 )
plot3(origin63(:,1),origin63(:,2),origin63(:,3),'linestyle', 'none','Color',originc63,'Marker','s','MarkerSize',1 )
plot3(origin64(:,1),origin64(:,2),origin64(:,3),'linestyle', 'none','Color',originc64,'Marker','s','MarkerSize',1 )
%%
% Create the colorbar
%}
map = c;
colormap(map)
colorbar
%% Outline the 4 sectors for better visualization
% YOU WILL HAVE TO REDEFINE ALL OF THESE FOR YOUR SPECIFIC GEOMETRY IN
% ORDER TO MAKE IT LOOK NICE. NOTE: YOU DO NOT NEED THESE "PATCH" FUNCTIONS
% IN ORDER FOR THE CODE TO WORK. IT JUST OUTLINES YOUR BOXES
%
if DefineValue == 1
    vert1 = [40 -40 -40 ; 40 80 -40 ; 40 -40 40 ; 40 80 40;
             88 -40 -40 ; 88 80 -40 ; 88 -40 40 ; 88 80 40];
    face1 = [1 2 4 3; 2 6 8 4; 4 3 7 8; 1 5 7 3; 1 2 6 5; 5 6 8 7];
    patch('Vertices', vert1, 'Faces', face1,'FaceColor','none');
    vert2 = [vert1(:,1)-128,vert1(:,2)-40,vert1(:,3)];
    %patch('Vertices', vert2, 'Faces', face1,'FaceColor','none');
    vert3 = [-80 40 -40 ; -80 40 40 ; -80 88 -40 ; -80 88 40;
             40 40 -40 ; 40 40 40 ; 40 88 -40 ; 40 88 40];
    %patch('Vertices', vert3, 'Faces', face1,'FaceColor','none');
    vert4 = [vert3(:,1)+40,vert3(:,2)-128,vert3(:,3)];
    %patch('Vertices', vert4, 'Faces', face1,'FaceColor','none');
end

%
% for half block
%
if DefineValue ==2
    vert1 = [40 -40 -40 ; 40 80 -40 ; 40 -40 40 ; 40 80 40;
             88 -40 -40 ; 88 80 -40 ; 88 -40 40 ; 88 80 40];
    face1 = [1 2 4 3; 2 6 8 4; 4 3 7 8; 1 5 7 3; 1 2 6 5; 5 6 8 7];
    %patch('Vertices', vert1, 'Faces', face1,'FaceColor','none');
    vert2 = [vert1(:,1)-128,vert1(:,2)-40,vert1(:,3)];
    patch('Vertices', vert2, 'Faces', face1,'FaceColor','none');
    vert3 = [-80 40 -40 ; -80 40 40 ; -80 88 -40 ; -80 88 40;
             0 40 -40 ; 0 40 40 ; 0 88 -40 ; 0 88 40];
    patch('Vertices', vert3, 'Faces', face1,'FaceColor','none');
    vert4 = [vert3(:,1)+40,vert3(:,2)-128,vert3(:,3)];
    for i=1:8
        if vert4(i,1)==40
            vert4(i,1)=0;
        end
    end

    patch('Vertices', vert4, 'Faces', face1,'FaceColor','none');
end
%}
%Set Viewing angle
grid on
zlim([-50 50])
xlim([-120 120])
ylim([-120 120])
%Make it look nice
set(gca,g);
set(gca,'linewidth',2)
set(gca,'FontSize',24)
set(gca,'FontWeight','bold');
b = findobj('tag', 'Colorbar');
% modify pos according to your needs
set(gca,'position',[0.13 0.16 0.6 0.8]);
set(b,'position',[0.85 0.05 0.05 0.9]);
hold off




