clear 
close
close all
%% STEPS
% 1) read comments and run HeatMapVariable code First (This Code) 
% 2) read comments of HeatMapPartitioning code and run that code second
% 3) read comments of heatMapVariableVisualization code and run that code
%    third
%%
% When reading in the Hits file column 23 has been changed to numbers 
% based on what interaction occured as shown below

% PHOTO                 = 1
% COMPTON               = 2
% ANNIHILATION          = 3
% BREMMSTRAHLUNG        = 4
% ELECTRON IONISATION   = 5
% RAYLEIGH              = 6
% TRANSPORT             = 7
% GammaConversion       = 8
%% START CODE
% IN ORDER TO MAKE SURE YOU CAN READ IN YOUR GEOMETRY YOU MUST CREATE A
% FILE THAT CONTAINS THE VOXEL CENTER POSITIOIN IN THE FORMAT X,Y,Z FOR
% EVER 1MM*1MM*1MM VOXEL OF YOU SYSTEM. IF YOUR SYSTEM IS A 2MM CUBE THEN
% YOU WILL HAVE 8 POINTS THAT DEFINE THE CENTER OF THE 8 1MM^3 VOXELS THAT
% MAKES UP YOUR 2MM CUBE GEOMETRY THIS IS THEN READ IN AT
% "detectorGeometry" NOTE: YOUR DETECTOR GEOMETRY MUST END IN HALF INTEGERS
% IE .5. IF YOU ARE DOING A ROTATE GEOMETRY NOT IN A BOX SHAPE YOU MUST
% ROUND ALL VALUES TO THE .5 
%
% Filename of importing detector geometry
detectorGeometry = 'detectorVoxels.txt';
% Filename of saving workspace for use in visualization
outputFileName = 'VariableOutputWTF';
%%
% Read in Hits File (File has been split up into multiple smaller files so
% that my computer can handle it)
%
files = {'ascii_outputHitsSiCorrectaa','ascii_outputHitsSiCorrectab','ascii_outputHitsSiCorrectac',...
    'ascii_outputHitsSiCorrectad','ascii_outputHitsSiCorrectae','ascii_outputHitsSiCorrectaf',...
    'ascii_outputHitsSiCorrectag','ascii_outputHitsSiCorrectah','ascii_outputHitsSiCorrectai',...
    'ascii_outputHitsSiCorrectaj','ascii_outputHitsSiCorrectak','ascii_outputHitsSiCorrectal',...
    'ascii_outputHitsSiCorrectam','ascii_outputHitsSiCorrectan','ascii_outputHitsSiCorrectao',...
    'ascii_outputHitsSiCorrectap','ascii_outputHitsSiCorrectaq','ascii_outputHitsSiCorrectar',...
    'ascii_outputHitsSiCorrectas','ascii_outputHitsSiCorrectat','ascii_outputHitsSiCorrectau',...
    'ascii_outputHitsSiCorrectav','ascii_outputHitsSiCorrectaw','ascii_outputHitsSiCorrectax',...
    'ascii_outputHitsSiCorrectay','ascii_outputHitsSiCorrectaz','ascii_outputHitsSiCorrectba',...
    'ascii_outputHitsSiCorrectbb','ascii_outputHitsSiCorrectbc','ascii_outputHitsSiCorrectbd',...
    'ascii_outputHitsSiCorrectbe','ascii_outputHitsSiCorrectbf','ascii_outputHitsSiCorrectbg',...
    'ascii_outputHitsSiCorrectbh','ascii_outputHitsSiCorrectbi','ascii_outputHitsSiCorrectbj',...
    'ascii_outputHitsSiCorrectbk','ascii_outputHitsSiCorrectbl','ascii_outputHitsSiCorrectbm',...
    'ascii_outputHitsSiCorrectbn','ascii_outputHitsSiCorrectbo','ascii_outputHitsSiCorrectbp',...
    'ascii_outputHitsSiCorrectbq','ascii_outputHitsSiCorrectbr','ascii_outputHitsSiCorrectbs',...
    'ascii_outputHitsSiCorrectbt','ascii_outputHitsSiCorrectbu','ascii_outputHitsSiCorrectbv',...
    'ascii_outputHitsSiCorrectbw','ascii_outputHitsSiCorrectbx','ascii_outputHitsSiCorrectby',...
    'ascii_outputHitsSiCorrectbz','ascii_outputHitsSiCorrectca','ascii_outputHitsSiCorrectcb',...
    'ascii_outputHitsSiCorrectcc','ascii_outputHitsSiCorrectcd','ascii_outputHitsSiCorrectce',...
    'ascii_outputHitsSiCorrectcf','ascii_outputHitsSiCorrectcg','ascii_outputHitsSiCorrectch',...
    'ascii_outputHitsSiCorrectci','ascii_outputHitsSiCorrectcj','ascii_outputHitsSiCorrectck',...
    'ascii_outputHitsSiCorrectcl','ascii_outputHitsSiCorrectcm','ascii_outputHitsSiCorrectcn',...
    'ascii_outputHitsSiCorrectco'};
files = {'ascii_outputRotated.dat'};
%%
%Preallocate memory for speed
origins = zeros(20000000,4);
HitCoord = zeros(20000000,4);
originsOld= zeros(20000000,4);
%time the program
tic
%Loop over all files in the "files" Structure
% See how many files you are looping over
size(files,2)
for r = 1:size(files,2)
    %Read in one file at a time
    fileID = fopen(files{r});
    C2 = textscan(fileID,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %s %s %s');
    fclose(fileID);
    Hits = cell2mat(C2(12:16));
    % see r output to know where you are in your code
    r
    % Uncomment this to Define energy an energy window and get rid of hits outside of window
    %{
    for i =1: size(Hits(:,1),1)
        if Hits(i,1)*1000 < 450 || Hits(i,1)*1000 > 550
            Hits(i,:) = zeros(1,5);
        end  
    end
    Hits( ~any(Hits,2), : ) = [];
    %}

    % Extrapolate vectors for x,y,z coordinate positions for each hit
    X2 = Hits(:,3);
    Y2 = Hits(:,4);
    Z2 = Hits(:,5);
    % combine x,y,z position into one matrix
    hitCoord = [X2,Y2,Z2];
    hitCoord( ~any(hitCoord,2), : ) = [];
    %% Read in Detector Geometry built out of 1mmx1mmx1mm voxels
    fileID = fopen(detectorGeometry);
    origins = textscan(fileID,'%f,%f,%f');
    fclose(fileID);
    origins = cell2mat(origins(:,:));
    origins( ~any(origins,2), : ) = [];
    %%
    origins = [origins , zeros(size(origins,1),1)];
    % define an old and a new xyz matrix for comparison
    practiceOld = hitCoord;
    practiceNew = zeros(size(practiceOld,1) , size(practiceOld,2));
    % loop through all of your hits' xyz coordinates and change the coordinates
    % such that they are located at the same position of the center of the
    % nearest voxel. (ie; change the coordinates so they end up at the nearest
    % half value (0.5 1.5 2.5 ...)
    for i = 1:size(hitCoord,1)
        practiceNew(i,:) = ceil(practiceOld(i,:));
        for j = 1:3
           if practiceOld(i,j) < practiceNew(i,j)
               practiceNew(i,j) = practiceNew(i,j)-.5;
           elseif practiceOld(i,j) > practiceNew(i,j)
               practiceNew(i,j) = practiceNew(i,j)+.5;
           else
               practiceNew(i,j) = practiceNew(i,j)+.5;
           end
        end
    end
    % Redefine your hitCoord with the new corrected hit coordinates 
    HitCoord = practiceNew;
    HitCoord( ~any(HitCoord,2), : ) = [];
    %HitCoord = [HitCoord , zeros(size(HitCoord,1),1)];

    % These next lines will find all rows that have the same exact coordinates.
    % It will then count how often that row appears. It then creates a new
    % matrix with the position and how often that position appears. It then
    % finds the location of that coordinate in your detector geometry and
    % appends how often that voxel center was hit. For example if the
    % coordinate (0.5 1.5 2.5) showed up 3 times in your HITS file It will
    % append the number three to the row of that coordinate in the overall
    % detector geometry matrix
    HitCoord = sortrows(HitCoord);
    %HitCoord = HitCoord(:,1:end-1);
    S = [1;any(diff(HitCoord),2)];
    [L,S] = regexp(sprintf('%i',S'),'1(0)+','start','end');
    repeated_rows = HitCoord(S,:); % Repeated Rows.
    repeat_count = (S-L+1)'; % How often each repeated row appears.
    hitNoRep = [repeated_rows,repeat_count]; %Hits file without repeated rows
    [Lia,Locb] = ismember(hitNoRep(:,1:3),origins(:,1:3),'rows');
    %add the column with the number of hits at a coordinate to the fourth 
    %column of the overall detector geometry file. As it loops through each
    %file in files it will keep adding the hits at each coordinate.
    Locb( ~any(Locb,2), : ) = [];
    hitNoRep = hitNoRep(Lia,:);
    

    origins(Locb,4) = originsOld(Locb,4)+hitNoRep(:,4); 
    originsOld = origins;
    originsOld( ~any(originsOld,2), : ) = [];
end
toc
save(outputFileName);