clear 
close
close all
% When reading in the Hits file column 23 has been changed to numbers 
% based on what interaction occured as shown below

% PHOTO                 = 1
% COMPTON               = 2
% ANNIHILATION          = 3
% BREMMSTRAHLUNG        = 4
% ELECTRON IONISATION   = 5
% RAYLEIGH              = 6
% TRANSPORT             = 7
%% START CODE
%%
% Read in Hits File (File has been split up into multiple smaller files so
% that my computer can handle it)
files = {'ascii_outputHitsSiaa','ascii_outputHitsSiab','ascii_outputHitsSiac',...
    'ascii_outputHitsSiad','ascii_outputHitsSiae','ascii_outputHitsSiaf',...
    'ascii_outputHitsSiag','ascii_outputHitsSiah','ascii_outputHitsSiai',...
    'ascii_outputHitsSiaj','ascii_outputHitsSiak','ascii_outputHitsSial',...
    'ascii_outputHitsSiam','ascii_outputHitsSian','ascii_outputHitsSiao',...
    'ascii_outputHitsSiap','ascii_outputHitsSiaq','ascii_outputHitsSiar',...
    'ascii_outputHitsSias','ascii_outputHitsSiat','ascii_outputHitsSiau',...
    'ascii_outputHitsSiav','ascii_outputHitsSiaw','ascii_outputHitsSiax',...
    'ascii_outputHitsSiay','ascii_outputHitsSiaz','ascii_outputHitsSiba',...
    'ascii_outputHitsSibb','ascii_outputHitsSibc','ascii_outputHitsSibd',...
    'ascii_outputHitsSibe','ascii_outputHitsSibf','ascii_outputHitsSibg',...
    'ascii_outputHitsSibh','ascii_outputHitsSibi','ascii_outputHitsSibj',...
    'ascii_outputHitsSibk','ascii_outputHitsSibl','ascii_outputHitsSibm',...
    'ascii_outputHitsSibn','ascii_outputHitsSibo','ascii_outputHitsSibp',...
    'ascii_outputHitsSibq','ascii_outputHitsSibr','ascii_outputHitsSibs',...
    'ascii_outputHitsSibt','ascii_outputHitsSibu','ascii_outputHitsSibv',...
    'ascii_outputHitsSibw','ascii_outputHitsSibx','ascii_outputHitsSiby',...
    'ascii_outputHitsSibz','ascii_outputHitsSica','ascii_outputHitsSicb',...
    'ascii_outputHitsSicc','ascii_outputHitsSicd','ascii_outputHitsSice',...
    'ascii_outputHitsSicf','ascii_outputHitsSicg','ascii_outputHitsSich',...
    'ascii_outputHitsSici','ascii_outputHitsSicj','ascii_outputHitsSick',...
    'ascii_outputHitsSicl','ascii_outputHitsSicm','ascii_outputHitsSicn',...
    'ascii_outputHitsSico','ascii_outputHitsSicp','ascii_outputHitsSicq',...
    'ascii_outputHitsSicr','ascii_outputHitsSics','ascii_outputHitsSict',...
    'ascii_outputHitsSicu','ascii_outputHitsSicv','ascii_outputHitsSicw',...
    'ascii_outputHitsSicx','ascii_outputHitsSicy','ascii_outputHitsSicz',...
    'ascii_outputHitsSida','ascii_outputHitsSidb','ascii_outputHitsSidc',...
    'ascii_outputHitsSidd','ascii_outputHitsSide','ascii_outputHitsSidf',...
    'ascii_outputHitsSidg','ascii_outputHitsSidh','ascii_outputHitsSidi',...
    'ascii_outputHitsSidj','ascii_outputHitsSidk','ascii_outputHitsSidl',...
    'ascii_outputHitsSidm','ascii_outputHitsSidn','ascii_outputHitsSido',...
    'ascii_outputHitsSidp','ascii_outputHitsSidq','ascii_outputHitsSidr',...
    'ascii_outputHitsSids','ascii_outputHitsSidt','ascii_outputHitsSidu',...
    'ascii_outputHitsSidv','ascii_outputHitsSidw'
    };
%%
%Preallocate memory for speed
origins = zeros(7720062,4);
HitCoord = zeros(7720062,4);
originsOld= zeros(7720062,4);
%time the program
tic
%Loop over all files in the "files" Structure
for r = 1:size(files,2)
%Read in one file at a time
fileID = fopen(files{r});
C2 = textscan(fileID,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %s %s %s');
fclose(fileID);
Hits = cell2mat(C2(12:16));

% Define energy window and get rid of hits outside of window
for i =1: size(Hits(:,1),1)
    if Hits(i,1)*1000 < 450 || Hits(i,1)*1000 > 550
        Hits(i,:) = zeros(1,5);
    end  
end
Hits( ~any(Hits,2), : ) = [];
%}
% Extrapolate vectors for x,y,z coordinate positions for each hit
X2 = Hits(:,3);
Y2 = Hits(:,4);
Z2 = Hits(:,5);
% combine x,y,z position into one matrix
hitCoord = [X2,Y2,Z2];

% define most "negative" voxel center position for one of the modules
xyz = [40.5,-39.5,-39.5];
% Loop through every voxel position in one module to define a matrix
% that contains the x,y,z position of every voxel center within that module
finalMatrix = zeros(460800,3);
z = 1;
for i=1:48
    for j=1:120
        for k = 1:80
            finalMatrix(z,:)=[xyz(1)+i-1,xyz(2)+j-1,xyz(3)+k-1];
            z = z+1;
        end
    end
end
% do the same for a perpindicular module
xyz2 = [-39.5,-40.5,-39.5];
finalMatrix2 = zeros(460800,3);
z = 1;
for i=1:120
    for j=1:48
        for k = 1:80
            finalMatrix2(z,:)=[xyz2(1)+(i-1),xyz2(2)-(j-1),xyz2(3)+(k-1)];
            z = z+1;
        end
    end
end
%invert and shift those two modules to create your last two module matrices
finalMatrix3 = [finalMatrix(:,1)-128,finalMatrix(:,2)-40,finalMatrix(:,3)];
finalMatrix4 = [finalMatrix2(:,1)-40,finalMatrix2(:,2)+128,finalMatrix2(:,3)];
%combine all matrices into one matrix that defines entire detector geometry
origins = [finalMatrix;finalMatrix2;finalMatrix3;finalMatrix4];
%add column of zeros
origins = [origins , zeros(size(origins,1),1)];
% define an old and a new xyz matrix for comparison
practiceOld = hitCoord;
practiceNew = zeros(size(practiceOld,1) , size(practiceOld,2));
% loop through all of your hits' xyz coordinates and change the coordinates
% such that they are located at the same position of the center of the
% nearest voxel. (ie; change the coordinates so they end up at the nearest
% half value (0.5 1.5 2.5 ...)
for i = 1:size(hitCoord,1)
    practiceNew(i,:) = ceil(practiceOld(i,:));
    for j = 1:3
       if practiceOld(i,j) < practiceNew(i,j)
           practiceNew(i,j) = practiceNew(i,j)-.5;
       elseif practiceOld(i,j) > practiceNew(i,j)
           practiceNew(i,j) = practiceNew(i,j)+.5;
       else
           practiceNew(i,j) = practiceNew(i,j)+.5;
       end
    end
end
% Redefine your hitCoord with the new corrected hit coordinates 
HitCoord = practiceNew;
%HitCoord = [HitCoord , zeros(size(HitCoord,1),1)];

% These next lines will find all rows that have the same exact coordinates.
% It will then count how often that row appears. It then creates a new
% matrix with the position and how often that position appears. It then
% finds the location of that coordinate in your detector geometry and
% appends how often that voxel center was hit. For example if the
% coordinate (0.5 1.5 2.5) showed up 3 times in your HITS file It will
% append the number three to the row of that coordinate in the overall
% detector geometry matrix
HitCoord = sortrows(HitCoord);
%HitCoord = HitCoord(:,1:end-1);
S = [1;any(diff(HitCoord),2)];
[L,S] = regexp(sprintf('%i',S'),'1(0)+','start','end');
repeated_rows = HitCoord(S,:); % Repeated Rows.
repeat_count = (S-L+1)'; % How often each repeated row appears.
hitNoRep = [repeated_rows,repeat_count]; %Hits file without repeated rows
[Lia,Locb] = ismember(hitNoRep(:,1:3),origins(:,1:3),'rows');
%add the column with the number of hits at a coordinate to the fourth 
%column of the overall detector geometry file. As it loops through each
%file in files it will keep adding the hits at each coordinate.
origins(Locb,4) = originsOld(Locb,4)+hitNoRep(:,4); 

originsOld = origins;
%% This was for debugging purposes
if r==1
    at = origins;
elseif r==2
    bt = origins;
elseif r==3
    ct = origins;
elseif r==4
    dt = origins;
end

end
toc
%% Visualization
% Define Color map values and range of colors
colors = hsv(46);
colors = colors(1:33,:);
colors = flipud(colors);
%Preallocation of memory for 32 different Possible voxel colors
origin0 = zeros(size(origins,1) , size(origins,2));
origin1 = zeros(size(origins,1) , size(origins,2));
origin2 = zeros(size(origins,1) , size(origins,2));
origin3 = zeros(size(origins,1) , size(origins,2));
origin4 = zeros(size(origins,1) , size(origins,2));
origin5 = zeros(size(origins,1) , size(origins,2));
origin6 = zeros(size(origins,1) , size(origins,2));
origin7 = zeros(size(origins,1) , size(origins,2));
origin8 = zeros(size(origins,1) , size(origins,2));
origin9 = zeros(size(origins,1) , size(origins,2));
origin10 = zeros(size(origins,1) , size(origins,2));
origin11 = zeros(size(origins,1) , size(origins,2));
origin12 = zeros(size(origins,1) , size(origins,2));
origin13 = zeros(size(origins,1) , size(origins,2));
origin14 = zeros(size(origins,1) , size(origins,2));
origin15 = zeros(size(origins,1) , size(origins,2));
origin16 = zeros(size(origins,1) , size(origins,2));
origin17 = zeros(size(origins,1) , size(origins,2));
origin18 = zeros(size(origins,1) , size(origins,2));
origin19 = zeros(size(origins,1) , size(origins,2));
origin20 = zeros(size(origins,1) , size(origins,2));
origin21 = zeros(size(origins,1) , size(origins,2));
origin22 = zeros(size(origins,1) , size(origins,2));
origin23 = zeros(size(origins,1) , size(origins,2));
origin24 = zeros(size(origins,1) , size(origins,2));
origin25 = zeros(size(origins,1) , size(origins,2));
origin26 = zeros(size(origins,1) , size(origins,2));
origin27 = zeros(size(origins,1) , size(origins,2));
origin28 = zeros(size(origins,1) , size(origins,2));
origin29 = zeros(size(origins,1) , size(origins,2));
origin30 = zeros(size(origins,1) , size(origins,2));
origin31 = zeros(size(origins,1) , size(origins,2));
origin32 = zeros(size(origins,1) , size(origins,2));
% preallocation for later recall
originc0 = [];
originc1 = [];
originc2 = [];
originc3 = [];
originc4 = [];
originc5 = [];
originc6 = [];
originc7 = [];
originc8 = [];
originc9 = [];
originc10 = [];
originc11 = [];
originc12 = [];
originc13 = [];
originc14 = [];
originc15 = [];
originc16 = [];
originc17 = [];
originc18 = [];
originc19 = [];
originc20 = [];
originc21 = [];
originc22 = [];
originc23 = [];
originc24 = [];
originc25 = [];
originc26 = [];
originc27 = [];
originc28 = [];
originc29 = [];
originc30 = [];
originc31 = [];
originc32 = [];

%Divide up Modules into 33 different sections to later assign a color to
%them based on the amount of hits that occured in that module
maxCounts = max(origins(:,4));
val = maxCounts/32;

for i=1:size(origins(:,1),1)
    if origins(i,4) == 0
        origin0(i,:) = origins(i,:);
        originc0 = colors(1,:); 
    elseif origins(i,4)>val*0 && origins(i,4)<=val*1
        origin1(i,:) = origins(i,:);
        originc1 = colors(2,:);
    elseif origins(i,4)>val*1 && origins(i,4)<=val*2
        origin2(i,:) = origins(i,:);
        originc2 = colors(3,:);
    elseif origins(i,4)>val*2 && origins(i,4)<=val*3
        origin3(i,:) = origins(i,:);
        originc3 = colors(4,:);
    elseif origins(i,4)>val*3 && origins(i,4)<=val*4
        origin4(i,:) = origins(i,:);
        originc4 = colors(5,:);
    elseif origins(i,4)>val*4 && origins(i,4)<=val*5
        origin5(i,:) = origins(i,:);
        originc5 = colors(6,:);
    elseif origins(i,4)>val*5 && origins(i,4)<=val*6
        origin6(i,:) = origins(i,:);
        originc6 = colors(7,:);
    elseif origins(i,4)>val*6 && origins(i,4)<=val*7
        origin7(i,:) = origins(i,:);
        originc7 = colors(8,:);
    elseif origins(i,4)>val*7 && origins(i,4)<=val*8
        origin8(i,:) = origins(i,:);
        originc8 = colors(9,:);
    elseif origins(i,4)>val*8 && origins(i,4)<=val*9
        origin9(i,:) = origins(i,:);
        originc9 = colors(10,:);
    elseif origins(i,4)>val*9 && origins(i,4)<=val*10
        origin10(i,:) = origins(i,:);
        originc10 = colors(11,:);
    elseif origins(i,4)>val*10 && origins(i,4)<=val*11
        origin11(i,:) = origins(i,:);
        originc11 = colors(12,:);
    elseif origins(i,4)>val*11 && origins(i,4)<=val*12
        origin12(i,:) = origins(i,:);
        originc12 = colors(13,:);
    elseif origins(i,4)>val*12 && origins(i,4)<=val*13
        origin13(i,:) = origins(i,:);
        originc13 = colors(14,:);
    elseif origins(i,4)>val*13 && origins(i,4)<=val*14
        origin14(i,:) = origins(i,:);
        originc14 = colors(15,:);
    elseif origins(i,4)>val*14 && origins(i,4)<=val*15
        origin15(i,:) = origins(i,:);
        originc15 = colors(16,:);
    elseif origins(i,4)>val*15 && origins(i,4)<=val*16
        origin16(i,:) = origins(i,:);
        originc16 = colors(17,:);
    elseif origins(i,4)>val*16 && origins(i,4)<=val*17
        origin17(i,:) = origins(i,:);
        originc17 = colors(18,:);
    elseif origins(i,4)>val*17 && origins(i,4)<=val*18
        origin18(i,:) = origins(i,:);
        originc18 = colors(19,:);
    elseif origins(i,4)>val*18 && origins(i,4)<=val*19
        origin19(i,:) = origins(i,:);
        originc19 = colors(20,:);
    elseif origins(i,4)>val*19 && origins(i,4)<=val*20
        origin20(i,:) = origins(i,:);
        originc20 = colors(21,:);
    elseif origins(i,4)>val*20 && origins(i,4)<=val*21
        origin21(i,:) = origins(i,:);
        originc21 = colors(22,:);
    elseif origins(i,4)>val*21 && origins(i,4)<=val*22
        origin22(i,:) = origins(i,:);
        originc22 = colors(23,:);
    elseif origins(i,4)>val*22 && origins(i,4)<=val*23
        origin23(i,:) = origins(i,:);
        originc23 = colors(24,:);
    elseif origins(i,4)>val*23 && origins(i,4)<=val*24
        origin24(i,:) = origins(i,:);
        originc24 = colors(25,:);
    elseif origins(i,4)>val*24 && origins(i,4)<=val*25
        origin25(i,:) = origins(i,:);
        originc25 = colors(26,:);
    elseif origins(i,4)>val*25 && origins(i,4)<=val*26
        origin26(i,:) = origins(i,:);
        originc26 = colors(27,:);
    elseif origins(i,4)>val*26 && origins(i,4)<=val*27
        origin27(i,:) = origins(i,:);
        originc27 = colors(28,:);
    elseif origins(i,4)>val*27 && origins(i,4)<=val*28
        origin28(i,:) = origins(i,:);
        originc28= colors(29,:);
    elseif origins(i,4)>val*28 && origins(i,4)<=val*29
        origin29(i,:) = origins(i,:);
        originc29 = colors(30,:);
    elseif origins(i,4)>val*29 && origins(i,4)<=val*30
        origin30(i,:) = origins(i,:);
        originc30 = colors(31,:);
    elseif origins(i,4)>val*30 && origins(i,4)<=val*31
        origin31(i,:) = origins(i,:);
        originc31 = colors(32,:);
    elseif origins(i,4)>val*31 && origins(i,4)<=val*32
        origin32(i,:) = origins(i,:);
        originc32 = colors(33,:);
    end
end
% Delete any rows that ended up being all zeros
origin0( ~any(origin0,2), : ) = [];
origin1( ~any(origin1,2), : ) = [];
origin2( ~any(origin2,2), : ) = [];
origin3( ~any(origin3,2), : ) = [];
origin4( ~any(origin4,2), : ) = [];
origin5( ~any(origin5,2), : ) = [];
origin6( ~any(origin6,2), : ) = [];
origin7( ~any(origin7,2), : ) = [];
origin8( ~any(origin8,2), : ) = [];
origin9( ~any(origin9,2), : ) = [];
origin10( ~any(origin10,2), : ) = [];
origin11( ~any(origin11,2), : ) = [];
origin12( ~any(origin12,2), : ) = [];
origin13( ~any(origin13,2), : ) = [];
origin14( ~any(origin14,2), : ) = [];
origin15( ~any(origin15,2), : ) = [];
origin16( ~any(origin16,2), : ) = [];
origin17( ~any(origin17,2), : ) = [];
origin18( ~any(origin18,2), : ) = [];
origin19( ~any(origin19,2), : ) = [];
origin20( ~any(origin20,2), : ) = [];
origin21( ~any(origin21,2), : ) = [];
origin22( ~any(origin22,2), : ) = [];
origin23( ~any(origin23,2), : ) = [];
origin24( ~any(origin24,2), : ) = [];
origin25( ~any(origin25,2), : ) = [];
origin26( ~any(origin26,2), : ) = [];
origin27( ~any(origin27,2), : ) = [];
origin28( ~any(origin28,2), : ) = [];
origin29( ~any(origin29,2), : ) = [];
origin30( ~any(origin30,2), : ) = [];
origin31( ~any(origin31,2), : ) = [];
origin32( ~any(origin32,2), : ) = [];
%Plot all different voxels with their now designated colors according to
%the HSV color map


figure(99)
plot3(origin0(:,1),origin0(:,2),origin0(:,3),'linestyle', 'none','Color',originc0,'Marker','s','MarkerSize',1 )
hold on
plot3(origin1(:,1),origin1(:,2),origin1(:,3),'linestyle', 'none','Color',originc1,'Marker','s','MarkerSize',1 )
plot3(origin2(:,1),origin2(:,2),origin2(:,3),'linestyle', 'none','Color',originc2,'Marker','s','MarkerSize',1 )
plot3(origin3(:,1),origin3(:,2),origin3(:,3),'linestyle', 'none','Color',originc3,'Marker','s','MarkerSize',1 )
plot3(origin4(:,1),origin4(:,2),origin4(:,3),'linestyle', 'none','Color',originc4,'Marker','s','MarkerSize',1 )
plot3(origin5(:,1),origin5(:,2),origin5(:,3),'linestyle', 'none','Color',originc5,'Marker','s','MarkerSize',1 )
plot3(origin6(:,1),origin6(:,2),origin6(:,3),'linestyle', 'none','Color',originc6,'Marker','s','MarkerSize',1 )
plot3(origin7(:,1),origin7(:,2),origin7(:,3),'linestyle', 'none','Color',originc7,'Marker','s','MarkerSize',1 )
plot3(origin8(:,1),origin8(:,2),origin8(:,3),'linestyle', 'none','Color',originc8,'Marker','s','MarkerSize',1 )
plot3(origin9(:,1),origin9(:,2),origin9(:,3),'linestyle', 'none','Color',originc9,'Marker','s','MarkerSize',1 )
plot3(origin10(:,1),origin10(:,2),origin10(:,3),'linestyle', 'none','Color',originc10,'Marker','s','MarkerSize',1 )
plot3(origin11(:,1),origin11(:,2),origin11(:,3),'linestyle', 'none','Color',originc11,'Marker','s','MarkerSize',1 )
plot3(origin12(:,1),origin12(:,2),origin12(:,3),'linestyle', 'none','Color',originc12,'Marker','s','MarkerSize',1 )
plot3(origin13(:,1),origin13(:,2),origin13(:,3),'linestyle', 'none','Color',originc13,'Marker','s','MarkerSize',1 )
plot3(origin14(:,1),origin14(:,2),origin14(:,3),'linestyle', 'none','Color',originc14,'Marker','s','MarkerSize',1 )
plot3(origin15(:,1),origin15(:,2),origin15(:,3),'linestyle', 'none','Color',originc15,'Marker','s','MarkerSize',1 )
plot3(origin16(:,1),origin16(:,2),origin16(:,3),'linestyle', 'none','Color',originc16,'Marker','s','MarkerSize',1 )
plot3(origin17(:,1),origin17(:,2),origin17(:,3),'linestyle', 'none','Color',originc17,'Marker','s','MarkerSize',1 )
plot3(origin18(:,1),origin18(:,2),origin18(:,3),'linestyle', 'none','Color',originc18,'Marker','s','MarkerSize',1 )
plot3(origin19(:,1),origin19(:,2),origin19(:,3),'linestyle', 'none','Color',originc19,'Marker','s','MarkerSize',1 )
plot3(origin20(:,1),origin20(:,2),origin20(:,3),'linestyle', 'none','Color',originc20,'Marker','s','MarkerSize',1 )
plot3(origin21(:,1),origin21(:,2),origin21(:,3),'linestyle', 'none','Color',originc21,'Marker','s','MarkerSize',1 )
plot3(origin22(:,1),origin22(:,2),origin22(:,3),'linestyle', 'none','Color',originc22,'Marker','s','MarkerSize',1 )
plot3(origin23(:,1),origin23(:,2),origin23(:,3),'linestyle', 'none','Color',originc23,'Marker','s','MarkerSize',1 )
plot3(origin24(:,1),origin24(:,2),origin24(:,3),'linestyle', 'none','Color',originc24,'Marker','s','MarkerSize',1 )
plot3(origin25(:,1),origin25(:,2),origin25(:,3),'linestyle', 'none','Color',originc25,'Marker','s','MarkerSize',1 )
plot3(origin26(:,1),origin26(:,2),origin26(:,3),'linestyle', 'none','Color',originc26,'Marker','s','MarkerSize',1 )
plot3(origin27(:,1),origin27(:,2),origin27(:,3),'linestyle', 'none','Color',originc27,'Marker','s','MarkerSize',1 )
plot3(origin28(:,1),origin28(:,2),origin28(:,3),'linestyle', 'none','Color',originc28,'Marker','s','MarkerSize',1 )
plot3(origin29(:,1),origin29(:,2),origin29(:,3),'linestyle', 'none','Color',originc29,'Marker','s','MarkerSize',1 )
plot3(origin30(:,1),origin30(:,2),origin30(:,3),'linestyle', 'none','Color',originc30,'Marker','s','MarkerSize',1 )
plot3(origin31(:,1),origin31(:,2),origin31(:,3),'linestyle', 'none','Color',originc31,'Marker','s','MarkerSize',1 )
plot3(origin32(:,1),origin32(:,2),origin32(:,3),'linestyle', 'none','Color',originc32,'Marker','s','MarkerSize',1 )
% Create the colorbar
map = colors;
colormap(map)
colorbar
%% Outline the 4 sectors for better visualization
vert1 = [40 -40 -40 ; 40 80 -40 ; 40 -40 40 ; 40 80 40;
         88 -40 -40 ; 88 80 -40 ; 88 -40 40 ; 88 80 40];
face1 = [1 2 4 3; 2 6 8 4; 4 3 7 8; 1 5 7 3; 1 2 6 5; 5 6 8 7];
patch('Vertices', vert1, 'Faces', face1,'FaceColor','none');
vert2 = [vert1(:,1)-128,vert1(:,2)-40,vert1(:,3)];
patch('Vertices', vert2, 'Faces', face1,'FaceColor','none');
vert3 = [-80 40 -40 ; -80 40 40 ; -80 88 -40 ; -80 88 40;
         40 40 -40 ; 40 40 40 ; 40 88 -40 ; 40 88 40];
patch('Vertices', vert3, 'Faces', face1,'FaceColor','none');
vert4 = [vert3(:,1)+40,vert3(:,2)-128,vert3(:,3)];
patch('Vertices', vert4, 'Faces', face1,'FaceColor','none');
%Set Viewing angle
grid on
az = 45;
el = 70;
view(az, el);
%Make it look nice
set(gca,'linewidth',2)
set(gca,'FontSize',24)
set(gca,'FontWeight','bold');
b = findobj('tag', 'Colorbar');
% modify pos according to your needs
set(gca,'position',[0.13 0.16 0.6 0.8]);
set(b,'position',[0.85 0.05 0.05 0.9]);
hold off








