# Author: Emmanouil "Manolis" Nikolakakis

import matplotlib.pyplot as plt
from pydicom import dcmread
from pydicom.data import get_testdata_file
import numpy as np
import os

input_file=""
nz_input=60.0
ny_input=60.0
nx_input=60.0
data=np.fromfile(input_file,dtype=np.float32, sep='')
data=data.reshape(int(nz_input),int(ny_input),int(nx_input))


for z in range(1,int(nz_input)):
    data[:,:,z]=np.transpose(data[:,:,z])

np.array(data,np.float32).tofile('')
