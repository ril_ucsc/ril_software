These are example Python files that have been used in RIL in the past and are added in case there are code segments that could be potentially useful in the future:

resize.py -> Used to change the dimensions of a 3D image
ct_to_mumap.py -> Used to convert a folder of a CT slices to a single attenuation map (in binary raw image format) to be used with rilrecon
cylinder_lac.py -> Used to create a cylinder of a uniform linear attenuation correction value to be used when testing the attenuation correction implementation
switchCoronalTransverse.py -> Used to change the views of an image between the x, y, z axis in order to align them with another image
