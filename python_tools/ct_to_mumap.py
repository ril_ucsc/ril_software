# Author: Emmanouil "Manolis" Nikolakakis

import matplotlib.pyplot as plt
from pydicom import dcmread
from pydicom.data import get_testdata_file
import numpy as np
import os

# giving directory path with DICOM CT slices
dirname = ''

# Ratio to upscale value from 110 eV(or 70 eV) to 511 keV 
ratio = 0.424 # calculated specifically for direct conversion for our soil, should be defined


# giving file extension
ext = ('.IMA')
i = 0

final3d = np.zeros((379,512,512))

# iterating over all files (DICOM CT slices)
files = os.listdir(dirname)
sorted_files = sorted(files)

for files in sorted_files:
    if files.endswith(ext):
        print(files)  # printing file name of desired extension
        ds = dcmread(dirname+'/'+files)
        new_arr=np.arange(512*512)
        new_result=new_arr.reshape(512,512).astype(np.float32)
        final_result=new_arr.reshape(512,512).astype(np.float32)
        print(i)
        for x in range(1,512):
            for y in range(1,512):
                # Each loop generates adds a new slice in the z axis
                if (i<379):
                    final3d[i,x,y]=(((float(ds.pixel_array[x,y]-1000))*0.171)/1000.0+0.171) #same but for 3D output
                new_result[x,y]=(((float(ds.pixel_array[x,y]-1000))*0.171)/1000.0+0.171) #convert Hounsfield to LAC
        i=i+1
    else:
        continue

# Export final binary image as 512 x 512 x 379 with each value as a 32-bit float
np.array(final3d,np.float32).tofile('')
