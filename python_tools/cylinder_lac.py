# Author: Emmanouil "Manolis" Nikolakakis

import numpy as np
import matplotlib.pyplot as plt

z_dims = 150
y_dims = 200
x_dims = 200

z = np.arange(0, z_dims)
y = np.arange(0, y_dims)
x = np.arange(0, x_dims)
arr = np.ones((y.size, x.size))
arr_t = arr.transpose()
arr2 = np.ones((z.size, x.size, y.size))


cx = x.size/2 # x coordinate of cylinder's centre
cy = y.size/2 # y coordinate of cylinder's centre
r = 55 #radius of cylinder in pixels
	
# The two lines below could be merged, but I stored the mask
# for code clarity.
mask = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < r**2

arr[mask] = 1

for i in range(0,z.size):
	if 16<i<142: # Radius is 55 mm so 55 pixels, height 126 mm so 126 pixels
		arr2[i,:,:] = arr_t [:,:]
	else:
		arr2[i,:,:] = np.ones((x.size, y.size))

np.array(arr2,np.float32).tofile('')