import pydicom as dicom
import matplotlib.pyplot as plt
import matplotlib.gridspec as grd
from matplotlib.widgets import Slider
from matplotlib.widgets import RangeSlider
from matplotlib.widgets import Button
from matplotlib.widgets import MultiCursor
import numpy as np
import glob
from tifffile import imread
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.ndimage import zoom
from scipy.ndimage import affine_transform
from matplotlib.backend_bases import MouseButton
plt.rcParams["font.family"] = "Arial"


class PointTracker:
    def __init__(self):
        self.crosshair = np.array([-1, -1, -1])
        self.CT_points = -1*np.ones([3,3])
        self.PET_points = -1*np.ones([3,3])
    
    def set_CT_point(self, row, point):
        self.CT_points[row, :] = point
    
    def set_PET_point(self, row, point):
        self.PET_points[row, :] = point
    
    def set_crosshair(self, point):
        self.crosshair = point
    
    def get_CT_points(self):
        return self.CT_points
    
    def get_PET_points(self):
        return self.PET_points
    
    def get_crosshair(self):
        return self.crosshair

class RegistrationEntry:
    def __init__(self, gridspace, row, label, points):
        # Data
        self.points = points
        point = self.points.get_crosshair()
        self.row = row
        
        # Grid
        self.cell = gridspace[self.row, 0]
        self.grid = grd.GridSpecFromSubplotSpec(2,5, self.cell, hspace=0.1, wspace=0.1)
 
        # Label
        self.ax_label = plt.subplot(self.grid[:, 0])
        self.ax_label.set_facecolor('black')
        self.label = self.ax_label.text(0.5, 0.5, label, color='white', ha='center', va='center', fontsize=18)
        self.ax_label.set_axis_off()
        self.ax_label.add_artist(self.ax_label.patch)
        self.ax_label.patch.set_zorder(-1)
        
        # CT
        self.ax_CT_button = plt.subplot(self.grid[0, 1])
        self.CT_button = Button(self.ax_CT_button, 'Set CT')
        self.CT_button.on_clicked(self.register_CT)

        self.ax_CT_caption = plt.subplot(self.grid[0, 2:])
        self.CT_caption = self.ax_CT_caption.text(0, 0.5, str(point), ha='left', va='center', fontsize=12)
        self.ax_CT_caption.axis('off')
        
        # PET
        self.ax_PET_button = plt.subplot(self.grid[1, 1])
        self.PET_button = Button(self.ax_PET_button, 'Set PET')
        self.PET_button.on_clicked(self.register_PET)

        self.ax_PET_caption = plt.subplot(self.grid[1, 2:])
        self.PET_caption = self.ax_PET_caption.text(0, 0.5, str(point), ha='left', va='center', fontsize=12)
        self.ax_PET_caption.axis('off')
    
    def register_CT(self, event):
        point = self.points.get_crosshair()
        self.points.set_CT_point(self.row, point)
        self.CT_caption.set_text(str(point))
        
    def register_PET(self, event):
        point = self.points.get_crosshair()
        self.points.set_PET_point(self.row, point)
        self.PET_caption.set_text(str(point))

class DualIndexTracker:
    def __init__(self, X_CT, X_PET, fig, points):
        self.mX_CT = X_CT
        self.mX_PET = X_PET
        self.X_CT_max = np.amax(X_CT)
        self.X_PET_max = np.amax(X_PET)
        self.points = points
        
        self.no_slices = X_CT.shape[2]
        self.slice_ind = self.no_slices // 2
        self.crosshair = [self.slice_ind, self.slice_ind, self.slice_ind]
        
        self.initialize_display(fig)
        self.initialize_CT(X_CT)
        self.initilaize_PET(X_PET)
        self.initialize_dual()
        self.initialize_slice_slider()
        self.initialize_caption()
        self.initialize_cursors()
        self.initialize_controls()
        self.refresh()
        self.draw_crosshair(self.slice_ind, self.slice_ind)
              
    def refresh(self):
        # Rotation and Crosshair
        self.cursor[self.view] = self.slice_ind
        self.X_CT_slice = np.take(self.X_CT, indices=self.crosshair[self.view], axis=self.view)
        self.X_PET_slice = np.take(self.X_PET, indices=self.crosshair[self.view], axis=self.view)
        self.cursor_text.set_text(f"Cursor i: {self.cursor[0]} j: {self.cursor[1]} k: {self.cursor[2]}")
        self.crosshair_text.set_text(f"Crosshair i: {self.crosshair[0]} j: {self.crosshair[1]} k: {self.crosshair[2]}")
        self.points.set_crosshair(self.crosshair)
        
        # CT 
        self.X_CT_slice[self.X_CT_slice < self.CT_thresh_min] = self.CT_thresh_min
        self.X_CT_slice[self.X_CT_slice > self.CT_thresh_max] = self.CT_thresh_max
        self.im_CT.set_data(self.X_CT_slice)
        self.im_CT.set_clim(vmin=self.CT_thresh_min, vmax=self.CT_thresh_max)
        self.cbar_CT.update_normal(self.im_CT)
        
        # PET
        self.X_PET_slice[self.X_PET_slice < self.PET_thresh_min] = self.PET_thresh_min
        self.X_PET_slice[self.X_PET_slice > self.PET_thresh_max] = self.PET_thresh_max
        self.im_PET.set_data(self.X_PET_slice)
        self.im_PET.set_clim(vmin=self.PET_thresh_min, vmax=self.PET_thresh_max)
        self.cbar_PET.update_normal(self.im_PET)
        
        # Dual
        self.im_dCT.set_data(self.X_CT_slice)
        self.im_dPET.set_data(self.X_PET_slice)
        self.im_dCT.set_clim(vmin=self.CT_thresh_min, vmax=self.CT_thresh_max)
        self.im_dPET.set_clim(vmin=self.PET_thresh_min, vmax=self.PET_thresh_max)
        self.im_dPET.set_alpha(self.opacity)
        
        # Caption
        self.caption_text.set_text('')
        
    def initialize_display(self, fig):
        # gridspace init
        self.fig = fig
        self.main_grid = grd.GridSpec(nrows=1, ncols=2, width_ratios=[0.2, 1], height_ratios=[1], left=0.01, right=0.99, bottom=0.01, top=0.99, hspace=0.05, wspace=0.05)
        self.control_cell = self.main_grid[0, 0]
        self.display_cell = self.main_grid[0, 1]
        
        # display init
        self.display_grid = grd.GridSpecFromSubplotSpec(3,1, self.display_cell, hspace=0.0, wspace=0.2, width_ratios=[1], height_ratios=[1, 0.05, 0.05])
        self.image_cell = self.display_grid[0, 0]
        self.slider_cell = self.display_grid[1, 0]
        self.caption_cell = self.display_grid[2, 0]
        self.image_grid = grd.GridSpecFromSubplotSpec(1,3, self.image_cell, hspace=0.2, wspace=0.2)
    
    def initialize_CT(self, X_CT):
        def updateCTThreshold(val):
            self.CT_threshold_min = val[0]
            self.CT_threshold_max = val[1]
            self.CT_thresh_min = self.CT_threshold_min*self.X_CT_max
            self.CT_thresh_max = self.CT_threshold_max*self.X_CT_max
            self.refresh()
        
        # CT init
        self.ax_CT = plt.subplot(self.image_grid[0, 0])
        self.CT_threshold = 1
        self.CT_thresh_min = 0
        self.CT_thresh_max = self.X_CT_max
        self.X_CT = X_CT
        self.im_CT = self.ax_CT.imshow(self.X_CT[:, :, self.slice_ind], cmap='gist_gray', interpolation='none')
        divider_CT = make_axes_locatable(self.ax_CT)
        
        # Threshold Slider
        int_cax_CT = divider_CT.append_axes("right", size="5%", pad=0.05)
        self.CT_intensity_slider = RangeSlider(int_cax_CT, '', valmin=0, valmax=1, valinit=(0, self.CT_threshold), valstep=0.05, orientation="vertical")
        self.CT_intensity_slider.on_changed(updateCTThreshold)
        
        # Color Bar
        cb_cax_CT = divider_CT.append_axes("right", size="5%", pad=0.05)
        self.cbar_CT = plt.colorbar(self.im_CT, cax=cb_cax_CT)
     
    def initilaize_PET(self, X_PET):
        def updatePETThreshold(val):
            self.PET_threshold_min = val[0]
            self.PET_threshold_max = val[1]
            self.PET_thresh_min = self.PET_threshold_min*self.X_PET_max
            self.PET_thresh_max = self.PET_threshold_max*self.X_PET_max
            self.refresh()
            
        # PET init
        self.ax_PET = plt.subplot(self.image_grid[0, 1])
        self.PET_threshold = 1
        self.PET_thresh_min = 0
        self.PET_thresh_max = self.X_PET_max
        self.X_PET = X_PET
        self.im_PET = self.ax_PET.imshow(self.X_PET[:, :, self.slice_ind], cmap='gist_yarg', interpolation='none')
        divider_PET = make_axes_locatable(self.ax_PET)
        
        # Threshold Slider
        int_cax_PET = divider_PET.append_axes("right", size="5%", pad=0.05)
        self.PET_intensity_slider = RangeSlider(int_cax_PET, '', valmin=0, valmax=1, valinit=(0, self.PET_threshold), valstep=0.05, orientation="vertical")
        self.PET_intensity_slider.on_changed(updatePETThreshold)
        
        # Color Bar
        cb_cax_PET = divider_PET.append_axes("right", size="5%", pad=0.05)
        self.cbar_PET = plt.colorbar(self.im_PET, cax=cb_cax_PET)
    
    def initialize_dual(self): 
        def updateOpacity(val):
            self.opacity = val
            self.refresh()
            
        # Overlay(dual) init
        self.ax_dual = plt.subplot(self.image_grid[0, 2])
        self.opacity = 0.5
        self.im_dCT = self.ax_dual.imshow(self.X_CT[:, :, self.slice_ind], cmap='gist_gray', interpolation='none')
        self.im_dPET = self.ax_dual.imshow(self.X_PET[:, :, self.slice_ind], cmap='magma', alpha=self.opacity, interpolation='none', extent=self.im_CT.get_extent())
        
        # Opacity Slider
        divider_dual = make_axes_locatable(self.ax_dual)
        cax_dual = divider_dual.append_axes("right", size="10%", pad=0.05)
        self.opacity_slider = Slider(cax_dual, '', 0, 1, valinit=self.opacity, valstep=0.05, orientation="vertical")
        self.opacity_slider.on_changed(updateOpacity)
    
    def initialize_slice_slider(self):
        def update_slice(val):
            self.slice_ind = val
            self.crosshair[self.view] = self.slice_ind
            self.refresh()
        
        # Slice Slider
        self.ax_slice_slider = plt.subplot(self.slider_cell)
        self.slice_slider = Slider(self.ax_slice_slider, '', 0, self.no_slices, valinit=self.slice_ind, valstep=1)
        self.slice_slider.valtext.set_visible(False)
        self.slice_slider.on_changed(update_slice)
        
    def initialize_caption(self):
        # Caption
        self.ax_caption = plt.subplot(self.caption_cell)
        self.caption_text = self.ax_caption.text(0.5, 0.5, '', ha='center', va='center', fontsize=8)
        self.ax_caption.axis('off')
        
    def initialize_controls(self):
        def viewAxis1(event):
            self.view = 0
            self.slice_slider.set_val(self.crosshair[self.view])
            self.draw_crosshair(self.crosshair[2], self.crosshair[1])
            self.refresh()
        
        def viewAxis2(event):
            self.view = 1
            self.slice_slider.set_val(self.crosshair[self.view])
            self.draw_crosshair(self.crosshair[2], self.crosshair[0])
            self.refresh()
            
        def viewAxis3(event):
            self.view = 2
            self.slice_slider.set_val(self.crosshair[self.view])
            self.draw_crosshair(self.crosshair[1], self.crosshair[0])
            self.refresh()
        
        def compute_transform(event):
            self.affine_matrix = compute_affine_matrix(self.points.get_CT_points(), self.points.get_PET_points())
            print('Found Affine Transformation')
        
        def apply_transform(event):
            self.mX_CT = apply_affine_transformation(self.mX_CT, self.affine_matrix)
            print('Transformation Finished')
            self.X_CT = self.mX_CT
            self.refresh()
            
        def save_views(event):
            self.hide_crosshairs()
            extent_dual = self.ax_dual.get_window_extent().transformed(self.fig.dpi_scale_trans.inverted())
            extent_PET = self.ax_PET.get_window_extent().transformed(self.fig.dpi_scale_trans.inverted())
            extent_CT = self.ax_CT.get_window_extent().transformed(self.fig.dpi_scale_trans.inverted())
            self.fig.savefig('dual_view.png', bbox_inches=extent_dual)
            self.fig.savefig('PET_view.png', bbox_inches=extent_PET)
            self.fig.savefig('CT_view.png', bbox_inches=extent_CT)
            self.show_crosshairs()
            
        def hide_cursors(event):
            self.hide_crosshairs()
        
        def show_cursors(event):
            self.show_crosshairs()
        
        # Controls
        buffer = 0.05
        self.control_grid = grd.GridSpecFromSubplotSpec(11,1, self.control_cell, hspace=0.0, wspace=0.0,
                                                        height_ratios=[2*buffer, 0.2, buffer, 0.2, buffer, 1, buffer, 0.2, buffer, 0.2, 1])
        
        # View Controls
        self.view = 0
        self.views_cell = self.control_grid[1, 0]
        self.views_grid = grd.GridSpecFromSubplotSpec(1,3, self.views_cell, hspace=0.0, wspace=0.1)
        self.ax_controls1 = plt.subplot(self.views_grid[0,0])
        self.ax_controls2 = plt.subplot(self.views_grid[0,1])
        self.ax_controls3 = plt.subplot(self.views_grid[0,2])
        self.b_view_Axis1 = Button(self.ax_controls1, 'View 1')
        self.b_view_Axis2 = Button(self.ax_controls2, 'View 2')
        self.b_view_Axis3 = Button(self.ax_controls3, 'View 3')
        self.b_view_Axis1.on_clicked(viewAxis1)
        self.b_view_Axis2.on_clicked(viewAxis2)
        self.b_view_Axis3.on_clicked(viewAxis3)
        
        # Cursor Views
        self.cursors_cell = self.control_grid[3, 0]
        self.cursors_grid = grd.GridSpecFromSubplotSpec(2,1, self.cursors_cell, hspace=0.0, wspace=0.1)
        
        self.ax_cursor_loc = plt.subplot(self.cursors_grid[0, 0])
        self.cursor_text = self.ax_cursor_loc.text(0, 0.5, '', ha='left', va='center', fontsize=12)
        self.ax_cursor_loc.axis('off')
        
        self.ax_crosshair_loc = plt.subplot(self.cursors_grid[1, 0])
        self.crosshair_text = self.ax_crosshair_loc.text(0, 0.5, '', ha='left', va='center', fontsize=12)
        self.ax_crosshair_loc.axis('off')
        
        # Registration Controls
        self.registration_cell = self.control_grid[5, 0]
        self.registration_grid = grd.GridSpecFromSubplotSpec(3,1, self.registration_cell, hspace=0.1, wspace=0.1)
        
        self.registerP1 = RegistrationEntry(self.registration_grid, 0, 'P1', self.points)
        self.registerP2 = RegistrationEntry(self.registration_grid, 1, 'P2', self.points)
        self.registerP3 = RegistrationEntry(self.registration_grid, 2, 'P3', self.points)
        
        # Transform Controls
        self.transform_cell = self.control_grid[7, 0]
        self.transform_grid = grd.GridSpecFromSubplotSpec(1,2, self.transform_cell, hspace=0.0, wspace=0.1)
        self.ax_compute_transform = plt.subplot(self.transform_grid[0,0])
        self.ax_capply_transform = plt.subplot(self.transform_grid[0,1])
        self.b_compute_transform = Button(self.ax_compute_transform, 'Compute Transform')
        self.b_apply_transform = Button(self.ax_capply_transform, 'Apply Transform')
        self.b_compute_transform.on_clicked(compute_transform)
        self.b_apply_transform.on_clicked(apply_transform)
        
        # Save Views Controls
        self.save_cell = self.control_grid[9, 0]
        self.save_grid = grd.GridSpecFromSubplotSpec(1,3, self.save_cell, hspace=0.0, wspace=0.1)
        self.ax_save_views = plt.subplot(self.save_grid[0,0])
        self.b_save_views = Button(self.ax_save_views, 'Save Views')
        self.b_save_views.on_clicked(save_views)
        
        self.ax_hide_cursors = plt.subplot(self.save_grid[0,1])
        self.b_hide_cursors = Button(self.ax_hide_cursors, 'Hide Cursors')
        self.b_hide_cursors.on_clicked(hide_cursors)
        
        self.ax_show_cursors = plt.subplot(self.save_grid[0,2])
        self.b_show_cursors = Button(self.ax_show_cursors, 'Show Cursors')
        self.b_show_cursors.on_clicked(show_cursors)
        
        

    def initialize_cursors(self):
        def on_move(event):
            if event.inaxes in [self.ax_CT, self.ax_PET, self.ax_dual]:
                if self.view == 0:
                    self.cursor[0] = self.slice_ind
                    self.cursor[1] = int(np.floor(event.ydata))
                    self.cursor[2] = int(np.floor(event.xdata))
                if self.view == 1:
                    self.cursor[0] = int(np.floor(event.ydata))
                    self.cursor[1] = self.slice_ind
                    self.cursor[2] = int(np.floor(event.xdata))
                if self.view == 2:
                    self.cursor[0] = int(np.floor(event.ydata))
                    self.cursor[1] = int(np.floor(event.xdata))
                    self.cursor[2] = self.slice_ind
                self.cursor_text.set_text(f"Cursor i: {self.cursor[0]} j: {self.cursor[1]} k: {self.cursor[2]}")

        def on_click(event):
            if (event.button is MouseButton.LEFT and
                event.inaxes in [self.ax_CT, self.ax_PET, self.ax_dual]):
                ix, iy = event.xdata, event.ydata
                self.draw_crosshair(ix, iy)
                
                if self.view == 0:
                    # self.crosshair[0] unchanged
                    self.crosshair[1] = int(np.floor(event.ydata))
                    self.crosshair[2] = int(np.floor(event.xdata))
                if self.view == 1:
                    self.crosshair[0] = int(np.floor(event.ydata))
                    # self.crosshair[1] unchanged
                    self.crosshair[2] = int(np.floor(event.xdata))
                if self.view == 2:
                    self.crosshair[0] = int(np.floor(event.ydata))
                    self.crosshair[1] = int(np.floor(event.xdata))
                    # self.crosshair[2] unchanged
                self.points.set_crosshair(self.crosshair)
                self.crosshair_text.set_text(f"Crosshair i: {self.crosshair[0]} j: {self.crosshair[1]} k: {self.crosshair[2]}")
        
        # Cursor
        self.cursor = [self.slice_ind, self.slice_ind, self.slice_ind]
        self.crosshair = [self.slice_ind, self.slice_ind, self.slice_ind]
        self.multicursor = MultiCursor(self.fig.canvas, (self.ax_CT, self.ax_PET, self.ax_dual), horizOn=True, vertOn=True, color='gray', alpha=0.5, lw=1, useblit=False)
        self.move_event = plt.connect("motion_notify_event", on_move)
        self.click_event = plt.connect("button_press_event", on_click)
        self.hline_CT = self.ax_CT.axhline(y=[0], visible=False, color='red', alpha=0.5)
        self.vline_CT = self.ax_CT.axvline(x=[0], visible=False, color='red', alpha=0.5)
        self.hline_PET = self.ax_PET.axhline(y=[0], visible=False, color='red', alpha=0.5)
        self.vline_PET = self.ax_PET.axvline(x=[0], visible=False, color='red', alpha=0.5)
        self.hline_dual = self.ax_dual.axhline(y=[0], visible=False, color='red', alpha=0.5)
        self.vline_dual = self.ax_dual.axvline(x=[0], visible=False, color='red', alpha=0.5)

    def draw_crosshair(self, x, y):
        self.hline_CT.set_ydata([y])
        self.vline_CT.set_xdata([x])
        self.hline_CT.set_visible(True)
        self.vline_CT.set_visible(True)
        
        self.hline_PET.set_ydata([y])
        self.vline_PET.set_xdata([x])
        self.hline_PET.set_visible(True)
        self.vline_PET.set_visible(True)
        
        self.hline_dual.set_ydata([y])
        self.vline_dual.set_xdata([x])
        self.hline_dual.set_visible(True)
        self.vline_dual.set_visible(True)
    
    def show_crosshairs(self):
        self.multicursor.connect()
        for line in self.multicursor.vlines:
            line.set_visible(True)
        for line in self.multicursor.hlines:
            line.set_visible(True)
        self.hline_CT.set_visible(True)
        self.vline_CT.set_visible(True)
        self.hline_PET.set_visible(True)
        self.vline_PET.set_visible(True)
        self.hline_dual.set_visible(True)
        self.vline_dual.set_visible(True)
        
    def hide_crosshairs(self):
        for line in self.multicursor.vlines:
            line.set_visible(False)
        for line in self.multicursor.hlines:
            line.set_visible(False)
        self.multicursor.disconnect()
        self.hline_CT.set_visible(False)
        self.vline_CT.set_visible(False)
        self.hline_PET.set_visible(False)
        self.vline_PET.set_visible(False)
        self.hline_dual.set_visible(False)
        self.vline_dual.set_visible(False)

def compute_affine_matrix(P1, P2):
    # Convert points to numpy arrays
    P1 = np.array(P1)
    P2 = np.array(P2)
    
    # Compute centroids
    centroid_P1 = np.mean(P1, axis=0)
    centroid_P2 = np.mean(P2, axis=0)
    
    # Centralize points
    P1_centered = P1 - centroid_P1
    P2_centered = P2 - centroid_P2
    
    # Compute covariance matrix
    H = P1_centered.T @ P2_centered
    
    # Singular Value Decomposition
    U, S, Vt = np.linalg.svd(H)
    R = Vt.T @ U.T
    
    # Ensure a proper rotation matrix (det(R) should be 1)
    if np.linalg.det(R) < 0:
        Vt[-1, :] *= -1
        R = Vt.T @ U.T
    
    # Compute translation
    t = centroid_P2 - R @ centroid_P1
    
    # Create the affine transformation matrix
    affine_matrix = np.eye(4)
    affine_matrix[:3, :3] = R
    affine_matrix[:3, 3] = t
    
    return affine_matrix

def apply_affine_transformation(array, affine_matrix):
    # The affine matrix should be inverted for the transformation
    inverse_affine = np.linalg.inv(affine_matrix)
    
    # Extract the rotation matrix and translation vector from the inverse affine matrix
    rotation = inverse_affine[:3, :3]
    translation = inverse_affine[:3, 3]
    
    # Apply the affine transformation
    transformed_array = affine_transform(array, rotation, offset=translation, order=1)
    
    return transformed_array

def showBothImages(CT_images_path, PET_image_path):
        
    # point data for rotation
    points = PointTracker()
    
    # PET Data
    filename_PET = PET_image_path.split('/')[-1]
    X_PET = imread(PET_image_path)
    X_PET = np.flip(X_PET, axis=0)
    vi_PET = float(0.5)
    vj_PET = float(0.5)
    vk_PET = float(0.5)
    v_PET = [vi_PET, vj_PET, vk_PET]
    rows_PET = int(X_PET.shape[0])
    cols_PET = int(X_PET.shape[1])
    slices_PET = int(X_PET.shape[2])
    PET_dims = [vi_PET*rows_PET, vj_PET*cols_PET, vk_PET*slices_PET]
    print('- PET Image Info -')
    print(f'{filename_PET}')
    print(f'Image Size: {rows_PET} x {cols_PET} x {slices_PET} vxls')
    print(f'Image Dims: [{vi_PET*rows_PET}, {vj_PET*cols_PET}, {vk_PET*slices_PET}] mm')
    print(f'Voxel Dims: [{vi_PET}, {vj_PET}, {vk_PET}] mm')
    print('')
    
    # CT Data
    plots = []
    filenames_CT = []
    slices_CT = 0
    for idx, filename in enumerate(glob.glob(CT_images_path + '*.IMA')):
        ds = dicom.dcmread(filename)
        pix = ds.pixel_array
        plots.append(pix)
        filename = filename.split('\\')[-1]
        filenames_CT.append(filename)  # Save the filename
        slices_CT = slices_CT + 1
        if idx == 0:
            vi_CT = float(ds.PixelSpacing[0])
            vj_CT = float(ds.PixelSpacing[1])
            vk_CT = float(ds.SliceThickness)
            v_CT = [vi_CT, vj_CT, vk_CT]
            rows_CT = int(ds.Rows)
            cols_CT = int(ds.Columns)

    X_CT = np.dstack(plots)
    CT_dims = [vi_CT*rows_CT, vj_CT*cols_CT, vk_CT*slices_CT]
    print('- CT Image Info -')
    print(f'{CT_images_path}')
    print(f'Image Size: {rows_CT} x {cols_CT} x {slices_CT} vxls')
    print(f'Image Dims: [{vi_CT*rows_CT}, {vj_CT*cols_CT}, {vk_CT*slices_CT}] mm')
    print(f'Voxel Dims: [{vi_CT}, {vj_CT}, {vk_CT}] mm')
    print('')
    
    print('- Resized Images -')
    
    # Pad to match dims
    for idx, (dim_CT, dim_PET) in enumerate(zip(CT_dims, PET_dims)):
        if dim_CT == dim_PET:
            continue
        if dim_CT < dim_PET:
            voxels_to_add = int(round( (dim_PET - dim_CT)/v_CT[idx] ))
            padding_width = [[0, 0], [0, 0], [0, 0]]
            padding_width[idx][0] = voxels_to_add // 2
            padding_width[idx][1] = voxels_to_add // 2
            X_CT = np.pad(X_CT, padding_width)
        if dim_CT > dim_PET:
            voxels_to_add = int(round( (dim_CT - dim_PET)/v_PET[idx] ))
            padding_width = [[0, 0], [0, 0], [0, 0]]
            padding_width[idx][0] = voxels_to_add // 2
            padding_width[idx][1] = voxels_to_add // 2
            X_PET = np.pad(X_PET, padding_width)
    
    # Reslice to match size of smaller
    scale_factors = np.divide(X_PET.shape, X_CT.shape)
    v_CT = np.divide(v_CT, scale_factors)
    X_CT = zoom(X_CT, scale_factors, order=1)
    
    print(f'PET Image Size: {X_PET.shape[0]} x {X_PET.shape[1]} x {X_PET.shape[2]} vxls')
    print(f'PET Image Dims: [{X_PET.shape[0]*v_PET[0]}, {X_PET.shape[1]*v_PET[1]}, {X_PET.shape[2]*v_PET[2]}] mm')
    print(f'CT Image Size: {X_CT.shape[0]} x {X_CT.shape[1]} x {X_CT.shape[2]} vxls')
    print(f'CT Image Dims: [{X_CT.shape[0]*v_CT[0]:.3f}, {X_CT.shape[1]*v_CT[1]:.3f}, {X_CT.shape[2]*v_CT[2]:.3f}] mm')
    print('')
    
    # plot
    fig = plt.figure(figsize=(18, 5))
    tracker = DualIndexTracker(X_CT, X_PET, fig, points)
    plt.ion()
    plt.show(block=False)
    
    # Protect data from garabage collector until plot is closed!
    # Usually, block=True would solve this exact problem, but it does not behave in Spyder.
    try:
        while fig.number in plt.get_fignums():
            plt.pause(0.01)
    except:
        plt.close(fig.number)
        raise
    
    return 0

def main():
    # CT Images (stored as series of .IMA files)
    CT_dir_path = '2024-05-23_EARTH_SHOT_CAP_TUBES_BENNETT-20240606T205140Z-001/'
    CT_images_path = CT_dir_path + 'DYNACT_HEAD_NAT_FILL_HU_NORMAL_180UM_VOXEL_109KV/'
    
    # PET Images (stored as 3D .tifs)
    PET_path = 'nasir_recon-20240606T222236Z-003/'
    PET_image_path = PET_path + 'USCS_Orgain_Soil_4Cap_150uC_fud_12uCi_1hour_11_53am.E15.V0.5I50RAW.tif'
    
    dual_images = showBothImages(CT_images_path, PET_image_path)

    return 0

if __name__ == "__main__":
    main()
